<?php

$config = [
    'settings' => [
        'debugMode' => true, // Set to false in production
        'displayErrorDetails' => true, // Set to false in production
        'query_delay' => 0.1, // Минимальное время между запросами для избежания частой отправки
        'api' => [
            'events' => [
                'cache_file' => __DIR__ . '/../cache/events/events.json',
                'cache_time' => 60, // В секундах
                'update_events' => true, // Обновлять список событий
                'test' => [
                    'cache_file' => __DIR__ . '/../cache/events/events_api.txt',
                ],
            ],
            'schema' => [
                'data_path' => __DIR__ . '/../public/schema/',
                'svg_path' => __DIR__ . '/../public/schema/svg/',
            ],
        ],
        'crocusApi' => [
            'block_time' => 15, // Время блокирования места в мин 1..30
            'connect' => false, // Создавать ли подключение (можно отключить на сервере, где нет доступа к апи)
            'url' => 'www.crocus-hall.ru/api',
            'wsdl' => [
                'url' => $_SERVER['DOCUMENT_ROOT'] . '/wsdl.wsdl',
                'login' => '',
                'password' => '',
                'connection_timeout' => 5,
            ],
        ],
        'database' => [
            'host' => 'localhost',
            'db_name' => '',
            'user' => '',
            'password' => '',
            'charset' => 'utf8',
        ],
        'languages' => [
            'default' => 'ru',
            'enabled' => ['ru', 'en'],
            'path' => __DIR__ . '/i18n/',
        ],
        'logger' => [
            'log' => false,
            'name' => 'vegas-tickets',
            'path' => __DIR__ . '/../logs/app.log',
        ],
        'mail' => [
            'host' => 'localhost',
            'port' => 25,
            'username' => '',
            'password' => '',
            'from' => '',
            'fromName' => 'Вегас Сити Холл',
            'max_count' => 5, // Число попыток при неудаче
            'send' => true,
            'ticketsForwarders' => [],
        ],
        'platron' => [
            'merchid' => '',
            'skey' => '',
            'pay_system' => '',
            'surl' => 'https://www.platron.ru/init_payment.php',
            'send' => true,
        ],
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],
        'smsc' => [
            'sender' => 'VEGAS-HALL',
            'login' => '',
            'password' => '',
            'post' => 0,
            'https' => 0,
            'charset' => 'utf-8',
            'debug' => 0,
            'smtp_from' => 'sms@crocus-hall.ru',
            'sms_err_def' => 5,
            'repeat_interval' => 10, // Интервал в секундах, через который можно отправить повторное смс
            'send' => true,
        ],
        'ticket' => [
            'system' => 'vegas',
            'name' => 'Вегас Сити Холл',
            'site' => 'www.vegas-hall.ru',
            'shop' => 'www.vegas-hall.ru/tickets',
            'max_order' => 6,
        ],
        'twig' => [
            'cache' => false,
            'charset' => 'utf-8',
            'template_path' => __DIR__ . '/../templates/',
        ],
    ],
];
