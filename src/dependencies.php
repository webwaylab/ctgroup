<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
if ($app->getContainer()->settings['logger']['log']) {
    $container['logger'] = function ($c) {
        $settings = $c->settings['logger'];
        $logger = new Monolog\Logger($settings['name']);
        $logger->pushProcessor(new Monolog\Processor\UidProcessor());
        $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));

        return $logger;
    };
}
//twig
$container['view'] = function ($c) {
    $settings = $c->settings['twig'];
    $view = new \Slim\Views\Twig($settings['template_path'], [
        'cache' => $settings['cache']
    ]);

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $c['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($c['router'], $basePath));

    return $view;
};
//db
$container['db'] = function ($c) {
    $settings = $c->settings['database'];
    $dsn = sprintf('mysql:host=%s;dbname=%s;charset=utf8', $settings['host'], $settings['db_name']);
    return new \Slim\PDO\Database($dsn, $settings['user'], $settings['password'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
};
//sessions
$container['session'] = function ($c) {
    $session_factory = new \Aura\Session\SessionFactory;
    $session = $session_factory->newInstance($_COOKIE);
    return $session;
};

$container['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        return $response
            ->withStatus(404)
            ->withHeader('Content-Type', 'text/html')
            ->write('Page not found');
    };
};