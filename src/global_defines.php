<?php

//errors
define('ERR_EVENT_ENDED', 1);
define('ERR_EVENT_NOT_FOUND', 100);
define('ERR_SEGMENT_NOT_FOUND', 101);
define('ERR_INCORRECT_NUMERATION', 102);

//crocus api methods
define('API_METHOD_GET_CURRENT_EVENTS', 'GetCurrentEvents');
define('API_METHOD_GET_SEGMENT_FREE_PLACES_COUNT', 'GetSegmentFreePlacesCount');
define('API_METHOD_GET_SECTOR_FREE_PLACES', 'GetSectorFreePlaces');
define('API_METHOD_GET_PLACE_STATUS', 'GetPlaceStatus');
define('API_METHOD_RESERVE_ALL_BLOCK_PLACES_BY_USER_HASH', 'ReserveAllBlockPlacesByUserHash');
define('API_METHOD_RESERVE_ORDER_GET_CLIENT_COUPON_RESULT', 'ReserveOrderGetClientCouponResult');
define('API_METHOD_RESERVE_ORDER_GET_CLIENT_RESULT', 'ReserveOrderGetClientResult');
define('API_METHOD_BLOCK_PLACE_BY_USER_HASH', 'BlockPlaceByUserHash');
define('API_METHOD_UNBLOCK_PLACE_BY_USER_HASH', 'UnBlockPlaceByUserHash');
define('API_METHOD_UNBLOCK_ALL_PLACES_BY_USER_HASH', 'UnBlockAllPlacesByUserHash');
define('API_METHOD_CREATE_PAY_ORDER_BY_USER_HASH', 'CreatePayOrderByUserHash');
define('API_METHOD_PAY_ORDER_ADD_PLATRON_ID', 'PayOrderAddPlatronID');
define('API_METHOD_IS_PAY_ORDER_AVAILABLE', 'IsPayOrderAvailable');
define('API_METHOD_CONFIRM_PAY_ORDER', 'ConfirmPayOrder');
define('API_METHOD_PAY_ORDER_GET_CLIENT_RESULT', 'PayOrderGetClientResult');
define('API_METHOD_PAY_ORDER_GET_CLIENT_RESULT_COUPONS', 'PayOrderGetClientResultCoupons');
define('API_METHOD_PAY_GET_COUPON_INFORMATION', 'GetCouponInformation');
define('API_METHOD_CANCEL_PAY_ORDER', 'CancelPayOrder');
define('API_METHOD_GET_COUPON_INFORMATION', 'GetCouponInformation');
define('API_METHOD_BLOCK_COUPONS_BY_USER_HASH', 'BlockCouponsByUserHash');
define('API_METHOD_UNBLOCK_COUPONS_BY_USER_HASH', 'UnBlockCouponsByUserHash');
define('API_METHOD_BLOCK_FAN_BY_USER_HASH', 'BlockFanByHash');
define('API_METHOD_UNBLOCK_FAN_BY_USER_HASH', 'UnBlockFanByUserHash');
define('API_METHOD_PAY_ORDER_GET_CLIENT_COUPON_RESULT', 'PayOrderGetClientCouponResult');
define('API_METHOD_GET_COUPON_INFORMATION_BY_EVENT_ID', 'GetCouponInformationByEventID');
define('API_METHOD_CHECK_PROMO_CODE', 'CheckPromoCode');

define('TYPE_STRING', 'string');
define('TYPE_INT', 'integer');
define('TYPE_ARRAY', 'array');

define('FORMAT_MERID_LENGTH', 10);
define('FORMAT_SEGID_LENGTH', 5);

define('MESSAGE_STATUS_WAIT', 'wait');
define('MESSAGE_STATUS_SUCCESS', 'success');
define('MESSAGE_STATUS_ERROR', 'error');
define('MESSAGE_STATUS_EXPIRED', 'expired');
define('MESSAGE_STATUS_SENT', 'sent');

define('PAY_STATUS_OK', 'ok');
define('PAY_STATUS_SUCCESS', 'success');
define('PAY_STATUS_ERR', 'error');
define('PAY_STATUS_REJECTED', 'rejected');
define('PAY_STATUS_WAIT', 'wait');

define('MSG_RESERVE_PLACES', 'reserve_places');
define('MSG_BUY_PLACES', 'buy_places');

define('COLOR_TRANSPARENT', 'transparent');

define('TABLE_PLATRON_TRANSACTIONS_LOG', 'platron_transactions_log');
define('TABLE_PLATRON_LOG', 'platron_log');
define('TABLE_TICKET', 'ticket');
define('TABLE_COUPONS', 'coupon');

define('REQUEST_DIRECTION_INCOME', 'income');
define('REQUEST_DIRECTION_OUTCOME', 'outcome');

define('PAY_REQUEST_TYPE_GET_TRANSACTION_ID', 'Запрос id платежа');
define('PAY_REQUEST_TYPE_CHECK_PAYMENT_IS_AVAILABLE', 'Проверка возможности оплаты');
define('PAY_REQUEST_TYPE_FINISH', 'Завершение оплаты');

define('REPORTS_PASSWORD', 'wwreports');
