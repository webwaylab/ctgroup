<?php
$config = [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'debugMode' => true, // set to false in production
        'query_delay' => 0.1, //минимальное время между запросами для избежания частой отправки

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],
//      twig settings
        'twig' => [
            'template_path' => __DIR__ . '/../templates/',
//          'cache' => __DIR__ . '/../cache/twig/',
            'cache' => false,
            'charset' => 'utf-8'
        ],

//      Monolog settings
        'logger' => [
            'log' => false,
            'name' => 'crocus-tickets',
            'path' => __DIR__ . '/../logs/app.log',
        ],
//      допустимые языки
        'languages' => [
            'default' => 'ru',
            'enabled' => ['ru', 'en'],
            'path' => __DIR__ . '/i18n/'
        ],
        'crocusApi' => [
            'url' => 'www.crocus-hall.ru/api',
            'block_time' => 15, //время блокирования места в мин 1..30
            'wsdl' => [
                'url' => $_SERVER['DOCUMENT_ROOT'] . '/wsdl.wsdl',
                // 'url' => 'http://www.crocus-hall.ru/_tickets/wsdl.wsdl',
                'login' => 'WebService',
                'password' => 'Kopr2ole304',
                'connection_timeout' => 5
            ],
//          создавать ли подключение (можно отключить на сервере, где нет доступа к апи)
            'connect' => false
        ],
        'smsc' => [
            'sender' => "CROCUS-HALL",
            'login' => 'crocuscityhall',
            'password' => 'dshfUYDFl434234jdf',
            'post' => 0,
            'https' => 0,
            'charset' => 'utf-8',
            'debug' => 0,
            'smtp_from' => "sms@crocus-hall.ru",
            'sms_err_def' => 5,
            //интервал в секундах, через который можно отправить повторное смс
            'repeat_interval' => 60,
            'send' => true
        ],
        'platron' => [
            'merchid' => '',
            'skey' => '',
            'pay_system' => '',
            'surl' => 'https://www.platron.ru/init_payment.php',
            'test' => [
                'card_number' => '5285 0000 0000 0005',
                'pay_system' => 'TEST'
            ],
            'send' => true
        ],
        'api' => [
            'events' => [
                'cache_file' => __DIR__ . '/../cache/events/events.json',
                'cache_time' => 60, //в секундах
                //обновлять список событий
                'update_events' => true,
                'test' => [
                    'cache_file' => __DIR__ . '/../cache/events/events_api.txt'
                ]
            ],
            'schema' => [
                'data_path' => __DIR__ . '/../public/schema/',
                'svg_path' => __DIR__ . '/../public/schema/svg/'
            ]
        ],
        'mail' => [
            'host' => 'localhost',
            'port' => 25,
            'username' => '',
            'password' => '',
            'from' => 'no-reply@crocus-hall.ru',
            'fromName' => 'Крокус Сити Холл',
            //число попыток при неудаче
            'max_count' => 5,
            'send' => true,
            'ticketsForwarders' => array(),
        ],
        'database' => [
            'host' => 'localhost',
            'db_name' => 'test',
            'user' => 'root',
            'password' => '',
            'charset' => 'utf8'
        ],
        'ticket' => [
            'system' => 'crocus',
            'max_order' => 6
        ],
    ],
];
