<?php
namespace OrangeData;

use Utils;

class Index
{
    public static function createOrder($id, $clientEmail)
    {
        $orange = self::client();
        $orange->is_debug();

        return $orange->create_order(
            $id,
            1,
            $clientEmail,
            Utils::settings('orange.taxation')
        );
    }

    private static function client()
    {
        return new Client(
            Utils::settings('orange.inn'),
            Utils::settings('orange.url'),
            getcwd() . 'private_key.xml',
            getcwd() . '/client.key',
            getcwd().'/client.crt',
            getcwd() . '/cacert.pem',
            '1234'
        );
    }
}
