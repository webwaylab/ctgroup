<?php

class Coupon extends Ticket
{
    /**
     * @var int
     */
    protected $ticket_id;

    /**
     * @return array
     */
    public function getPrintData()
    {
        $coupon = $this->fetchData($this->statementCoupon());
        $this->ticket_id = $coupon['ticket_id'];

        $data = $this->fetchData($this->statement());
        $data['parameters'] = json_decode($data['parameters'], true);
        $data['price'] = $coupon['price'];
        $data['coupon_id'] = $coupon['coupon_id'];
        $data['order_id'] = $coupon['order_id'];
        $data['consist'] = json_decode($coupon['consist']);
        $this->data = $data;

        return $this->data;
    }

    /**
     * @return mixed
     */
    protected function statementCoupon()
    {
        $pdo = $this->container->db;
        return $pdo->select(['id', 'order_id', 'ticket_id', 'price', 'consist', 'coupon_id'])
            ->from(TABLE_COUPONS)
            ->where('coupon_hash', '=', $this->hash);
    }

    /**
     * @return mixed
     */
    protected function statement()
    {
        $pdo = $this->container->db;
        return $pdo->select(['id', 'order_id', 'ticket_id', 'buy_datetime', 'price', 'event_id', 'event_name', 'event_datetime', 'segment', 'row', 'seat', 'series', 'client_email', 'client_phone', 'parameters', 'client_name'])
            ->from('ticket')
            ->where('id', '=', $this->ticket_id);
    }
}
