<?php

/**
 * Class Utils
 */
class Utils
{
    /**
     * @var null
     */
    private static $config = null;

    /**
     * @param $path
     * @return array|bool
     */
    public static function checkDir($path)
    {
        $path = str_replace('/', DIRECTORY_SEPARATOR, $path);
        $directories = explode(DIRECTORY_SEPARATOR, dirname($path));
        $newPath = '';
        foreach ($directories as $d => $dir) {
            $dir = ltrim($dir, DIRECTORY_SEPARATOR);
            if (!empty($dir)) {
                $newPath .= DIRECTORY_SEPARATOR . $dir;
                $realPath = realpath($newPath);
                if ($realPath === false) {
                    if (!mkdir($newPath, 0775, true)) {
                        return ['err_msg' => 'Can\'t create directory ' . $realPath];
                    }
                }
            }
        }
        return true;
    }

    /**
     * @param $phone
     * @return bool
     */
    public static function validatePhone($phone)
    {
        $pattern = "#^\+?[0-9]{0,3}[\s\-]?\(?[0-9]{3,5}\)?[\s\-]?[0-9]+[\s\-]?[0-9]+[\s\-]?[0-9]+$#";

        return (preg_match($pattern, $phone) === 1);
    }

    /**
     * @param $phone
     * @return string
     */
    public static function getFormattedPhone($phone)
    {
        $formattedPhone = self::getDigits($phone);

        //если пришел телефон, начинающийся с 8
        if ($formattedPhone[0] == 8) {
            $formattedPhone = substr($formattedPhone, 1);
        }

        //если пришел телефон без +7
        if (strlen($formattedPhone) == 10) {
            $formattedPhone = '7' . $formattedPhone;
        }
        return $formattedPhone;
    }

    /**
     * @param $string
     * @return string
     */
    public static function getDigits($string)
    {
        $pattern = "#\d+#";
        $numbers_string = '';

        preg_match_all($pattern, $string, $digits);
        if (count($digits[0]) > 0) {
            $numbers_string = implode('', $digits[0]);
        }
        return $numbers_string;
    }

    /**
     * @param $config
     */
    public static function setSettings($config)
    {
        self::$config = $config;
    }

    /**
     * @param string $section
     * @param null $default
     * @return bool|null
     */
    public static function settings($section = '', $default = null)
    {
        if (is_null(self::$config)) {
            return false;
        } else {
            if (!empty($section)) {
                $sections = explode('.', $section);
                $subsection = self::$config;
                foreach ($sections as $sub) {
                    if (isset($subsection[$sub])) {
                        $subsection = $subsection[$sub];
                    } else if (isset($default)) {
                        return $default;
                    }
                }
                return $subsection;
            } else {
                return self::$config;
            }
        }
    }

    /**
     * @param $id
     * @param string $default
     * @return string
     */
    public static function getSession($id, $default = '')
    {
        return (isset($_SESSION[$id])) ?
            $_SESSION[$id] :
            $default;
    }

    /**
     * @param $id
     * @param string $value
     */
    public static function setSession($id, $value = '')
    {
        $_SESSION[$id] = $value;
    }

    /**
     * @param string $phone
     * @param $msg
     * @param bool $test
     * @return bool
     */
    public static function sendSms($phone, $msg, $test = false)
    {
        if (Utils::settings('smsc.send') == true || $test === true) {
            ob_start();
            $smsApi = SmsApi::getInstance();
            $response = $smsApi->sendSms($phone, $msg, 0, 0, 0, 0, Utils::settings('smsc.sender'));

            if ($response[1] > 0) {
                Logger::logSmsMessage($phone, $msg, MESSAGE_STATUS_SUCCESS);
                return true;
            } else {
                Logger::error('Utils::sendSms', ob_get_contents());
                ob_clean();
                return false;
            }
        } else {
            Logger::logSmsMessage($phone, $msg, MESSAGE_STATUS_WAIT);
        }

        return true;
    }

    /**
     * @param string $val
     * @return int
     */
    public static function getCheckboxInt($val = '')
    {
        return ($val == 'on') ? 1 : 0;
    }

    /**
     * @param $email
     * @return mixed
     */
    public static function validateEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * @param PDOStatement $statement
     * @return PDOStatement|bool
     */
    public static function executeQuery($statement)
    {
        try {
//            $statement->beginTransaction();
            return $statement->execute();
        } catch (Exception $e) {
//            $statement->rollback();
            Logger::error('Utils::executeQuery', "DBError on '{$statement}':\n{$e->getMessage()}");
        }
    }

    /**
     * @param \Slim\Container $container
     * @throws Exception
     * @todo сделать защиту от частых запросов и csrf
     */
    public static function checkLastQuery(\Slim\Container $container)
    {
        $segment = $container->session->getSegment('Crocus');
        $query = $container->request->getUri()->getPath();
        $params = $container->request->getQueryParams();

        $last_query_time = (int)$segment->get('last_query_datetime');
        if ($last_query_time) {
            $query_delay = $segment->get('query_delay', self::settings('query_delay', 1));
            $time = time();
            if (time() - $last_query_time < $query_delay) {
                $segment->set('query_delay', $query_delay + 1);
                throw new Exception('too often queries!');
            } else {
                $segment->set('query_delay', self::settings('query_delay', 1));
            }
        }
        $segment->set('last_query_datetime', time());
        $segment->set('last_query', md5($query . json_encode($params)));

    }

    /**
     * @param $path
     * @return array|bool
     */
    public static function addFileLastVersion($path)
    {
        $file = $_SERVER['DOCUMENT_ROOT'] . '/' . ltrim($path, '/');
        if (!file_exists($file)) {
            return false;
        }
        $last_modified = filemtime($file);
        return [
            'path' => $path,
            'last_modified' => $last_modified
        ];
    }
}
