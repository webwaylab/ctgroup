<?php

/**
 * Class Adminarea
 */
class Adminarea
{
    /** @var \Slim\Container */
    protected $container;

    /**
     * @param \Slim\Container $container
     */
    public function __construct($container)
    {
        if ($container instanceof \Slim\Container) {
            $this->container = $container;
        }
    }

    /**
     * @return bool
     */
    public function isAuthorized()
    {
        return !empty($_COOKIE['adminarea_login']);
    }

    /**
     * @param array $queryParams
     * @return array
     */
    public function getTickets($queryParams)
    {
        static $params = ['order_id', 'ticket_id', 'price', 'event_id', 'client_email', 'client_phone', 'client_name'];

        /** @var \Slim\PDO\Database $pdo */
        $pdo = $this->container->db;
        $statement = $pdo->select()->from(TABLE_TICKET);

        foreach ($params as $param) {
            if (!empty($queryParams[$param])) {
                $statement->where($param, '=', $queryParams[$param]);
            }
        }

        $from = isset($queryParams['from']) ? $queryParams['from'] : 0;
        $limit = 20;
        $statement->limit($limit, $from);
        $statement->orderBy('id', 'DESC');

        $tickets = $statement->execute()->fetchAll(PDO::FETCH_ASSOC);
        return $tickets;
    }

    /**
     * @param array $params
     * @return bool
     */
    public function auth($params)
    {
        static $login = 'vegas_login';
        static $password = 'vegas_pass_word';
        if (!isset($params['login'], $params['password'])) {
            return false;
        }

        if ($params['login'] != $login) {
            return false;
        }

        if ($params['password'] != $password) {
            return false;
        }

        return setcookie('adminarea_login', 1, 0);
    }
}
