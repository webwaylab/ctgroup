<?php
use Nette\Mail\Message;

/**
 * Class Mailer
 */
class Mailer
{
    /**
     * @var \Nette\Mail\SmtpMailer
     */
    private $mailer;

    /**
     * Mailer constructor.
     */
    public function __construct()
    {
        $settings = Utils::settings('mail');

        if ($settings['send'] === true) {
            $this->mailer = new Nette\Mail\SmtpMailer(array(
                'host' => $settings['host'],
                'username' => $settings['username'],
                'password' => $settings['password'],
                // 'secure' => 'ssl',
            ));
        }
        $this->message = new Message();
        $this->message->setFrom($settings['from'], $settings['fromName']);
    }

    /**
     * @param array $params
     */
    public function addMessage($params)
    {
        $mailer = $this->message;
        $mailer->setSubject($params['subject'])
            ->setBody($params['body']);

        if (is_array($params['to'])) {
            foreach ($params['to'] as $to) {
                $mailer->addTo($to);
            }
        } else {
            $mailer->addTo($params['to']);
        }
    }

    /**
     * @param $attachment
     */
    public function addAttachment($attachment)
    {
        if (is_array($attachment)) {
            foreach ($attachment as $att) {
                if (file_exists($att)) {
                    $this->message->addAttachment($att);
                }
            }
        } else {
            if (file_exists($attachment)) {
                $this->message->addAttachment($attachment);
            }
        }
    }

    /**
     *
     */
    public function send()
    {
        $email_id = Logger::logEmailMessage([
            'to' => implode(', ', array_keys($this->message->getHeader('To'))),
            'from' => implode(', ', array_keys($this->message->getFrom())),
            'message' => $this->message->getBody(),
            'subject' => $this->message->getSubject(),
            'status' => MESSAGE_STATUS_WAIT
        ]);

        if (Utils::settings('mail.send') === true) {
            $this->mailer->send($this->message);
            Logger::logEmailMessage(['id' => $email_id, 'status' => MESSAGE_STATUS_SENT], 'update');
        }
    }
}