<?php

define('COL_NUM_SEGMENT_ID', 1);
define('COL_NUM_SEGMENT_RUS_NAME', 0);
define('COL_NUM_SECTOR_ID', 2);
define('COL_NUM_ROW_ID', 3);
define('COL_NUM_PLACE_ID', 4);
define('COL_NUM_PLACE_COORDS', 5);

define('LANG_RUS', 'rus');
define('LANG_ENG', 'eng');

/**
 * Class SchemeGenerator
 */
class SchemeGenerator
{
    const RESULT_OK = 1;

    /**
     * @var string
     */
    public $lang;
    /**
     * @var string
     */
    private static $upload_dir = '/tmp/';

    /**
     * @return array|string
     */
    public function run()
    {
        $act = isset($_POST['act']) ? $_POST['act'] : '';
        if ($act === 'save') {
            $file = isset($_FILES['scheme_file']) ? $_FILES['scheme_file'] : '';
            $this->lang = isset($_POST['lang']) ? $_POST['lang'] : LANG_RUS;
            if (!$file['name']) {
                $output = ['status' => 0, 'error' => 'Error! No scheme file'];
            } else {
                $output = $this->generateScheme();
            }
        } else {
            $output = $this->getForm();
        }
        return $output;
    }

    /**
     * @return string
     */
    public function getForm()
    {
        $form =
            '<form id="schemeGenerator" action="" method="post" enctype="multipart/form-data">
    <div>
        <label for="scheme_id">ID схемы</label>
        <input type="text" name="scheme_id" id="scheme_id" value="" size="5"/>
    </div>
    <div>
     <label for="lang">Язык схемы</label>
        <select name="lang" id=""lang>
            <option value="' . LANG_RUS . '">' . LANG_RUS . '</option>
            <option value="' . LANG_ENG . '">' . LANG_ENG . '</option>
        </select>
    </div>
    <div>
    <div>
    <label for="scheme_var">По какой схеме выгружать</label>
        <select id="scheme_var" name="scheme_var">
            <option value="0">По новой схеме (json)</option>
            <option value="1">По старой схеме (для crocus-hall.ru)</option>
        </select>
    </div>
     <label for="scheme_file">Файл с данными (Название сегмента, Номер сегмента, Номер сектора, Номер ряда, Номер места)</label>
     </div>
     <div>
        <input type="file" name="scheme_file" id="scheme_file" required="true"/>
    </div>
    <input type="hidden" name="act" value="save" />
    <div>
        <input type="submit" value="Сгенерировать схему" />
    </div>
</form>';
        return $form;
    }

    /**
     * @param string $err_mess
     * @return string
     */
    public function formatError($err_mess = '')
    {
        $output = sprintf(
            '<div>
    <h1>ERROR</h1>
    <div>%s</div>
</div>', $err_mess);
        return $output;
    }

    /**
     * @return array
     */
    public function generateScheme()
    {
        $id = isset($_POST['scheme_id']) ? $_POST['scheme_id'] : '';
        $variant = isset($_POST['scheme_var']) ? $_POST['scheme_var'] : 0;
        $min_col_count = 4;

        if (!$id) {
            return ['status' => 0, 'error' => 'Error! Wrong scheme ID'];
        }
        if (isset($_FILES['scheme_file']) && $_FILES['scheme_file']['error'] == UPLOAD_ERR_OK) {
            $scheme_file_input = $_FILES['scheme_file'];

            if (
                pathinfo($scheme_file_input['name'], PATHINFO_EXTENSION) != 'csv'
                ||
                $scheme_file_input['type'] != 'text/csv'
            ) {
                return ['status' => 0, 'error' => 'Error! Wrong file type'];
            }

            $scheme_file_input_uploaded = self::$upload_dir . $scheme_file_input['name'];

            switch ($variant) {
                case 0:
                    $scheme_file_output = $id . '.json';
                    break;
                case 1:
                    $scheme_file_output = $id . '.' . $this->lang . '.js';
                    break;
            }

            if (move_uploaded_file($scheme_file_input['tmp_name'], $scheme_file_input_uploaded) !== false) {
                $hall_info = [];
                switch ($variant) {
                    case 0:
                        $coordinates_ptrn = '/R(\d+)C(\d+)/';
                        $row_interval = 20;
                        $col_interval = 20;
                        $rows = [];
                        $cols = [];

                        $width = 0;
                        $height = 0;

                        $sector_path = [];

                        if (($f = fopen($scheme_file_input_uploaded, 'r')) !== false) {
                            $row = 1;
                            $hall_info['id'] = (int)$id;

                            while (($data = fgetcsv($f, 0, ",", '"')) !== false) {
                                if (count($data) < $min_col_count) {
                                    return ['status' => 0, 'error' => 'Error! Wrong data format'];
                                }
                                if ($row > 1) {
                                    $segment_id = (int)$data[COL_NUM_SEGMENT_ID];

                                    switch ($this->lang) {
                                        case LANG_ENG:
                                            $segment_name = $data[COL_NUM_SEGMENT_RUS_NAME];
                                            break;
                                        case LANG_RUS:
                                        default:
                                            $segment_name = $data[COL_NUM_SEGMENT_RUS_NAME];
                                            break;
                                    }

                                    if (!in_array($segment_id, array_column($hall_info['segments'], 'id'))) {
                                        $segment_index = count($hall_info['segments']);
                                        $hall_info['segments'][$segment_index] = [
                                            'id' => $segment_id,
                                            'name' => $segment_name
                                        ];
                                        $sector_path[$segment_id] = ['row' => [], 'col' => []];
                                    } else {
                                        $segment_index = array_search($segment_id, array_column($hall_info['segments'], 'id'));
                                    }

                                    $sector_id = (int)$data[COL_NUM_SECTOR_ID];
                                    $sector_name = $segment_name;

                                    if (!in_array($sector_id, array_column($hall_info['segments'][$segment_index]['sectors'], 'id'))) {
                                        $hall_info['segments'][$segment_index]['sectors'][] = [
                                            'id' => $sector_id
                                        ];
                                        $sector_path[$segment_id][$sector_id] = ['row' => [], 'col' => []];
                                    }

                                    $row_num = (int)$data[COL_NUM_ROW_ID];
                                    $seat_num = (int)$data[COL_NUM_PLACE_ID];

                                    preg_match($coordinates_ptrn, $data[COL_NUM_PLACE_COORDS], $seat_coords);
                                    $cell_r = $seat_coords[1];
                                    $cell_c = $seat_coords[2];

                                    $x = (int)$cell_c * $col_interval;
                                    $y = (int)$cell_r * $row_interval;

                                    $segment_row = &$sector_path[$segment_id]['row'];
                                    $segment_col = &$sector_path[$segment_id]['col'];

                                    $sector_row = &$sector_path[$segment_id][$sector_id]['row'];
                                    $sector_col = &$sector_path[$segment_id][$sector_id]['col'];


                                    if (!isset($sector_row['min']) && !isset($sector_row['max'])) {
                                        $sector_row['min'] = $cell_r;
                                        $sector_row['max'] = $cell_r;
                                    } else {
                                        $sector_row['min'] = min($sector_row['min'], $cell_r);
                                        $sector_row['max'] = max($sector_row['max'], $cell_r);
                                    }

                                    if (!isset($sector_col['min']) && !isset($sector_col['max'])) {
                                        $sector_col['min'] = $cell_c;
                                        $sector_col['max'] = $cell_c;
                                    } else {
                                        $sector_col['min'] = min($sector_col['min'], $cell_c);
                                        $sector_col['max'] = max($sector_col['max'], $cell_c);
                                    }


                                    if (!isset($segment_row['min']) && !isset($segment_row['max'])) {
                                        $segment_row['min'] = $cell_r;
                                        $segment_row['max'] = $cell_r;
                                    } else {
                                        $segment_row['min'] = min($segment_row['min'], $cell_r);
                                        $segment_row['max'] = max($segment_row['max'], $cell_r);
                                    }

                                    if (!isset($segment_col['min']) && !isset($segment_col['max'])) {
                                        $segment_col['min'] = $cell_c;
                                        $segment_col['max'] = $cell_c;
                                    } else {
                                        $segment_col['min'] = min($segment_col['min'], $cell_c);
                                        $segment_col['max'] = max($segment_col['max'], $cell_c);
                                    }


                                    if (!isset($rows['min']) && !isset($rows['max'])) {
                                        $rows['min'] = $cell_r;
                                        $rows['max'] = $cell_r;
                                    } else {
                                        $rows['min'] = min($rows['min'], $cell_r);
                                        $rows['max'] = max($rows['max'], $cell_r);
                                    }

                                    if (!isset($cols['min']) && !isset($cols['max'])) {
                                        $cols['min'] = $cell_c;
                                        $cols['max'] = $cell_c;
                                    } else {
                                        $cols['min'] = min($rows['min'], $cols);
                                        $cols['max'] = max($rows['max'], $cols);
                                    }

                                    $width = max($x, $width);
                                    $height = max($y, $height);

                                    $hall_info['seats'][] = [
                                        'segment_id' => $segment_id,
                                        'sector_id' => $sector_id,
                                        'row' => $row_num,
                                        'seat' => $seat_num,
                                        'row_label' => 'Ряд',
                                        'seat_label' => 'Место',
                                        'x' => $x,
                                        'y' => $y
                                    ];

                                    unset($x, $y);
                                }
                                $row++;
                            }

                            $hall_info['width'] = $width + $col_interval;
                            $hall_info['height'] = $height + $row_interval;

                            unset($sector_id, $segment_id, $segment_name, $segment_index, $seat_coords, $row);

                            fclose($f);
                        } else {
                            unlink($scheme_file_input_uploaded);
                            return ['status' => 0, 'error' => 'Error! Cannot open file '];
                        }

                        $sector_outline = [];
                        $padding = 20;

                        foreach ($sector_path as $seg => &$segment) {
                            $sector_outline[$seg]['x'] = ($segment['col']['min'] - 1) * $col_interval;
                            $sector_outline[$seg]['y'] = ($segment['row']['min'] - 1) * $row_interval;

                            $sector_outline[$seg]['width'] = ($segment['col']['max'] - $segment['col']['min'] + 2) * $col_interval;;
                            $sector_outline[$seg]['height'] = ($segment['row']['max'] - $segment['row']['min'] + 2) * $row_interval;;

                            foreach ($segment as $s => &$sector) {
                                ksort($sector);
                                if ($s == 'row' || $s == 'col') {
                                    continue;
                                }
                                $sector_outline[$seg][$s]['x'] = ($sector['col']['min'] - 1) * $col_interval;
                                $sector_outline[$seg][$s]['y'] = ($sector['row']['min'] - 1) * $row_interval;

                                $sector_outline[$seg][$s]['width'] = ($sector['col']['max'] - $sector['col']['min'] + 2) * $col_interval;
                                $sector_outline[$seg][$s]['height'] = ($sector['row']['max'] - $sector['row']['min'] + 2) * $row_interval;

                                $sector_outline[$seg][$s][] = 'M' .
                                    ($sector['col']['min'] - 1) * $col_interval . ' ' .
                                    ($sector['row']['min'] - 1) * $row_interval;
                                $sector_outline[$seg][$s][] = 'L' .
                                    ($sector['col']['max'] + 1) * $col_interval . ' ' .
                                    ($sector['row']['min'] - 1) * $row_interval;
                                $sector_outline[$seg][$s][] = 'L' .
                                    ($sector['col']['max'] + 1) * $col_interval . ' ' .
                                    ($sector['row']['max'] + 1) * $row_interval;
                                $sector_outline[$seg][$s][] = 'L' .
                                    ($sector['col']['min'] - 1) * $col_interval . ' ' .
                                    ($sector['row']['max'] + 1) * $row_interval;

                                $sector_outline[$seg][$s][] = 'L' .
                                    ($sector['col']['min'] - 1) * $col_interval . ' ' .
                                    ($sector['row']['min'] - 1) * $row_interval;
                            }
                        }
                        unset($segment, $sector, $row, $row_array, $row_array_intersect, $row_array_prev);

                        foreach ($hall_info['segments'] as &$segment) {
                            $segment_id = $segment['id'];
                            $segment['x'] = $sector_outline[$segment_id]['x'];
                            $segment['y'] = $sector_outline[$segment_id]['y'];
                            $segment['width'] = $sector_outline[$segment_id]['width'];
                            $segment['height'] = $sector_outline[$segment_id]['height'];
                            foreach ($segment['sectors'] as &$sector) {
                                $sector_id = $sector['id'];
                                $sector['outline'] = implode(" ", $sector_outline[$segment_id][$sector_id]);
                                $sector['x'] = $sector_outline[$segment_id][$sector_id]['x'];
                                $sector['y'] = $sector_outline[$segment_id][$sector_id]['y'];
                                $sector['width'] = $sector_outline[$segment_id][$sector_id]['width'];
                                $sector['height'] = $sector_outline[$segment_id][$sector_id]['height'];
                            }
                        }

                        $string = json_encode($hall_info, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

                        echo '<pre>';
                        print_r($hall_info);
                        echo '</pre>';
                        // exit;
                        break;

                    case 1:
                        $hooking_map = [];
                        if (($f = fopen($scheme_file_input_uploaded, 'r')) !== false) {
                            $row = 1;
                            while (($data = fgetcsv($f, 0, ",", '"')) !== false) {
                                if (count($data) < $min_col_count) {
                                    return ['status' => 0, 'error' => 'Error! Wrong data format'];
                                }
                                if ($row > 1) {
                                    $segment_id = (int)$data[COL_NUM_SEGMENT_ID];

                                    switch ($this->lang) {
                                        case LANG_ENG:
                                            $segment_name = $data[COL_NUM_SEGMENT_RUS_NAME];
                                            break;
                                        case LANG_RUS:
                                        default:
                                            $segment_name = $data[COL_NUM_SEGMENT_RUS_NAME];
                                            break;
                                    }

                                    if (!isset($hall_info[$segment_name])) {
                                        $hall_info[$segment_name]['id'] = (string)$segment_id;
                                    }

                                    $sector_id = (int)$data[COL_NUM_SECTOR_ID];

                                    if (
                                    !in_array($sector_id, array_column($hall_info[$segment_name]['segments'], 'id'))
                                    ) {
                                        $hall_info[$segment_name]['segments'][] = [
                                            'id' => (string)$sector_id,
                                            'name' => $segment_name
                                        ];
//                                        большой жирный ужасный костыль
                                        if ($sector_id != 1) {
                                            $hooking_map[] = [$segment_id, $sector_id, $segment_id, 1];
                                        }
                                    }
                                }
                                $row++;
                            }
                            fclose($f);

                        } else {
                            unlink($scheme_file_input_uploaded);
                            return ['status' => 0, 'error' => 'Error! Cannot open file '];
                        }

                        $string = 'var scheme = ';
                        $string .= json_encode($hall_info, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
//                        $string .= "\n";
//                        $string .= 'var SectorMap = ' . json_encode($hooking_map);

                        break;
                }
                unlink($scheme_file_input_uploaded);
                return ['status' => 1,
                    'filename' => $scheme_file_output,
                    'length' => strlen($string),
                    'content' =>
                        $string
                ];

            } else {
                return ['status' => 0, 'error' => 'Error! Cannot move uploaded file'];
            }
        } else {
            return ['status' => 0, 'error' => 'Error while upload file: ' . $_FILES['scheme_file']['error']];
        }
    }
}
