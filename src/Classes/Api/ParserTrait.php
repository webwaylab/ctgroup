<?php
namespace Api;

use Utils;

/**
 * Trait ParserTrait
 * @package Api
 */
trait ParserTrait
{
    /**
     * распарсить страшную строку с событиями
     * @param $csv_string
     * @return array|bool
     */
    protected function parseEvent($csv_string)
    {
        if (empty($csv_string)) {
            return false;
        }

        $event = [];
        $event['name'] = [
            'ru' => $csv_string[0],
            'en' => $csv_string[9]
        ];
        $event['schema_id'] = (int)$csv_string[1];
        $event['date_time'] = \DateTime::createFromFormat('d.m.Y H:i:s', $csv_string[2])->getTimestamp();
        $tmp_prices = explode('-', $csv_string[3]);
        $event['prices'] = [
            'min' => (int)$tmp_prices[0],
            'max' => (int)$tmp_prices[1]
        ];
        $event['id'] = (int)$csv_string[4];

        $event['features'] = [
            'reserve' => (bool)$csv_string[5],
            'buy' => (bool)$csv_string[7],
            'delivery' => (bool)$csv_string[6],
            'coupons' => (bool)$csv_string[8]
        ];

        $event['premium'] = (bool)$csv_string[10];

        $event['organisation'] = [
            'name' => $csv_string[11],
            'inn' => $csv_string[12],
            'address' => $csv_string[13]
        ];

        $insurance_on = (string)$csv_string[14];
        $event['insurance_on'] = isset($insurance_on) ? $insurance_on : '0';

        $gift = (string)$csv_string[15];
        $event['gift'] = isset($gift) ? $gift : '0';

        $discount = (int)$csv_string[16];
        $event['discount'] = isset($discount) ? $discount : 0;

        return $event;
    }

    /**
     * @param string $coupon_string
     * @return array
     */
    public function parseCoupon($coupon_string)
    {
        $coupon_params = explode("###", $coupon_string);

        return [
            'coupon_id' => $coupon_params[1] ?: null,
            'title' => $coupon_params[2] ?: '',
            'count' => $coupon_params[3] ?: 0,
            'price' => $coupon_params[4] ?: 0
        ];
    }

    /**
     * @param string $ticket_string
     * @return array
     */
    protected function parseOrderResult($ticket_string)
    {
        $ticket_params = explode("###", $ticket_string);

        preg_match_all('#[\d\.]+#', $ticket_params[7], $digits);
        $price = implode('', $digits[0]);

        preg_match_all('#[\d\.]+#', $ticket_params[18], $digits);
        $cost = implode('', $digits[0]);

        $event_id = (int)Utils::getDigits($ticket_params[20]);

        return [
            'order_id' => $ticket_params[0],
            'ticket_id' => $ticket_params[1],
            'client_email' => $ticket_params[4],
            'buy_datetime' => \DateTime::createFromFormat('d/m/y H:i', $ticket_params[5] . ' ' . $ticket_params[6])->getTimestamp(),
            'price' => $price,
            'client_phone' => $ticket_params[9],
            'client_name' => $ticket_params[10],
            'event_name' => $ticket_params[11],
            'segment' => $ticket_params[13],
            'row' => $ticket_params[14],
            'seat' => $ticket_params[15],
            'event_datetime' => $this->getEvent(['event' => $event_id])['date_time'],
            'cost' => $cost,
            'event_id' => $event_id,
            'parameters' => [
                'org' => [
                    'inn' => $ticket_params[2],
                    'address' => $ticket_params[3],
                    'name' => $ticket_params[8]
                ],
                'age' => $ticket_params[12],
                'hall' => [
                    'address' => $ticket_params[22]
                ]
            ]
        ];
    }
}
