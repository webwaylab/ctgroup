<?php
namespace Api;

use Utils;
use I18N;
use Logger;
use Platron;

class Index extends AbstractApi
{
    /**
     * @var string
     * @todo no usage
     */
    public $lang;

    /**
     * @param string $method
     * @param array $params
     * @return array
     */
    public function __call($method, $params = [])
    {
        if (is_callable([$this, $method])) {
            $this->logMethodCall($method, $params);
            return $this->$method($params);
        } else {
            return ['err_msg' => 'ERROR: api method ' . $method . ' is not found'];
        }
    }

    /**
     * @param array $params
     * @return array|string
     * @todo переписать на получение схемы из бд
     */
    public function getSchema($params = [])
    {
        if (isset($params['id'])) {
            $id = (int)$params['id'];
            $filename = Utils::settings('api.schema.data_path') . $id . '.json';
            if (file_exists($filename)) {
                $json = json_decode(file_get_contents($filename), true);
                return $json;
            } else {
                return ['err_msg' => 'No schema id found'];
            }
        } else {
            return ['err_msg' => 'Schema id is required'];
        }
    }

    /**
     * @param array $params
     * @return array|bool|mixed
     */
    public function getEvent($params = [])
    {
        $json = $this->getEvents();

        if (empty($params)) {
            return $json;
        } else {
            if (isset($params['event'])) {
                $id = (int)$params['event'];

                if (isset($json[$id])) {
                    return $json[$id];
                }
            }
        }

        return ['err_msg' => 'No event is found'];
    }

    /**
     * @return array|bool
     */
    public function loadEvents()
    {
        $src_events_data = $this->crocusApi->getCurrentEvents();
        if (empty($src_events_data)) {
            return false;
        }

        $csv_data = preg_split('/[\n\r]/', $src_events_data, 0, PREG_SPLIT_NO_EMPTY);
        $events_data = [];
        foreach ($csv_data as $csv_string) {
            $csv_string_parsed = str_getcsv($csv_string);
            $event = $this->parseEvent($csv_string_parsed);
            $events_data[$event['id']] = $event;
        }

        return $events_data;
    }

    /**
     * @param array $params
     * @return array|bool|mixed
     */
    public function getEvents($params = [])
    {
        $filename = Utils::settings('api.events.cache_file');
        $cache_time = Utils::settings('api.events.cache_time');

        if (file_exists($filename)) {
            $last_update_time = filemtime($filename);
            if (
                (isset($params['forceUpdate']) && $params['forceUpdate'] == 1 || (time() - $last_update_time) > $cache_time)
                &&
                Utils::settings('api.events.update_events') !== false
            ) {
                $events_data = $this->loadEvents();
                $this->cacheEvents($events_data);
            } else {
                $events_data = json_decode(file_get_contents($filename), true);
            }
        } else {
            if (Utils::settings('api.events.update_events') !== false) {
                $events_data = $this->loadEvents();
                $this->cacheEvents($events_data);
            } else {
                $events_data = [];
            }
        }

        return $events_data;
    }

    /**
     * @param int $eventId
     * @return bool|mixed
     */
    public function getCoupon($eventId)
    {
        $params = array('eventId' => $eventId);
        $data = explode(",", $this->crocusApi->getCouponInformation($params));
        $coupon = array(
            'id' => $data[0],
            'title' => $data[1],
            'price' => $data[2],
            'nds' => $data[3],
            'count' => $data[4],
            'consist' => $data[5]
        );

        return $coupon;
    }

    /**
     * @param $params
     * @return array|bool|mixed
     */
    public function blockPlaces($params)
    {
        $arguments = ['event', 'places'];
        $user_hash = $this->getUserHash();

        if ($this->checkUserHash($user_hash)) {
            $segment = $this->session->getSegment('Crocus');
            $segment->set('event_id', $params['event']);
            $allBlocked = false;

            if ($this->checkArguments($arguments, $params)) {
                if ($oddSeats = $this->isOddSeats($params)) {
                    return $oddSeats;
                }

                $request_params = [];
                $request_params['event'] = $params['event'];
                $request_params['hash'] = $user_hash;
                if (isset($params['fan'])) {
                    $request_params['fan'] = $params['fan'];
                }
                if (!empty($params['places'])) {
                    $max_order = Utils::settings('ticket.max_order');
                    if (count($params['places']) > Utils::settings('ticket.max_order')) {
                        return ['err_msg' => $this->messageService->get_message('%MAIN_SITE_MSG_MAX_ORDER%', ['count' => $max_order])];
                    }
                    foreach ($params['places'] as $place) {
                        $request_params['segment'] = $place['segment'];
                        $request_params['sector'] = $place['sector'];
                        $request_params['row'] = $place['row'];
                        $request_params['place'] = $place['place'];

                        $result = $this->blockPlace($request_params);

                        if ($result == 0) {
                            $allBlocked = false;
                            break;
                        } else {
                            $allBlocked = true;
                        }
                    }
                    if ($allBlocked) {
                        $segment->set('tickets_count', count($params['places']));
                    }
                }
                $result = ['status' => $allBlocked];
                if (Utils::settings('debugMode')) {
                    $result['test'] = 1;
                }
                return $result;
            }
        } else {
            return ['err_msg' => I18N::translation('ERR_USER_VALIDATION')];
        }

        return false;
    }

    /**
     * метод вызывается со стороны Платрона, чтобы проверить возможность проведения платежа
     * @param $params - массив входных параметров
     * pg_order_id Идентификатор платежа в системе продавца
     * pg_payment_id Внутренний идентификатор платежа в системе platron.ru
     * pg_amount Сумма выставленного счета (в валюте pg_currency), совпадает с pg_amount в момент инициации платежа
     * pg_currency Валюта выставленного счета, совпадает с pg_currency в момент инициации платежа
     * pg_ps_amount Сумма счета (в валюте pg_ps_currency), выставленного в ПС
     * pg_ps_full_amount Полная сумма (в валюте pg_ps_currency), которую заплатит покупатель с учетом всех комиссий
     * pg_ps_currency Валюта, в которой будет произведен платеж в платежной системе
     * pg_payment_system Идентификатор платежной системы
     * @return string с тегом <pg_status> выставленный в ok или rejected
     */
    public function checkCanAcceptPayment($params)
    {
        $request_params = [
            'order_id' => (string)$params['pg_order_id'],
            'pl_id' => (int)$params['pg_payment_id'],
            'sum' => (float)$params['pg_amount'],
        ];
        $result = $this->isOrderAvailable($request_params);
        $response_params['pg_status'] = $result == 1 ? PAY_STATUS_OK : PAY_STATUS_REJECTED;

        $platron = new Platron($this->container);
        $response = $platron->buildResponse($response_params, 'checkCanAcceptPayment');

        Logger::logPlatronTransaction(
            $request_params['order_id'],
            $request_params['pl_id'],
            $this->container->request->getUri(),
            json_encode($params),
            $response,
            PAY_REQUEST_TYPE_CHECK_PAYMENT_IS_AVAILABLE,
            REQUEST_DIRECTION_INCOME
        );

        return $response;
    }

    /**
     * @param array $params
     * @return string
     */
    public function finishPayOrder($params)
    {
        $request_params = [
            'order_id' => (string)$params['pg_order_id'],
            'pl_id' => (int)$params['pg_payment_id'],
            'sum' => (float)$params['pg_amount']
        ];

        $response_params = [];
        //проверяем, что можно принимать платеж (если ПС не поддерживает check_url)
        $canAcceptPaymentResult = $this->isOrderAvailable($request_params);

        if ($canAcceptPaymentResult == 1) {
            $request_params['pay_system'] = (string)$params['pg_payment_system'];

            // проверяем, что совпадает хэш для транзакции (защита от левых запросов)
            $checkFinishResult = Logger::checkFinishOrderPayment([
                'order_id' => $request_params['order_id'],
                'payment_id' => $request_params['pl_id'],
                'user_hash' => (string)$params['user_hash']
            ]);


            if ($checkFinishResult) {
                //обновляем информацию об оплате в базе
				$data = [
                    'pay_system' => $request_params['pay_system'],
                    'status' => (int)$params['pg_result'] ? PAY_STATUS_SUCCESS : PAY_STATUS_ERR,
                    'datetime_payment' => (string)$params['pg_payment_date'],
                    'order_id' => $request_params['order_id'],
                    'payment_id' => $request_params['pl_id'],
                    'parameters' => json_encode($params),
                    'user_hash' => (string)$params['user_hash']
                ];
                $logFinishResult = Logger::logFinishOrderPayment($data);

                if ($logFinishResult) {
                    if ((int)$params['pg_result'] == 1) {
                        //отправляем информацию об оплате в 1С
                        $confirmPayOrderResult = $this->confirmPayOrder($request_params);

                        if ($confirmPayOrderResult) {
                            $order_result = $this->payOrderGetClientResult([
                                'order_id' => $request_params['order_id'],
                                'pl_id' => $request_params['pl_id']
                            ]);

                            $response_params['pg_status'] = ($order_result)
                                ? PAY_STATUS_OK
                                : PAY_STATUS_ERR;
                        } else {
                            //не смогли подтвердить
                            $response_params['pg_status'] = PAY_STATUS_REJECTED;
                        }
                    } else {
                        //платеж был отклонен - отменяем оплату
                        $cancelPayOrderResult = $this->cancelPayOrder([
                            'pay_system' => $request_params['pay_system'],
                            'order_id' => $request_params['order_id'],
                            'fail_code' => (int)$params['pg_failure_code'],
                            'fail_desc' => (string)$params['pg_failure_description']
                        ]);

                        if ($cancelPayOrderResult) {
                            $response_params['pg_status'] = PAY_STATUS_OK;
                        } else {
                            //проблемы с отменой платежа
                            $response_params['pg_status'] = PAY_STATUS_ERR;
                        }
                    }
                } else {
                    Logger::writeLog('что-то произошло в результате записи в лог logFinishOrderPayment', 'finishPayOrder');
                    //проверку прошли, но что-то произошло в результате записи в лог, можно попробовать еще раз
                    $response_params['pg_status'] = PAY_STATUS_ERR;
                }
            } else {
                //не прошли проверку на совпадение хэша и параметров запроса
                $response_params['pg_status'] = PAY_STATUS_REJECTED;
            }

        } else {
            //не прошли проверку, что можно принимать платеж (напр-р, истекло время)
            $response_params['pg_status'] = PAY_STATUS_REJECTED;
        }


        $platron = new Platron($this->container);
        $response = $platron->buildResponse($response_params, 'finishPayOrder');

        Logger::logPlatronTransaction(
            $request_params['order_id'],
            $request_params['pl_id'],
            $this->container->request->getUri(),
            json_encode($params),
            $response,
            PAY_REQUEST_TYPE_FINISH,
            REQUEST_DIRECTION_INCOME
        );

        return $response;
    }
}
