<?php
namespace Api;

use Logger;

trait UserHashTrait
{
    /**
     * @var \Aura\Session\Session
     */
    protected $session;

    /**
     * @return string
     */
    public function generateUserHash()
    {
        $user_ip = $_SERVER['REMOTE_ADDR'];
        $user_browser = $_SERVER['HTTP_USER_AGENT'];
        $session_id = $this->session->getId();
        $segment = $this->session->getSegment('Crocus');
        $user_salt = $segment->get('user_salt');

        if (is_null($user_salt)) {
            $user_salt = md5(time());
            $segment->set('user_salt', $user_salt);
        }

        $user_hash = md5(md5($user_ip . $user_browser . $session_id) . $user_salt);
        Logger::logUserSession($user_hash);
        return $user_hash;
    }

    /**
     * @param $user_hash
     * @return bool
     */
    public function checkUserHash($user_hash)
    {
        $session_id = $this->session->getId();
        $segment = $this->session->getSegment('Crocus');

        $user_ip = $_SERVER['REMOTE_ADDR'];
        $user_browser = $_SERVER['HTTP_USER_AGENT'];

        $user_salt = $segment->get('user_salt');
        $phone = $segment->get('user_phone', '');

        if (!empty($phone)) {
            $phone_code_status = $segment->get('phone_code_status', 0);
            if ($phone_code_status != 1) {
                return false;
            }
        }

        $user_hash_origin = md5(md5($user_ip . $user_browser . $session_id) . $user_salt);
        if ($user_hash !== $user_hash_origin) {
            $this->session->clear();
        }
        return ($user_hash === $user_hash_origin);
    }

    /**
     * @param $user_hash
     */
    public function setUserHash($user_hash)
    {
        $this->session->getSegment('Crocus')->set('user_hash', $user_hash);
    }

    /**
     * @return mixed
     */
    public function getUserHash()
    {
        return $this->session->getSegment('Crocus')->get('user_hash', false);
    }
}
