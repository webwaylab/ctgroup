<?php
namespace Api;

use Utils;
use I18N;
use Logger;
use PDOStatement;
use Message;
use Mailer;
use CrocusApi;
use Platron;

/**
 * Class AbstractApi
 * @package Api
 */
abstract class AbstractApi
{
    use ParserTrait;
    use UserHashTrait;

    /**
     * @var \Slim\Container
     */
    protected $container;

    /**
     * @var \CrocusApi
     */
    protected $crocusApi;

    /**
     * @var Message
     */
    protected $messageService;

    /**
     * @var Mailer
     */
    protected $mailerService;

    /**
     * @param array $params
     * @return array|bool|mixed
     */
    abstract public function getEvent($params = []);

    /**
     * @return array|bool
     */
    abstract public function loadEvents();

    /**
     * @param array $params
     * @return array|bool|mixed
     */
    abstract public function getEvents($params = []);

    /**
     * @param $params
     * @return array|bool|mixed
     */
    abstract public function blockPlaces($params);

    /**
     * @param int $eventId
     * @return bool|mixed
     */
    abstract public function getCoupon($eventId);

    /**
     * @param CrocusApi|null $crocusApi
     */
    protected function initCrocusApi($crocusApi = null)
    {
        if (!is_null($crocusApi)) {
            $this->crocusApi = $crocusApi;
        } else {
            $this->crocusApi = CrocusApi::getInstance($this->container);
        }
    }

    /**
     * @param $params
     * @return array|bool
     */
    protected function reserveAllPlaces($params)
    {
        $arguments = ['fio', 'phone', 'address', 'email', 'hash', 'subscribe'];
        $reserve = false;
        if ($this->checkArguments($arguments, $params)) {
            $result = $this->crocusApi->reserveAllBlockPlacesByUserHash($params);
            if ($result) {
                $tmp = explode(';', $result);
                $reserve = [
                    'status' => 1,
                    'code' => $tmp[0],
                    'datetime_until' => strtotime($tmp[1])
                ];
                if (Utils::settings('debugMode')) {
                    $reserve['debug'] = $result;
                }

                return $reserve;
            }
        }

        return $reserve;
    }

    /**
     * получить сформированные билеты из 1С
     * @param $params
     * @return bool|mixed|PDOStatement
     */
    protected function payOrderGetClientResult($params)
    {
        $arguments = ['order_id', 'pl_id'];
        $client_phone = $client_email = $event_name = $event_datetime = '';
        $scheme = $this->container->request->getUri()->getScheme();
        $host = $this->container->request->getUri()->getHost();
        $logged_success = false;
        $ticket_id = $event_id = null;

        if (!$this->checkArguments($arguments, $params)) {
            Logger::writeLog('wrong $params: ' . json_encode($params), 'payOrderGetClientResult');
            return false;
        }

        $order_id = $params['order_id'];

        $result = $this->crocusApi->payOrderGetClientResult($params);
        $result = trim($result);
        $tickets = explode("\n", $result);
        $ticket_link = [];

        if (count($tickets) < 1) {
            Logger::writeLog('No tickets', 'payOrderGetClientResult');
            return false;
        }

        $result = $this->crocusApi->payOrderGetClientResultCoupons($params);
        $result = trim($result);
        $coupons = explode("\n", $result);
        $coupon_link = [];

        $pdo = $this->container->db;
        $pdo->beginTransaction();

        foreach ($tickets as $ticket) {
            $ticket_params = $this->parseOrderResult($ticket);
            $result = Logger::logGetOrderResult($ticket_params);

            //проверка, что все билеты записались правильно
            $logged_success = boolval($logged_success | $result);

            if ($result) {
                $ticket_link[] = $scheme . '://' . $host .
                    $this->container->router->pathFor(
                        'ticket', ['id' => $this->getTicketHash($ticket_params['ticket_id'])]
                    );

                $client_phone = $ticket_params['client_phone'];
                $client_email = $ticket_params['client_email'];
                $event_id = $ticket_params['event_id'];
                $event_name = $ticket_params['event_name'];
                $event_datetime = date('d.m.Y, H:i', $ticket_params['event_datetime']);
            } else {
                Logger::writeLog('Tickets not save', 'payOrderGetClientResult');
                return '';
            }

            if (!$ticket_id) {
                $ticket_id = $this->getTicketId($ticket_params['ticket_id']);
            }
        }

        if (strlen($coupons[0]) > 0) {
            if ($event_id) {
                $consist = $this->getCoupon($event_id);
            } else {
                $consist['consist'] = '';
            }

            foreach ($coupons as $coupon) {
                $coupon_params = $this->parseCoupon($coupon);
                $coupon_params['order_id'] = $order_id;
                $coupon_params['ticket_id'] = $ticket_id;
                $coupon_params['consist'] = $consist['consist'];
                $result = Logger::logGetOrderResultCoupon($coupon_params);

                //проверка, что все купоны записались правильно
                $logged_success = boolval($logged_success | $result);

                if ($result) {
                    $coupon_link[] = $scheme . '://' . $host .
                        $this->container->router->pathFor(
                            'coupon', ['id' => $this->getCouponHash($coupon_params['coupon_id'])]
                        );

                    // Отправка запроса на печать чеков на купоны
                    \OrangeData\Index::createOrder($coupon_params['coupon_id'], $client_email);
                }
            }
        }

        if ($logged_success) {
            // Коммит транакции БД
            $pdo->commit();

            // Отправка смс клиенту
            $message = $this->messageService->smsBuyPlaces($order_id);
            Utils::sendSms($client_phone, $message);

            // Отправка письма клиенту с билетами и купонами
            $subject = 'Заказ №' . $order_id . '. Билеты на ' . $event_name . ', ' . $event_datetime . ' - ' . Utils::settings('ticket.name') . ' - ' . Utils::settings('ticket.site');
            $body = $this->messageService->mailBuyPlaces(
                $order_id,
                $event_name,
                $event_datetime,
                implode(",\n", $ticket_link),
                implode(",\n", $coupon_link)
            );
            $this->mailerService->addMessage([
                'to' => $client_email,
                'subject' => $subject,
                'body' => $body
            ]);
            $this->mailerService->send();

            // Отправка дополнительных писем менеджерам
            $this->mailToForwarders($subject, $body);
        }

        return $result;
    }

    /**
     * подтверждение оплаты в системе 1С
     * @param $params
     * @return bool|mixed
     */
    protected function confirmPayOrder($params)
    {
        $arguments = ['order_id', 'pl_id', 'sum', 'pay_system'];
        if ($this->checkArguments($arguments, $params)) {
            $result = $this->crocusApi->confirmPayOrder($params);
            return $result;
        } else {
            return false;
        }

    }

    /**
     * отмена заказа в 1С
     * @param $params
     * @return bool|mixed
     */
    protected function cancelPayOrder($params)
    {
        $arguments = ['order_id', 'pay_system', 'fail_code', 'fail_desc'];
        if ($this->checkArguments($arguments, $params)) {
            $result = $this->crocusApi->cancelPayOrder($params);
            return $result;
        } else {
            return false;
        }
    }

    /**
     * сформировать номер заказа в 1С
     * @param $params
     * @return array
     */
    protected function getOrderId($params)
    {
        $arguments = ['event', 'hash', 'fio', 'phone', 'email', 'insurance', 'coupons'];

        if ($this->checkArguments($arguments, $params)) {
            $result = $this->crocusApi->createPayOrderByUserHash($params);
            if ($result == 0) {
                if (Utils::settings('logger.log')) {
                    $this->container->logger->err('Cannot create pay order');
                }

                return ['err_msg' => 'Cannot create pay order'];
            } else {
                $tmp = explode('#', $result);
                $order = [
                    'id' => $tmp[0],
                    'sum' => (float)$tmp[1],
                    'desc' => $tmp[2]
                ];

                return $order;
            }
        }

        return ['err_msg' => 'Not all arguments'];
    }

    /**
     * сформировать номер платежа в Платроне
     * @param $params
     * @return array|bool
     */
    protected function getPaymentID($params)
    {
        $arguments = ['order_id', 'sum', 'desc', 'user_hash'];
        $this->checkArguments($arguments, $params);
        $platron = new Platron($this->container);
        $result = $platron->initPayment($params);

        return $result;
    }

    /**
     * присвоить номер платежа номеру заказа в 1С
     * @param $params
     * @return bool|mixed
     */
    protected function addPlatronId($params)
    {
        $arguments = ['order_id', 'pl_id'];
        if ($this->checkArguments($arguments, $params)) {
            $result = $this->crocusApi->payOrderAddPlatronId($params);
            return $result;
        } else {
            return false;
        }
    }

    /**
     * @param $arguments
     * @param $params
     * @return bool
     */
    protected function checkArguments($arguments, $params)
    {
        foreach ($arguments as $arg) {
            if (!isset($params[$arg])) {
                // ошибка: неверные входные параметры
                if (Utils::settings('logger.log')) {
                    $this->container->logger->err('Couldn\'t match parameter ' . $arg);
                }
                return false;
            }
        }
        return true;
    }

    /**
     * @param $events_data
     * @return array|bool
     */
    protected function cacheEvents($events_data)
    {
        if (json_encode($events_data) == false) {
            return ['err_msg' => json_last_error_msg()];
        }
        $events_data_json = json_encode($events_data, JSON_UNESCAPED_UNICODE);
        $filename = Utils::settings('api.events.cache_file');
        if (Utils::checkDir($filename) === true) {
            $f = fopen($filename, 'w');
            if (fwrite($f, $events_data_json) !== false) {
                $result = true;
            } else {
                if (Utils::settings('logger.log')) {
                    $this->container->logger->err('Error while writing cache file');
                }
                $result = false;
            }
            fclose($f);
        } else {
            if (Utils::settings('logger.log')) {
                $this->container->logger->err('Error while writing cache file');
            }
            $result = false;
        }
        return $result;
    }

    /**
     * @param $params
     * @return array|false
     */
    protected function isOddSeats($params)
    {
        $event_data = $this->getEvent(['event' => $params['event']]);
        if (411 != $event_data['schema_id']) {
            return false;
        }

        $seats = [];
        foreach ($params['places'] as $place) {
            if (false === in_array($place['segment'], [90308, 90318, 90319, 90320])) {
                continue;
            }
            $key = $place['segment'] . $place['row'];
            $seats[$key] = isset($seats[$key]) ? $seats[$key] + 1 : 1;
        }

        foreach ($seats as $seat => $count) {
            if ($count % 2 == 0) {
                continue;
            }
            return ['err_msg' => I18N::translation('EVEN_SEATS_ERROR')];
        }

        return false;
    }

    /**
     * @param $params
     * @return bool|mixed
     */
    protected function isOrderAvailable($params)
    {
        $arguments = ['order_id', 'pl_id', 'sum'];
        if ($this->checkArguments($arguments, $params)) {
            $result = $this->crocusApi->isPayOrderAvailable($params);
        } else {
            $result = false;
        }

        return $result;
    }

    /**
     * @param $params
     * @return bool|mixed
     */
    protected function blockPlace($params)
    {
        $arguments = ['event', 'segment', 'sector', 'hash', 'fan', 'row', 'place'];

        if (isset($params['fan']) && $params['fan'] > 0) {
            $params['row'] = 1;
            $params['place'] = 1;
        } else {
            $params['fan'] = 0;
        }

        if ($this->checkArguments($arguments, $params)) {
            return $this->crocusApi->blockPlaceByUserHash($params);
        }

        return false;
    }

    /**
     * @param $id
     * @return int
     */
    final protected function getTicketId($id)
    {
        $pdo = $this->container->db;
        $statement = $pdo->select(['id'])
            ->from('ticket')
            ->where('ticket_id', '=', $id)
            ->limit(1, 0);

        $result = Utils::executeQuery($statement);
        return $result->fetchColumn();
    }

    /**
     * @param $id
     * @param string $object
     * @return string
     */
    final protected function getHash($id, $object = 'ticket')
    {
        $pdo = $this->container->db;
        $statement = $pdo->select([$object . '_hash'])
            ->from($object)
            ->where($object . '_id', '=', $id)
            ->limit(1, 0);

        $result = Utils::executeQuery($statement);
        return $result->fetchColumn();
    }

    /**
     * @param $id
     * @return string
     */
    final protected function getTicketHash($id)
    {
        return $this->getHash($id);
    }

    /**
     * @param $id
     * @return string
     */
    final protected function getCouponHash($id)
    {
        return $this->getHash($id, 'coupon');
    }

    /**
     * @param $messageService
     */
    final protected function initMessageService($messageService = null)
    {
        if (!is_null($messageService)) {
            $this->messageService = $messageService;
        } else {
            $this->messageService = Message::getInstance();
        }
    }

    /**
     * @param $mailerService
     */
    final protected function initMailerService($mailerService = null)
    {
        if (!is_null($mailerService)) {
            $this->mailerService = $mailerService;
        } else {
            $this->mailerService = new Mailer();
        }
    }

    /**
     * @param $method
     * @param $params
     */
    final protected function logMethodCall($method, $params)
    {
        if (Utils::settings('logger.log')) {
            $this->container->logger->info(
                'API call method ' . $method . ' with params ' . json_encode($params,
                    JSON_UNESCAPED_UNICODE)
            );
        }
    }

    final protected function reloadSession()
    {
        session_regenerate_id(true);
    }

    /**
     * Отправка дополнительных писем менеджерам.
     * @param string $subject
     * @param $body
     */
    private function mailToForwarders($subject, $body)
    {
        $forwarders = Utils::settings('mail.ticketsForwarders');

        if ($forwarders) {
            $mailerService = new Mailer();
            $mailerService->addMessage([
                'to' => $forwarders,
                'subject' => $subject,
                'body' => $body
            ]);
            $mailerService->send();
        }
    }
}
