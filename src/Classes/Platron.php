<?php

/**
 * Class Platron
 */
class Platron
{
    /**
     * @var
     */
    private $options;
    /**
     * @var null|mixed
     */
    private $container;

    /**
     * Platron constructor.
     * @param null $c
     */
    public function __construct($c = null)
    {
        if (!is_null($c)) {
            $this->container = $c;
        }

        $settings = $this->container->settings['platron'];
        $this->options['pg_merchant_id'] = $settings['merchid'];
        $this->options['pg_payment_system'] = $settings['pay_system'];

        if ($this->container->settings['debugMode'] === true && $this->container->request->getParam('test') == 1) {
            $this->options['pg_testing_mode'] = 1;
            $this->options['pg_payment_system'] = $settings['test']['pay_system'];
        }
    }

    /**
     * @param $params
     * @return array|bool
     */
    public function initPayment($params)
    {
        Logger::info('Platron::initPayment', 'Init payment with params ' . json_encode($params));

        $request_params = $this->options;
        $request_params['pg_order_id'] = $params['order_id'];
        $request_params['pg_amount'] = $params['sum'];
        $request_params['pg_description'] = $params['desc'];
        $request_params['pg_lifetime'] = Utils::settings('crocusApi.block_time');

        if (isset($params['check_url'])) {
            $request_params['pg_check_url'] = $params['check_url'];
            $request_params['pg_request_method '] = 'POST';
        }
        if (isset($params['success_url'])) {
            $request_params['pg_success_url'] = $params['success_url'];
            $request_params['pg_success_url_method '] = 'POST';
        }
        if (isset($params['result_url'])) {
            $request_params['pg_result_url'] = $params['result_url'];
        }
        if (isset($params['user_hash'])) {
            $request_params['user_hash'] = $params['user_hash'];
        }
        if (isset($params['client_phone'])) {
            $request_params['pg_user_phone'] = $params['client_phone'];
        }
        if (isset($params['client_email'])) {
            $request_params['pg_user_contact_email'] = $params['client_email'];
        }
        $request_params['pg_salt'] = $this->getSalt();
        $request_params['pg_sig'] = $this->getSignature($request_params);
        $response = $this->sendRequest($request_params);

        $result = [];
        if ($response === false) {
            return false;
        }

        if ($response->pg_status == PAY_STATUS_OK) {
            $result['pl_id'] = (int)$response->pg_payment_id;
            if (isset($response->pg_redirect_url)) {
                $result['redirect_url'] = (string)$response->pg_redirect_url;
            }
        } else {
            $result['err_msg'] = $response->pg_error_description;
        }

        Logger::logPlatronTransaction(
            $params['order_id'],
            isset($result['pl_id']) ? $result['pl_id'] : null,
            Utils::settings('platron.surl'),
            json_encode($request_params),
            json_encode($response),
            PAY_REQUEST_TYPE_GET_TRANSACTION_ID,
            REQUEST_DIRECTION_OUTCOME
        );

        return $result;
    }

    /**
     * @param $params
     * @param $script_name
     * @return string
     */
    public function buildResponse($params, $script_name)
    {
        $request_params = $params;
        $request_params['pg_salt'] = $this->getSalt();
        $request_params['pg_sig'] = $this->getSignature($request_params, $script_name);

        $response = new DOMDocument('1.0', 'utf-8');
        $root_element = $response->createElement('response');
        $root_element = $response->appendChild($root_element);

        foreach ($request_params as $r => $request_param) {
            if (!is_array($request_param)) {
                $root_element->appendChild($response->createElement($r, $request_param));
            }
        }
        return $response->saveXML();
    }

    /**
     * @param array $request_params
     * @return bool|SimpleXMLElement|stdClass
     */
    private function sendRequest(Array $request_params)
    {
        if (Utils::settings('platron.send') == true) {
            $cl = curl_init();
            curl_setopt_array($cl, [
                CURLOPT_URL => Utils::settings('platron.surl'),
                CURLOPT_HEADER => 0,
                CURLOPT_FRESH_CONNECT => 1,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $request_params,
                CURLOPT_RETURNTRANSFER => 1,
                CURLINFO_HEADER_OUT => 1
            ]);
            $params_info = curl_getinfo($cl);
            $response = curl_exec($cl);
            $curl_error = curl_error($cl);
            curl_close($cl);

            if ($response === false) {
                Logger::error('Platron::sendRequest', 'Error CURL-request with params ' . json_encode($params_info) . ': ' . $curl_error);
                return false;
            } else {
                Logger::info('Platron::sendRequest', 'Result payment with params ' . json_encode($request_params) . ': ' . json_encode($response));
                $data = simplexml_load_string($response);
                return $data;
            }
        } else {
            $data = new stdClass();
            $data->pg_status = PAY_STATUS_OK;
            $data->pg_payment_id = '999999';
            return $data;
        }
    }

    /**
     * @return array|bool
     */
    public function getTransactionsLog()
    {
        $transactions = Logger::getPlatronTransactions();
        if ($transactions !== false) {
            return $transactions;
        } else {
            return [];
        }
    }

    /**
     * @return array
     */
    public function getLog()
    {
        /** @var Slim\PDO\Database $db */
        $db = $this->container->db;
        return $db
            ->select()
            ->from(TABLE_PLATRON_LOG)
            ->orderBy('id', 'DESC')
            ->execute()
            ->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @return string
     */
    private function getSalt()
    {
        return md5(mt_rand());
    }

    /**
     * @param $params
     * @param null $script_name
     * @return string
     */
    private function getSignature($params, $script_name = null)
    {
        if (isset($params['pg_sig'])) {
            unset($params['pg_sig']);
        }

        ksort($params, SORT_STRING);
        $secretKey = Utils::settings('platron.skey');
        if (is_null($script_name)) {
            $script_name = basename(Utils::settings('platron.surl'));
        }

        $sig = md5(
            implode(';', [$script_name, implode(';', $params), $secretKey])
        );

        return $sig;
    }
}
