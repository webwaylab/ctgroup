<?php

/**
 * Class I18N
 */
class I18N
{
    /**
     * @var self
     */
    private static $instance;

    /**
     * @var string
     */
    private $current_language;
    /**
     * @var array
     */
    private $enabled_languages;

    /**
     * I18N constructor.
     * @param array $enabled_languages
     */
    private function __construct($enabled_languages = [])
    {
        $this->enabled_languages = $enabled_languages;
    }

    /**
     * @return I18N
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @param string $lang
     * @return mixed
     */
    public static function setLanguage($lang)
    {
        self::getInstance()->current_language = $lang;
        return self::getTranslation($lang);
    }

    /**
     * @return string
     */
    public static function getLanguage()
    {
        return self::getInstance()->current_language;
    }

    /**
     * @param string $lang
     * @return mixed
     */
    public static function getTranslation($lang)
    {
        $instance = self::getInstance();
        if (!isset($instance->enabled_languages[$lang])) {
            $instance->enabled_languages[$lang] = $instance->loadTranslations($lang);
        }
        return $instance->enabled_languages[$lang];
    }

    /**
     * @param array $translation
     */
    public static function setTranslation($translation = [], $lang = null)
    {
        $instance = self::getInstance();
        if (is_null($lang)) {
            $lang = self::getLanguage();
        }
        if (!empty($translation)) {
            $instance->enabled_languages[$lang] = $translation;
        }
    }

    /**
     * @param string $lang
     * @return bool|string
     */
    private function loadTranslations($lang)
    {
        $filepath = Utils::settings('languages.path');
        $filename = sprintf('%s%s.json', $filepath, $lang);
        if (file_exists($filename)) {
            $translation = json_decode(file_get_contents($filename), true);
            if (json_last_error() !== 0) {
                return json_last_error_msg();
            } else {
                return $translation['i18n'];
            }
        } else {
            return false;
        }
    }

    /**
     * @param string $phrase
     * @return mixed|string
     */
    public static function translation($phrase)
    {
        $instance = self::getInstance();

        if (!empty($instance->current_language)) {
            $lang = $instance->current_language;
        } else {
            $lang = Utils::settings('languages')['default'];
            if (!isset($instance->enabled_languages[$lang])) {
                if ($instance->setLanguage($lang) === false) {
                    return 'No active translation found';
                }
            }
        }
        $text = Message::getInstance()->get_message($instance->enabled_languages[$lang][$phrase]);
        return $text;
    }
}