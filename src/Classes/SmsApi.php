<?php

/**
 * Class SmsApi
 *
 * Одиночка для расширения \Zhukmax\Smsc
 */
class SmsApi
{
    /**
     * @var \Zhukmax\Smsc\Api
     */
    private static $_instance;

    /**
     * SmsApi constructor.
     */
    private function __construct()
    {}

    protected function __clone()
    {}

    /**
     * @return \Zhukmax\Smsc\Api
     */
    public static function getInstance()
    {
        if(is_null(self::$_instance)) {
            self::$_instance = new \Zhukmax\Smsc\Api(
                Utils::settings('smsc.login'),
                Utils::settings('smsc.password'),
                [
                    'from'  => Utils::settings('smsc.smtp_from'),
                    'debug' => false,
                    'sender' => Utils::settings('smsc.sender')
                ]
            );
        }

        return self::$_instance;
    }
}
