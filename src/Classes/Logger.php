<?php

/**
 * Class Logger
 */
class Logger
{
    const TYPE_INFO = 10;
    const TYPE_ERROR = 20;

    /**
     * @var \Slim\Container
     */
    private $container = null;
    /**
     * @var Logger
     */
    private static $instance = null;

    /**
     * Logger constructor.
     * @param \Slim\Container $container
     */
    private function __construct($container = null)
    {
        if ($container instanceof \Slim\Container) {
            $this->container = $container;
        }
    }

    /**
     * @param \Slim\Container $container
     * @return Logger
     */
    public static function getInstance($container = null)
    {
        if (null === self::$instance) {
            self::$instance = new static($container);
        }

        return self::$instance;
    }

    /**
     * @param $phone
     * @param $message
     * @param $status
     * @return mixed
     */
    public static function logSmsMessage($phone, $message, $status)
    {
        $instance = self::getInstance();
        /** @var \Slim\PDO\Database $pdo */
        $pdo = $instance->container->db;
        $statement = $pdo
            ->insert(['phone', 'message', 'status'])
            ->into('sms_messages_log')
            ->values([$phone, $message, $status]);
        Utils::executeQuery($statement);
        return $statement;
    }

    /**
     * @param $params
     * @param string $act
     * @return PDOStatement
     */
    public static function logEmailMessage($params, $act = 'create')
    {
        $instance = self::getInstance();
        /** @var \Slim\PDO\Database $pdo */
        $pdo = $instance->container->db;
        $table = 'email_messages_log';
        switch ($act) {
            case 'create':
                $statement = $pdo
                    ->insert(['email', 'subject', 'message', '`from`', 'status'])
                    ->into($table)
                    ->values([
                        $params['to'],
                        $params['subject'],
                        $params['message'],
                        $params['from'],
                        $params['status'],
                    ]);
                break;
            case 'update':
                $statement = $pdo
                    ->update(['status' => $params['status'], 'datetime_sent' => 'NOW()'])
                    ->table($table)
                    ->where('id', '=', $params['id']);
                break;
        }
        $result = Utils::executeQuery($statement);
        return $result;
    }

    /**
     * @param $phone
     * @param $code
     * @param $status
     * @param string $act
     * @return mixed
     */
    public static function logPhoneCode($phone, $code, $status, $act = 'create')
    {
        $instance = self::getInstance();
        /** @var \Slim\PDO\Database $pdo */
        $pdo = $instance->container->db;
        $table = 'phone_code';
        switch ($act) {
            case 'create':
                $statement = $pdo
                    ->insert(['phone', 'code', 'status'])
                    ->into($table)
                    ->values([$phone, $code, $status]);
                break;
            case 'update':
                $statement = $pdo
                    ->update(['status' => $status])
                    ->table($table)
                    ->whereMany(['phone' => $phone, 'code' => $code], '=')
                    ->where('datetime_sent', '!=', MESSAGE_STATUS_EXPIRED)
                    ->orderBy('datetime_sent', 'DESC');
                break;
        }
        Utils::executeQuery($statement);
        return $statement;
    }

    /**
     * @param $params
     */
    public static function logReserve($params)
    {
        $instance = self::getInstance();
        /** @var \Slim\PDO\Database $pdo */
        $pdo = $instance->container->db;
        $statement = $pdo
            ->insert([
                'reserve_id', 'datetime_until', 'email', 'fio', 'subscribe',
                'phone', 'delivery', 'address', 'user_hash', 'description'
            ])
            ->into('reserve_log')
            ->values([
                $params['code'],
                date('Y-m-d H:i:s', $params['datetime_until']),
                $params['email'],
                $params['fio'],
                $params['subscribe'],
                $params['phone'],
                $params['delivery'],
                $params['address'],
                $params['user_hash'],
                $params['description']
            ]);
        Utils::executeQuery($statement);
    }

    /**
     * @param $params
     */
    public static function logCreateOrderPayment($params)
    {
        $instance = self::getInstance();
        /** @var \Slim\PDO\Database $pdo */
        $pdo = $instance->container->db;
        $statement = $pdo
            ->insert(['order_id', 'payment_id', 'sum', 'description', 'user_hash', 'status', 'fio'])
            ->into(TABLE_PLATRON_LOG)
            ->values([
                $params['order_id'],
                $params['payment_id'],
                $params['sum'],
                $params['desc'],
                $params['user_hash'],
                $params['status'],
                $params['fio']
            ]);
        Utils::executeQuery($statement);
    }

    /**
     * @param $params
     * @return PDOStatement
     */
    public static function logFinishOrderPayment($params)
    {
        $instance = self::getInstance();
        $pdo = $instance->container->db;
        $statement = $pdo
            ->update([
                'pay_system' => $params['pay_system'],
                'status' => $params['status'],
                'datetime_payment' => $params['datetime_payment'],
                'parameters' => $params['parameters']
            ])
            ->table(TABLE_PLATRON_LOG)
            ->whereMany([
                'order_id' => $params['order_id'],
                'payment_id' => $params['payment_id'],
                'status' => PAY_STATUS_WAIT,
                'user_hash' => $params['user_hash']
            ], '=')
            ->orderBy('datetime_created', 'DESC');

        return Utils::executeQuery($statement);
    }

    /**
     * проверка, что мы пытаемся выполнить
     * правильное подтверждение оплаты - защита от прямых ссылок
     * @param $params
     * @return PDOStatement
     */
    public static function checkFinishOrderPayment($params)
    {
        $instance = self::getInstance();
        /** @var \Slim\PDO\Database $pdo */
        $pdo = $instance->container->db;
        $statement = $pdo
            ->select(['id'])
            ->from(TABLE_PLATRON_LOG)
            ->whereMany([
                'order_id' => $params['order_id'],
                'payment_id' => $params['payment_id'],
                'status' => PAY_STATUS_WAIT,
                'user_hash' => $params['user_hash']
            ], '=')
            ->orderBy('datetime_created', 'DESC')
            ->limit(1, 0);

        return Utils::executeQuery($statement);
    }

    /**
     * @param $params
     * @return bool|PDOStatement
     */
    public static function logGetOrderResult($params)
    {
        $instance = self::getInstance();
        /** @var \Slim\PDO\Database $pdo */
        $pdo = $instance->container->db;
        $statement = $pdo
            ->select(['id'])
            ->from(TABLE_TICKET)
            ->where('ticket_id', '=', $params['ticket_id']);
        $result = Utils::executeQuery($statement);
        if ($result && $result->fetchColumn() > 0) {
            return false;
        }

        $statement = $pdo
            ->insert([
                'order_id',
                'ticket_id',
                'ticket_hash',
                'price',
                'buy_datetime',
                'event_id',
                'event_name',
                'event_datetime',
                'segment',
                'row',
                'seat',
                'client_name',
                'client_email',
                'client_phone',
                'parameters'
            ])
            ->into(TABLE_TICKET)
            ->values([
                $params['order_id'],
                $params['ticket_id'],
                md5($params['ticket_id'] . $params['order_id'] . $params['event_id'] . md5(time())),
                $params['price'],
                date('Y-m-d H:i:s', $params['buy_datetime']),
                $params['event_id'],
                $params['event_name'],
                date('Y-m-d H:i:s', $params['event_datetime']),
                $params['segment'],
                $params['row'],
                $params['seat'],
                $params['client_name'],
                $params['client_email'],
                $params['client_phone'],
                json_encode($params['parameters'], JSON_UNESCAPED_UNICODE)
            ]);

        return Utils::executeQuery($statement);
    }

    /**
     * @param $params
     * @return bool|PDOStatement
     */
    public static function logGetOrderResultCoupon($params)
    {
        $instance = self::getInstance();

        /** @var \Slim\PDO\Database $pdo */
        $pdo = $instance->container->db;
        $statement = $pdo
            ->select(['id'])
            ->from(TABLE_COUPONS)
            ->where('coupon_id', '=', $params['coupon_id']);
        $result = Utils::executeQuery($statement);
        if ($result && $result->fetchColumn() > 0) {
            return false;
        }

        $statement = $pdo
            ->insert([
                'order_id',
                'coupon_id',
                'ticket_id',
                'price',
                'consist',
                'coupon_hash'
            ])
            ->into(TABLE_COUPONS)
            ->values([
                $params['order_id'],
                $params['coupon_id'],
                $params['ticket_id'],
                $params['price'],
                $params['consist'],
                md5($params['coupon_id'] . $params['order_id'] . md5(time()))
            ]);

        return Utils::executeQuery($statement);
    }

    /**
     * @param $user_hash
     * @param null $phone
     */
    public static function logUserSession($user_hash, $phone = null)
    {
        $instance = self::getInstance();
        $session = $instance->container->session;
        $session_id = $session->getId();
        $site = $_SERVER['SERVER_NAME'];
        $user_ip = $_SERVER['REMOTE_ADDR'];
        /** @var \Slim\PDO\Database $pdo */
        $pdo = $instance->container->db;
        $statement = $pdo
            ->insert(['user_hash', 'phone', 'ip', 'session_id', 'site'])
            ->into('user_session')
            ->values([$user_hash, $phone, $user_ip, $session_id, $site]);
        Utils::executeQuery($statement);
    }

    /**
     * @param null $phone
     */
    public static function updateLogUserSession($phone = null)
    {
        $instance = self::getInstance();
        /** @var \Slim\PDO\Database $pdo */
        $pdo = $instance->container->db;
        $session = $instance->container->session;
        $segment = $session->getSegment('Crocus');
        $session_id = $session->getId();
        $user_hash = $segment->get('user_hash', false);

        if ($user_hash && $session_id) {
            $statement = $pdo
                ->update(['phone' => $phone])
                ->table('user_session')
                ->whereMany([
                    'session_id' => $session_id,
                    'user_hash' => $user_hash
                ], '=');
            Utils::executeQuery($statement);
        }
    }

    /**
     * @param null $order_id
     * @param null $transaction_id
     * @param null $url
     * @param null $request
     * @param null $response
     * @param null $type
     * @param null $direction
     */
    public static function logPlatronTransaction($order_id = null, $transaction_id = null, $url = null, $request = null, $response = null, $type = null, $direction = null)
    {
        $instance = self::getInstance();
        /** @var \Slim\PDO\Database $pdo */
        $pdo = $instance->container->db;
        $statement = $pdo
            ->insert(['order_id', 'transaction_id', 'url', 'request', 'response', 'type', 'direction'])
            ->into(TABLE_PLATRON_TRANSACTIONS_LOG)
            ->values([$order_id, $transaction_id, $url, $request, $response, $type, $direction]);
        Utils::executeQuery($statement);
    }

    /**
     * @return array|bool
     */
    public static function getPlatronTransactions()
    {
        $instance = self::getInstance();
        /** @var \Slim\PDO\Database $pdo */
        $pdo = $instance->container->db;
        $statement = $pdo
            ->select(['pt.id', 't.event_id', 'pt.order_id', 'pt.transaction_id', 'pt.url', 'pt.request', 'pt.response', 'pt.type', 'pt.direction', 'pt.datetime_update'])
            ->from(TABLE_PLATRON_TRANSACTIONS_LOG . ' pt')
            ->leftJoin(TABLE_TICKET . ' t', 't.order_id', '=', 'pt.order_id');
        $result = Utils::executeQuery($statement);
        if ($result) {
            return $result->fetchAll(PDO::FETCH_ASSOC);
        }

        return false;
    }

    /**
     * @param string $message
     * @param string|null $description
     */
    public static function error($message, $description = null)
    {
        $instance = self::getInstance();
        /** @var \Slim\PDO\Database $pdo */
        $pdo = $instance->container->db;
        $pdo
            ->insert([
                'type_id' => self::TYPE_ERROR,
                'message' => $message,
                'description' => $description && is_string($description) ? $description : json_encode($description),
                'ip' => isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null,
                'created_at' => date('Y-m-d H:i:s'),
            ])
            ->into('log')
            ->execute();
    }

    /**
     * @param string $message
     * @param string|null $description
     */
    public static function info($message, $description = null)
    {
        $instance = self::getInstance();
        /** @var \Slim\PDO\Database $pdo */
        $pdo = $instance->container->db;
        $pdo
            ->insert([
                'type_id' => self::TYPE_INFO,
                'message' => $message,
                'description' => $description && is_string($description) ? $description : json_encode($description),
                'ip' => isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null,
                'created_at' => date('Y-m-d H:i:s'),
            ])
            ->into('log')
            ->execute();
    }

    /**
     * @param $message
     * @param $type
     */
    public static function writeLog($message, $type)
    {
        $data = "[" . date("Y-m-d H:i:s") . "] - " . $message . "\n";
        $logFile = APP_ROOT . '/logs/' . $type . '.log';
        file_put_contents($logFile, $data, FILE_APPEND | LOCK_EX);
    }
}
