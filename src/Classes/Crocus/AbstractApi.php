<?php
namespace Crocus;

/**
 * Class AbstractApi
 * @package Crocus
 */
abstract class AbstractApi
{
    /**
     * @var \Slim\Container
     */
    protected $container;
    /**
     * @var \SoapClient
     */
    protected $soap;
    /**
     * @var
     */
    protected static $instance;
}
