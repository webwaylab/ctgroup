<?php

/**
 * Class Api
 */
class Api extends \Api\Index
{
    /**
     * Api constructor.
     * @param \Slim\Container $c
     * @param null $messageService
     * @param null $mailerService
     * @param null $crocusApi
     */
    public function __construct($c, $messageService = null, $mailerService = null, $crocusApi = null)
    {
        if (!is_null($c)) {
            $this->container = $c;
            $this->session = $this->container->session;
        }
        Logger::getInstance($c);
        $this->initMessageService($messageService);
        $this->initMailerService($mailerService);
        $this->initCrocusApi($crocusApi);
        session_start();
    }

    public function testResult()
    {
        Logger::writeLog('Tickets not save', 'payOrderGetClientResult');
    }

    /**
     * @param array $params
     * @return array
     */
    public function getPhoneCode($params)
    {
        $debugMode = Utils::settings('debugMode');
        $segment = $this->session->getSegment('Crocus');
        $phone_code = $segment->get('phone_code', '');
        $response = [];

        if (!empty($phone_code)) {
            $last_update = $segment->get('phone_code_last_updated');
            if ((time() - $last_update) < Utils::settings('smsc.repeat_interval')) {
                return ['err_msg' => I18N::translation('MSG_PHONE_ALREADY')];
            }
        }

        $phone = $params['phone'];
        if (Utils::validatePhone($phone)) {
            $phone = Utils::getFormattedPhone($phone);
            $segment->set('user_phone', $phone);

            $code = rand(1000, 9999);
            $segment->set('phone_code', $code);
            $segment->set('phone_code_status', 0);
            $segment->set('phone_code_last_updated', time());

            $text = I18N::translation('YOUR_CODE') . $code;
            Logger::logPhoneCode($phone, $code, MESSAGE_STATUS_WAIT);

            $response['interval'] = Utils::settings('smsc.repeat_interval');
            if ($debugMode || Utils::sendSms($phone, $text)) {
                $response['status'] = 1;
            }

            if ($debugMode && Utils::settings('smsc.test.send_code') === true) {
                $response['code'] = $code;
            }
        } else {
            $response = ['err_msg' => I18N::translation('MSG_PHONE_BAD')];
        }

        return $response;
    }

    /**
     * @param $params
     * @return array
     */
    public function checkPhoneCode($params)
    {
        if (!isset($params['code'])) {
            return ['err_msg' => I18N::translation('ERROR_WRONG_CODE')];
        }

        $segment = $this->session->getSegment('Crocus');
        $code = $segment->get('phone_code', '');
        $phone = $segment->get('user_phone');
        $status = $segment->get('phone_code_status', 0);
        $debugMode = Utils::settings('debugMode');

        if ($debugMode) {
            Logger::updateLogUserSession($phone);
            $segment->set('phone_code_status', 1);
            return ['status' => 1];
        } elseif (!empty($code) && $params['code'] == $code && $status !== 1) {
            Logger::logPhoneCode($phone, $code, MESSAGE_STATUS_SUCCESS, 'update');
            Logger::updateLogUserSession($phone);
            $segment->set('phone_code_status', 1);
            return ['status' => 1];
        } else {
            return ['err_msg' => I18N::translation('ERROR_WRONG_CODE')];
        }
    }

    /**
     * @param array $params
     * @return array
     */
    public function getFreePlaces($params = [])
    {
        $this->session->clear();

        $event_data = $this->getEvent(['event' => $params['event']]);
        $schema = $this->getSchema(['id' => $event_data['schema_id']]);
        $freePlaces = [];

        foreach ($schema['segments'] as $segment) {
            $segment_id = $segment['id'];
            $segment_total = 0;
            $segment_price_min = 10000000;
            $segment_price_max = 0;

            foreach ($segment['sectors'] as $sector) {
                $sector_total = 0;
                $sector_price_min = 1000000;
                $sector_price_max = 0;
                $sector_id = $sector['id'];

                $tmpFreePlacesSector = $this->crocusApi->getSectorFreePlaces([
                    'event' => $params['event'],
                    'segment' => $segment_id,
                    'sector' => $sector_id
                ]);

                if (!empty($tmpFreePlacesSector)) {
                    $tmpFreePlacesSector = trim($tmpFreePlacesSector, "\r\n");
                    $tmpFreePlacesRows = explode("\n", $tmpFreePlacesSector);
                    foreach ($tmpFreePlacesRows as $tmpFreePlacesRow) {
                        $tmpFreePlacesRow = trim($tmpFreePlacesRow, "\r");
                        $tmpFreePlacesRowPlaces = explode(":", $tmpFreePlacesRow);
                        $row_id = $tmpFreePlacesRowPlaces[0];
                        $tmpFreePlacesRowPlacesPrices = explode(',', $tmpFreePlacesRowPlaces[1]);
                        $row_total = count($tmpFreePlacesRowPlacesPrices);
                        if ($row_total > 0) {
                            foreach ($tmpFreePlacesRowPlacesPrices as $tmpPlacePrice) {
                                $tmpPlaces = explode("/", $tmpPlacePrice);

                                $place_id = $tmpPlaces[0];
                                $price = (int)$tmpPlaces[1];

                                if ($price < $sector_price_min) {
                                    $sector_price_min = $price;
                                }

                                if ($price > $sector_price_max) {
                                    $sector_price_max = $price;
                                }

                                $freePlaces['segments'][$segment_id]['sectors'][$sector_id]['rows'][$row_id][$place_id] = $price;
                                if (!isset($freePlaces['prices'])) {
                                    $freePlaces['prices'] = [];
                                }
                                if (!in_array($price, $freePlaces['prices'])) {
                                    $freePlaces['prices'][] = $price;
                                }

                            }
                            $sector_total += $row_total;
                        }
                    }
                }
                $freePlaces['segments'][$segment_id]['sectors'][$sector_id]['total'] = $sector_total;
                if ($sector_total > 0) {
                    $freePlaces['segments'][$segment_id]['sectors'][$sector_id]['price'] = ['min' => $sector_price_min, 'max' => $sector_price_max];
                    $segment_total += $sector_total;
                }

                if ($sector_price_min < $segment_price_min) {
                    $segment_price_min = $sector_price_min;
                }

                if ($sector_price_max > $segment_price_max) {
                    $segment_price_max = $sector_price_max;
                }
            }
            $freePlaces['segments'][$segment_id]['total'] = $segment_total;
            if ($segment_total > 0) {
                $freePlaces['segments'][$segment_id]['price'] = ['min' => $segment_price_min, 'max' => $segment_price_max];
                sort($freePlaces['prices'], SORT_NUMERIC);
            }

        }
        $result = $freePlaces;

        return $result;
    }

    /**
     * @param $params
     * @return array|bool
     */
    public function checkPlaces($params)
    {
        $arguments = ['event', 'places'];
        $hash = $this->generateUserHash();
        $this->setUserHash($hash);

        if ($this->checkArguments($arguments, $params)) {
            $allFree = false;
            $request_params = [];
            $request_params['event'] = $params['event'];
            $request_params['hash'] = $hash;
            if (!empty($params['places'])) {
                $max_order = Utils::settings('ticket.max_order');
                if (count($params['places']) > Utils::settings('ticket.max_order')) {
                    return ['err_msg' => $this->messageService->get_message('%MAIN_SITE_MSG_MAX_ORDER%', ['count' => $max_order])];
                }
                foreach ($params['places'] as $place) {
                    $request_params['segment'] = $place['segment'];
                    $request_params['sector'] = $place['sector'];
                    if (isset($place['fan']) && $place['fan'] > 0) {
                        $request_params['row'] = 1;
                        $request_params['place'] = 1;
                    } else {
                        $request_params['row'] = $place['row'];
                        $request_params['place'] = $place['place'];
                    }

                    $result = $this->crocusApi->getPlaceStatus($request_params);
                    if ($result == 0) {
                        $allFree = false;
                        break;
                    } else {
                        $allFree = true;
                    }
                }
            }

            if ($oddSeats = $this->isOddSeats($params)) {
                return $oddSeats;
            }

            return $allFree;
        } else {
            return false;
        }
    }

    /**
     * @param $params
     * @return array
     */
    public function unblockPlaces($params)
    {
        $user_hash = $this->getUserHash();

        if ($this->checkUserHash($user_hash)) {
            $params['hash'] = $user_hash;
            // $this->reloadSession();
            $result = $this->crocusApi->unblockPlaceByUserHash($params);
            return ['status' => $result];
        }

        $this->session->clear();
    }

    /**
     * @param array $params
     * params['fio']
     * params['address'] string обязательный
     * params['email'] string обязательный
     * params['subscribe'] - по умолчанию 0
     * params['delivery'] - необязательный. по умолчанию 0
     * @return array|bool
     */
    public function reservePlaces($params)
    {
        $arguments = ['fio', 'phone', 'address', 'email'];
        $segment = $this->session->getSegment('Crocus');

        $user_hash = $this->getUserHash();
        $params['phone'] = $code = $segment->get('user_phone');
        $params['subscribe'] = Utils::getCheckboxInt($this->container->request->getParam('subscribe'));
        $params['delivery'] = Utils::getCheckboxInt($this->container->request->getParam('delivery'));
        $params['email'] = Utils::validateEmail($params['email']);

        if ($this->checkUserHash($user_hash)) {
            $params['hash'] = $user_hash;
            if ($this->checkArguments($arguments, $params)) {
                $reserved = $this->reserveAllPlaces($params);
                if ($reserved) {
                    if ($reserved['code'] != 0) {
                        $event_id = $segment->get('event_id');
                        $event_data = $this->getEvent(['event' => $event_id]);
                        $event_name = $event_data['name'][I18N::getLanguage()];
                        $event_datetime = date('d.m.Y H:i', $event_data['date_time']);

                        $message = $this->messageService->smsReservePlaces(
                            $reserved['code'],
                            date('d.m.Y, H:i', $reserved['datetime_until']),
                            $event_name,
                            $event_datetime,
                            $segment->get('tickets_count'),
                            Utils::settings('ticket.shop')
                        );

                        Utils::sendSms($params['phone'], $message);

                        Logger::logReserve([
                            'code' => $reserved['code'],
                            'datetime_until' => $reserved['datetime_until'],
                            'email' => $params['email'],
                            'fio' => $params['fio'],
                            'subscribe' => $params['subscribe'],
                            'phone' => $params['phone'],
                            'delivery' => $params['delivery'],
                            'address' => $params['address'],
                            'user_hash' => $user_hash,
                            'description' => json_encode(['event_id' => $event_id, 'event_name' => $event_name, 'count' => $segment->get('tickets_count')], JSON_UNESCAPED_UNICODE)
                        ]);

                        $this->mailerService->addMessage([
                            'to' => $params['email'],
                            'subject' => 'Бронь №' . $reserved['code'] . '. ' . $event_name . ', ' . $event_datetime . ' - ' . Utils::settings('ticket.name') . ' - ' . Utils::settings('ticket.site'),
                            'body' => $this->messageService->mailReservePlaces(
                                $reserved['code'],
                                date('d.m.Y, H:i', $reserved['datetime_until']),
                                $event_name,
                                $event_datetime,
                                $segment->get('tickets_count'),
                                Utils::settings('ticket.shop')
                            )
                        ]);
                        $this->mailerService->send();

                        return $reserved;
                    } else {
                        return ['err_msg' => I18N::translation('ERROR_RESERVE')];
                    }
                } else {
                    return ['err_msg' => I18N::translation('ERROR_RESERVE')];
                }
            }
        } else {
            return ['err_msg' => I18N::translation('ERR_USER_VALIDATION')];
        }
        return false;
    }

    /**
     * @param $params
     * @return array|bool
     */
    public function buyPlaces($params)
    {
        $arguments = ['fio', 'phone', 'email', 'subscribe'];
        $segment = $this->session->getSegment('Crocus');
        $user_hash = $this->getUserHash();
        $phone = $segment->get('user_phone');
        $params['phone'] = $phone;
        $params['subscribe'] = Utils::getCheckboxInt($this->container->request->getParam('subscribe'));
        $params['email'] = Utils::validateEmail($params['email']);
        $params['event'] = $segment->get('event_id');
        $params['insurance'] = $params['insurance'] == 'true' ? true : false;

        if ($this->checkUserHash($user_hash)) {
            if ($this->checkArguments($arguments, $params)) {
                $params['hash'] = $user_hash;
                $request_params = [];
                $request_params['event'] = $params['event'];
                $request_params['hash'] = $params['hash'];
                $request_params['fio'] = $params['fio'];
                $request_params['phone'] = $params['phone'];
                $request_params['email'] = $params['email'];
                $request_params['insurance'] = $params['insurance'];
                $request_params['coupons'] = isset($params['coupons']) ? $params['coupons'] : '';

                $orderId = $this->getOrderId($request_params);

                if (isset($orderId['id']) && $orderId['id'] > 0) {
                    $response = [];

                    $payment = $this->getPaymentID([
                        'order_id' => $orderId['id'],
                        'sum' => $orderId['sum'],
                        'desc' => $orderId['desc'],
                        'check_url' =>
                            $this->container->request->getUri()->getScheme() . '://' .
                            $this->container->request->getUri()->getHost() .
                            $this->container->router->pathFor('api', ['method' => 'checkCanAcceptPayment']) . '?xml=1',
                        'result_url' =>
                            $this->container->request->getUri()->getScheme() . '://' .
                            $this->container->request->getUri()->getHost() .
                            $this->container->router->pathFor('api', ['method' => 'finishPayOrder']) . '?xml=1',
                        'user_hash' => $user_hash,
                        'client_phone' => $params['phone']
                    ]);

                    if ($payment && $payment['pl_id']) {
                        $result = $this->addPlatronId([
                            'order_id' => $orderId['id'],
                            'pl_id' => $payment['pl_id']
                        ]);

                        if ($result == 1) {
                            $response['order_id'] = $orderId['id'];
                            $response['redirect_url'] = isset($payment['redirect_url']) ?
                                $payment['redirect_url'] :
                                '';
                            $response['status'] = 1;
                        }
                    }

                    Logger::logCreateOrderPayment([
                        'order_id' => $orderId['id'],
                        'payment_id' => isset($payment['pl_id']) ? $payment['pl_id'] : null,
                        'sum' => $orderId['sum'],
                        'desc' => $orderId['desc'],
                        'user_hash' => $user_hash,
                        'status' => PAY_STATUS_WAIT,
                        'fio' => $params['fio']
                    ]);

                    return $response;
                } else {
                    return ['err_msg' => I18N::translation('ERROR_BUY')];
                }
            }
            return false;
        } else {
            return ['err_msg' => I18N::translation('ERR_USER_VALIDATION')];
        }
    }
}
