<?php

use \Crocus\AbstractApi;
use \Crocus\SoapException;

/**
 * Class CrocusApi
 */
class CrocusApi extends AbstractApi
{
    /**
     * @param \Slim\Container $container
     * @return CrocusApi
     */
    public static function getInstance($container = null)
    {
        if (null === self::$instance) {
            self::$instance = new static($container);
        }

        return self::$instance;
    }

    /**
     * @return bool
     */
    public function connect()
    {
        $settings = Utils::settings('crocusApi.wsdl');
        $soapParms = [
            'login' => $settings['login'],
            'password' => $settings['password'],
            'connection_timeout' => $settings['connection_timeout'],
            'trace' => true,
            'exceptions' => true,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
            'stream_context' => stream_context_create(array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true,
                ),
            )),
        ];

        try {
            $this->soap = new SoapClient($settings['url'], $soapParms);
        } catch (SoapFault $e) {
            if (!$settings['url2']) {
                Logger::error(
                    'SoapClient Connect Error',
                    "SoapClient Connect Error: {$e->getCode()}\n{$e->getMessage()}"
                );
                return false;
            } else {
                try {
                    $this->soap = new SoapClient($settings['url2'], $soapParms);
                } catch (SoapFault $e) {
                    Logger::error(
                        'SoapClient Connect Error',
                        "SoapClient Connect Error: {$e->getCode()}\n{$e->getMessage()}"
                    );
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param string $method
     * @param array $params
     * @return $this
     */
    private function logMethodCall($method, $params)
    {
        if (false === $this->isMethodLogable($method)) {
            return $this;
        }

        Logger::info('CrocusAPI method call', "CrocusApi::{$method} is called\nParams:\n" . json_encode($params, JSON_UNESCAPED_UNICODE));
        Logger::info('CrocusApi method call', "CrocusApi::{$method} is called\nParams:\n" . print_r($params, true));
        return $this;
    }

    /**
     * @param string $method
     * @return bool
     */
    private function isMethodLogable($method)
    {
        return isset(self::getMethodParameters($method)['log'])
            && self::getMethodParameters($method)['log']
            && Utils::settings('logger.log');
    }

    /**
     * @return int
     */
    public function testConnection()
    {
        $isConnected = $this->connect();
        if (false === $isConnected) {
            Logger::error('CrocusApi::testConnection failed');
            return 0;
        }

        print_r($this->soap->__getFunctions());
        Logger::info('CrocusApi::testConnection succeeded');
        return 1;
    }

    /**
     * @param $method
     * @param array $params
     * @return bool|mixed
     */
    private function callApiMethod($method, $params = [])
    {
        if (is_null($this->soap) && $this->container->settings['crocusApi']['connect'] === true) {
            $this->connect();
        }
//      список ожидаемых аргументов для запроса
        $parameters = self::getMethodParameters($method)['arguments'];
//      список свойств по каждому аргументу
        $argumentsProperties = self::getArguments();
        $outputParams = [];
        if (count($parameters) > 0) {
            foreach ($parameters as $inputParam) {
                $argument = $argumentsProperties[$inputParam];
                if (!isset($params[$inputParam])) {
                    if (isset($argument['default'])) {
                        $value = $argument['default'];
                    } else {
                        return ['result' => 0, 'err_msg' => 'No "' . $inputParam . '" argument'];
                    }
                } else {
                    $value = $params[$inputParam];
                }

                if (isset($argument['type'])) {
                    settype($value, $argument['type']);
                }
                if (isset($argument['format'])) {
                    $value = sprintf($argument['format'], $value);
                }
                $outputParams[$argument['output']] = $value;

            }
        }

        $this->logMethodCall($method, $outputParams);

        try {
            $method_parameters = self::getMethodParameters($method);
            if ($this->container->settings['crocusApi']['connect'] === true) {
                $soapResponse = $this->soap->__soapCall($method, array($outputParams));
                $result = $soapResponse->Out;
            } else {
                if (self::$instance->container->settings['debugMode'] === true) {
                    $result = isset($method_parameters['test']) ? $method_parameters['test'] : false;
                } else {
                    $result = false;
                }
            }

            if (isset($method_parameters['log']) && $method_parameters['log'] && Utils::settings('logger.log')) {
                Logger::info('CrocusApi method was called', "CrocusApi::{$method} was called\nResult:\n" . print_r($result, true));
            }

            return $result;
        } catch (Exception $e) {
            Logger::error('SoapClient Error', "SoapClient Error: {$e->getCode()}\n{$e->getMessage()}; Method: $method");
            return false;
        }
    }

    /**
     * @return array
     */
    public static function getArguments()
    {
        return [
            'event' => [
                'output' => 'EventID',
                'type' => TYPE_INT,
                'format' => '%0' . FORMAT_MERID_LENGTH . 'd'
            ],
            'segment' => [
                'output' => 'SegmentID',
                'type' => TYPE_INT,
                'format' => '%0' . FORMAT_SEGID_LENGTH . 'd'
            ],
            'sector' => [
                'output' => 'SectorID',
                'type' => TYPE_INT
            ],
            'row' => [
                'output' => 'RowNumber',
                'type' => TYPE_INT
            ],
            'place' => [
                'output' => 'PlaceNumber',
                'type' => TYPE_INT
            ],
            'fors' => [
                'output' => 'Minets',
                'default' => 15
            ],
            'hash' => [
                'output' => 'UserHash',
                'type' => TYPE_STRING
            ],
            'fan' => [
                'output' => 'PlaceFanCount',
                'type' => TYPE_INT,
                'default' => 0
            ],
            'fio' => [
                'output' => 'FIO'
            ],
            'phone' => [
                'output' => 'Tel'
            ],
            'address' => [
                'output' => 'Location',
                'default' => ''
            ],
            'email' => [
                'output' => 'Email',
                'type' => TYPE_STRING
            ],
			'insurance' => [
				'output' => 'Insurance'
			],
            'coupons' => [
                'output' => 'Coupons',
                'type' => TYPE_STRING
            ],
            'eventId' => [
                'output' => 'EventID'
            ],
            'delivery' => [
                'output' => 'DeliveryStatus',
                'type' => TYPE_INT,
                'default' => 0
            ],
            'comment' => [
                'output' => 'DeliveryComment',
                'default' => ''
            ],
            'metro' => [
                'output' => 'Metro',
                'default' => ''
            ],
            'gifts' => [
                'output' => 'GiftsNumber',
                'type' => TYPE_INT,
                'default' => 0
            ],
            'subscribe' => [
                'output' => 'NeedSubsribe',
                'type' => TYPE_INT,
                'default' => 0
            ],
            'attach' => [
                'output' => 'Attach',
                'default' => ''
            ],
            'order_id' => [
                'output' => 'PayOrderID'
            ],
            'pl_id' => [
                'output' => 'PlatronID'
            ],
            'pay_system' => [
                'output' => 'PaymentSystem'
            ],
            'parameters' => [
                'output' => 'OtherParametrs',
                'default' => ''
            ],
            'fail_code' => [
                'output' => 'FailureCode'
            ],
            'fail_desc' => [
                'output' => 'FailureDesc'
            ],
            'sum' => [
                'output' => 'Summ'
            ]
        ];
    }

    /**
     * @param null $method
     * @return array|bool|mixed
     */
    private static function getMethodParameters($method = null)
    {
        $array = [
            API_METHOD_BLOCK_PLACE_BY_USER_HASH => [
                'arguments' => [
                    'event',
                    'segment',
                    'sector',
                    'row',
                    'place',
                    'fors',
                    'hash',
                    'fan'
                ],
                'test' => 1,
                'log' => 1
            ],

            API_METHOD_UNBLOCK_ALL_PLACES_BY_USER_HASH => [
                'arguments' => [
                    'hash'
                ],
                'test' => 1,
                'log' => 1
            ],

            API_METHOD_RESERVE_ALL_BLOCK_PLACES_BY_USER_HASH => [
                'arguments' => [
                    'fio',
                    'phone',
                    'address',
                    'email',
                    'delivery',
                    'comment',
                    'hash',
                    'metro',
                    'gifts',
                    'subscribe',
                    'attach'
                ],
                'test' => '000383814;05.07.2016 14:28:34',
                'log' => 1
            ],

            API_METHOD_GET_CURRENT_EVENTS => [
                'arguments' => []/*,
                'test' => file_get_contents(Utils::settings()['api']['events']['test']['cache_file'])*/
            ],

            API_METHOD_GET_SEGMENT_FREE_PLACES_COUNT => [
                'arguments' => [
                    'event'
                ]
            ],
            API_METHOD_GET_SECTOR_FREE_PLACES => [
                'arguments' => [
                    'event',
                    'segment',
                    'sector'
                ],
                'test' => <<<STR
10:11/1000,12/1000,13/2000,14/5000
11:12/2000,13/3000,14/1000
STR
            ],

            API_METHOD_GET_PLACE_STATUS => [
                'arguments' => [
                    'event',
                    'segment',
                    'sector',
                    'row',
                    'place',
                    'hash'
                ],
                'test' => true
            ],

            API_METHOD_CREATE_PAY_ORDER_BY_USER_HASH => [
                'arguments' => [
                    'event',
                    'hash',
                    'fio',
                    'phone',
                    'subscribe',
                    'email',
                    'insurance',
                    'coupons'
                ],
                'test' => '0000666#5000#Мероприятие Тестовое для ВСХ, билетов: 2 (ПАРТЕР ряд 1 место 7, ПАРТЕР ряд 1 место 8)',
                'log' => 1
            ],

            API_METHOD_PAY_ORDER_ADD_PLATRON_ID => [
                'arguments' => [
                    'order_id',
                    'pl_id'
                ],
                'test' => 1,
                'log' => 1
            ],

            API_METHOD_IS_PAY_ORDER_AVAILABLE => [
                'arguments' => [
                    'order_id',
                    'pl_id',
                    'sum'
                ],
                'log' => 1,
                'test' => 1
            ],

            API_METHOD_CONFIRM_PAY_ORDER => [
                'arguments' => [
                    'order_id',
                    'pl_id',
                    'sum',
                    'pay_system',
                    'parameters'
                ],
                'test' => 1,
                'log' => 1
            ],

            API_METHOD_PAY_GET_COUPON_INFORMATION => [
                'arguments' => [
                    'eventId'
                ]
            ],

            /*
            varchar(32) Номер заказа
            varchar(64) Код билета (он же и штрихкодируется EAN13)
            varchar(128) ИНН, КПП, ОГРН
            varchar(128) Юр. адрес Организатора
            varchar(128) Эл. почта клиента
            varchar(16) Дата покупки
            varchar(8) Время покупки
            varchar(16) Цена билета
            varchar(128) Организатор полное наименование
            varchar(10) Мобильный телефон клиента
            varchar(16) Серия (“ББ 000001”)
            varchar(128) Мероприятие наименование
            varchar(4) Возрастное ограничение (“18+”)
            varchar(32) Сегмент наименование
            varchar(32) Ряд
            varchar(32) Место
            varchar(16) Дата мероприятия (06/12/2013)
            varchar(8) Время мероприятия (20:00)
            varchar(16) Стоимость услуги
            varchar(128) Адрес изображения URL (если пустая строка - заглушка)
            int(11) ИД Мероприятия
            сайт
            адрес зала
            */
            API_METHOD_PAY_ORDER_GET_CLIENT_RESULT => [
                'arguments' => [
                    'order_id',
                    'pl_id'
                ],
                'test' => '001254551###1248445426446###ИНН 7728115183###143402, Московская обл.,г. Красногорск, ул. Международная д.18###loookon@yandex.ru###26/08/16###18:57###1###Филиал "ВЕГАС СИТИ" Акционерного общества "КРОКУС ИНТЕРНЭШНЛ" "CROCUS INTERNATIONAL"###+7(906)713-68-67###Alena A###Мероприятие Тестовое для ВСХ###18+###БАЛКОН###6###1###1 сентября 2019 г.###19:30###1###урл###70###http://www.crocus-hall.ru/###143402, МО, Красногорский р-н, г. Красногорск, ул. Международная, д.12###',
                'log' => 1
            ],

            API_METHOD_PAY_ORDER_GET_CLIENT_RESULT_COUPONS => [
                'arguments' => [
                    'order_id',
                    'pl_id'
                ],
                'test' => '001416041###1252445947290###Подарочный талон###1###1',
                'log' => 1
            ],

            API_METHOD_CANCEL_PAY_ORDER => [
                'arguments' => [
                    'order_id',
                    'fail_code',
                    'fail_desc',
                    'pay_system'
                ],
                'log' => 1
            ]
        ];

        if (isset($method)) {
            if (isset($array[$method])) {
                return $array[$method];
            } else {
                return false;
            }
        } else {
            return $array;
        }
    }

    /**
     * @param $params
     * @return bool|mixed
     */
    public function blockPlaceByUserHash($params)
    {
        return $this->callApiMethod(API_METHOD_BLOCK_PLACE_BY_USER_HASH, $params);
    }

    /**
     * @param $params
     * @return bool|mixed
     */
    public function unblockPlaceByUserHash($params)
    {
        return $this->callApiMethod(API_METHOD_UNBLOCK_ALL_PLACES_BY_USER_HASH, $params);
    }

    /**
     * @param $params
     * @return bool|mixed
     */
    public function reserveAllBlockPlacesByUserHash($params)
    {
        return $this->callApiMethod(API_METHOD_RESERVE_ALL_BLOCK_PLACES_BY_USER_HASH, $params);
    }

    /**
     * @return bool|mixed
     */
    public function getCurrentEvents()
    {
        return $this->callApiMethod(API_METHOD_GET_CURRENT_EVENTS);
    }

    /**
     * @param $params
     * @return bool|mixed
     */
    public function getSectorFreePlaces($params)
    {
        return $this->callApiMethod(API_METHOD_GET_SECTOR_FREE_PLACES, $params);
    }

    /**
     * @param $params
     * @return bool|mixed
     */
    public function getPlaceStatus($params)
    {
        return $this->callApiMethod(API_METHOD_GET_PLACE_STATUS, $params);
    }

    /**
     * @param $params
     * @return bool|mixed
     */
    public function createPayOrderByUserHash($params)
    {
        return $this->callApiMethod(API_METHOD_CREATE_PAY_ORDER_BY_USER_HASH, $params);
    }

    /**
     * @param $params
     * @return bool|mixed
     */
    public function isPayOrderAvailable($params)
    {
        return $this->callApiMethod(API_METHOD_IS_PAY_ORDER_AVAILABLE, $params);
    }

    /**
     * @param $params
     * @return bool|mixed
     */
    public function payOrderAddPlatronId($params)
    {
        return $this->callApiMethod(API_METHOD_PAY_ORDER_ADD_PLATRON_ID, $params);
    }

    /**
     * @param $params
     * @return bool|mixed
     */
    public function confirmPayOrder($params)
    {
        return $this->callApiMethod(API_METHOD_CONFIRM_PAY_ORDER, $params);
    }

    /**
     * @param $params
     * @return bool|mixed
     */
    public function cancelPayOrder($params)
    {
        return $this->callApiMethod(API_METHOD_CANCEL_PAY_ORDER, $params);
    }

    /**
     * @param $params
     * @return bool|mixed
     */
    public function payOrderGetClientResult($params)
    {
        return $this->callApiMethod(API_METHOD_PAY_ORDER_GET_CLIENT_RESULT, $params);
    }

    /**
     * @param $params
     * @return bool|mixed
     */
    public function payOrderGetClientResultCoupons($params)
    {
        return $this->callApiMethod(API_METHOD_PAY_ORDER_GET_CLIENT_RESULT_COUPONS, $params);
    }

    /**
     * @param $params
     * @return bool|mixed
     */
    public function getCouponInformation($params)
    {
        return $this->callApiMethod(API_METHOD_PAY_GET_COUPON_INFORMATION, $params);
    }

    /**
     * CrocusApi constructor.
     * @param \Slim\Container $container
     */
    private function __construct($container = null)
    {
        if ($container instanceof \Slim\Container) {
            $this->container = $container;
        }
    }
}