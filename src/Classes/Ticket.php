<?php
use \tklovett\barcodes;

/**
 * Class Ticket
 */
class Ticket
{
    /**
     * @var string
     */
    protected $container;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var string
     */
    protected $hash;

    /**
     * Ticket constructor.
     * @param string $hash
     * @param string $container
     */
    public function __construct($hash, $container = '')
    {
        $this->hash = $hash;
        $this->data['ticket_hash'] = $hash;
        if (!empty($container)) {
            $this->container = $container;
        }
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function getPrintData()
    {
        $data = $this->fetchData($this->statement());
        $data['parameters'] = json_decode($data['parameters'], true);
        $this->data = $data;

        return $this->data;
    }

    /**
     * @param $ticket_id
     * @param int $width
     * @param int $height
     * @param int $angle
     * @return string
     */
    public static function drawCode($ticket_id, $width = 200, $height = 200, $angle = 0)
    {
        $generator = new barcodes\BarcodeGenerator();
        $image = $generator->generate(barcodes\BarcodeType::EAN_13, $ticket_id);
        return $image->toPNG($width, $height, null, $angle, [255, 255, 255]);
    }

    /**
     * @param $statement
     * @return mixed
     * @throws \Slim\Exception\NotFoundException
     */
    protected function fetchData($statement)
    {
        $result = Utils::executeQuery($statement);
        if ($result) {
            $row = $result->fetch(PDO::FETCH_ASSOC);
            if ($row) {
                return $row;
            }
        }

        throw new \Slim\Exception\NotFoundException($this->container->request, $this->container->response);
    }

    /**
     * @return mixed
     */
    protected function statement()
    {
        $pdo = $this->container->db;
        return $pdo->select(['id', 'order_id', 'ticket_id', 'buy_datetime', 'price', 'event_id', 'event_name', 'event_datetime', 'segment', 'row', 'seat', 'series', 'client_email', 'client_phone', 'parameters', 'client_name'])
            ->from('ticket')
            ->where('ticket_hash', '=', $this->hash);
    }
}
