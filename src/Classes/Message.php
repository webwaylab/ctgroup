<?php

/**
 * Class Message
 */
class Message
{
    /**
     * @var Message
     */
    private static $instance;

    /**
     * @return Message
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @param $bron_id
     * @param $date_until
     * @param $event_name
     * @param $event_date
     * @param $count
     * @param $ticket_shop
     * @return mixed
     */
    public function smsReservePlaces($bron_id, $date_until, $event_name, $event_date, $count, $ticket_shop)
    {
        return $this->sms(MSG_RESERVE_PLACES, [
            'bron_id' => $bron_id,
            'date_until' => $date_until,
            'event_name' => $event_name,
            'event_date' => $event_date,
            'count' => $count,
            'ticket_shop' => $ticket_shop
        ]);
    }

    /**
     * @param $order_id
     * @return mixed
     */
    public function smsBuyPlaces($order_id)
    {
        return $this->sms(MSG_BUY_PLACES, ['order_id' => $order_id]);
    }

    /**
     * @param $bron_id
     * @param $date_until
     * @param $event_name
     * @param $event_date
     * @param $count
     * @param $ticket_shop
     * @return mixed
     */
    public function mailReservePlaces($bron_id, $date_until, $event_name, $event_date, $count, $ticket_shop)
    {
        return $this->mail(MSG_RESERVE_PLACES, [
            'bron_id' => $bron_id,
            'date_until' => $date_until,
            'event_name' => $event_name,
            'event_date' => $event_date,
            'count' => $count,
            'ticket_shop' => $ticket_shop
        ]);
    }

    /**
     * @param $order_id
     * @param $event_name
     * @param $event_date
     * @param $ticket_link
     * @param $coupon_link
     * @return mixed
     */
    public function mailBuyPlaces($order_id, $event_name, $event_date, $ticket_link, $coupon_link)
    {
        return $this->mail(MSG_BUY_PLACES, [
            'order_id' => $order_id,
            'event_name' => $event_name,
            'event_date' => $event_date,
            'ticket_link' => $ticket_link,
            'coupon_link' => $coupon_link
        ]);
    }

    /**
     * @param $message_type
     * @param array $params
     * @return mixed
     */
    private function sms($message_type, $params = [])
    {
        switch ($message_type) {
            case MSG_RESERVE_PLACES:
                $message = "%INF_ORDER_NUM_SHORT% %:bron_id%, " .
                    "%WORD_VALID_UNDER% %:date_until%.\n" .
                    "%:event_name%, " .
                    "%:event_date%,\n" .
                    "%:count::count(билет|билета|билетов)%\n" .
                    "%TICKET_SHOP_SHORT% %:ticket_shop% %CROCUS_CITY_HALL_PHONE_SHORT%";
                break;

            case MSG_BUY_PLACES:
                $message =
                    "%MSG_BUY_GOOD_COMPLETED%. " .
                    "%CROCUS_CITY_HALL_PHONE_SHORT%";
                break;
            default:
                $message = '';
                break;
        }

        return $this->get_message($message, $params);
    }

    /**
     * @param $message_type
     * @param array $params
     * @return mixed
     */
    private function mail($message_type, $params = [])
    {
        switch ($message_type) {
            case MSG_RESERVE_PLACES:
                $message =
                    "%MSG_ORDER_GOOD%.\n" .
                    "%:event_name%, %:event_date%.\n" .
                    "%INF_ORDER_NUM_SHORT% %:bron_id%, " .
                    "%MSG_ORDER_UNDER% %:date_until%.\n" .
                    "%:count::count(билет|билета|билетов)%\n" .
                    "%TICKET_SHOP_SHORT% %:ticket_shop% %CROCUS_CITY_HALL_PHONE_SHORT%";
                break;

            case MSG_BUY_PLACES:
                $message =
                    "%MSG_BUY_GOOD%.\n" .
                    "%:event_name%, %:event_date%.\n" .
                    "%MSG_ORDER_NUMBER_SHORT%: %:order_id%.\n" .
                    "%MSG_YOU_CAN_PRINT_SHORT%:\n%:ticket_link%\n\n" .
                    "%MSG_YOU_CAN_PRINT_COUPON%:\n%:coupon_link%\n\n" .
                    "%CALL_CENTER%: %CROCUS_CITY_HALL_PHONE_SHORT%";
                break;
            default:
                $message = '';
                break;
        }

        return $this->get_message($message, $params);
    }

    /**
     * выбирает вариант из массива в зависимости от параметра-числа
     * @param integer $number
     * @param array $endings содержит варианты [single, some, many]
     * @return string
     */
    private function num_ending($number, $endings)
    {
        // $endings = Array('день','дня','дней');
        $num100 = $number % 100;
        $num10 = $number % 10;
        $index = 0;

        if (
            $num100 >= 11 && $num100 <= 20
            ||
            $num10 == 0
            ||
            $num10 >= 5 && $num10 <= 9
        ) {
            $index = 2;
        } else if (
            $num10 >= 2
            &&
            $num10 <= 4
        ) {
            $index = 1;
        } else if ($num10 == 1) {
            $index = 0;
        }
        return $endings[$index];
    }

    /**
     * возвращает обработанное сообщение
     * фразы из перевода заменяет на %PHRASE% на i18n[PHRASE]
     * параметры %:parameter% заменяет на $params[parameter]
     * поддерживает функции:
     *  - count() выбирает один из вариантов, разделенных "|" в зависимости от переданного параметра-числа и возвращает полную фразу
     *  использование:
     *   %:parameter::count(single|some|many)%
     *  пример:
     *   %:tickets::count(билет|билета|билетов)%
     *   если в :tickets передано 4, возвращает "4 билета"
     * @param
     * @param string $message
     * @param array $params
     * @return mixed
     */
    public function get_message($message, $params = [])
    {
        $i = 0;
        $i_max = 10;
        $translation_pattern = '/%([\w\_]+)%/';
        $params_pattern = '/%:([\w\_\-]+)%/';
        $functions_pattern = '/%:(?<param>\w+)::(?<func>\w+)\((?<args>.+)\)%/';

        while (preg_match($translation_pattern, $message) && $i++ <= $i_max) {
            preg_match_all($translation_pattern, $message, $variables);
            if (count($variables)) {
                foreach ($variables[1] as $v => $variable) {
                    $message = str_replace($variables[0][$v], I18N::translation($variable), $message);
                }

            }
        }
        unset($variables);

        preg_match_all($params_pattern, $message, $variables);
        if (count($variables)) {
            foreach ($variables[1] as $v => $variable) {
                if (isset($params[$variable])) {
                    $message = str_replace($variables[0][$v], $params[$variable], $message);
                }
            }

        }
        unset($variables);

        preg_match($functions_pattern, $message, $variables);
        if (count($variables)) {
            switch ($variables['func']) {
                case 'count':
                    $value = $params[$variables['param']];
                    $result_string = $value . ' ' . self::num_ending($value, explode('|', $variables['args']));
                    $message = str_replace($variables[0], $result_string, $message);
                    break;
            }

        }

        return $message;
    }
}
