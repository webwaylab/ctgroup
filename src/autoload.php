<?php

spl_autoload_register(function ($class) {
    $filepath = __DIR__ . '/Classes/' . str_replace('\\', '/', $class) . '.php';
    if (file_exists($filepath)) {
        require_once $filepath;
    }
}, true, true);
