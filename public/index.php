<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

error_reporting(E_ALL);
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
date_default_timezone_set('Europe/Moscow');

define('APP_ROOT', realpath(__DIR__ . '/..'));
define('SRC_DIR', APP_ROOT . '/src');
require_once APP_ROOT . '/vendor/autoload.php';
require SRC_DIR . '/autoload.php';
require_once SRC_DIR . '/settings.php';
if (file_exists(SRC_DIR . '/custom_settings.php')) {
    require_once SRC_DIR . '/custom_settings.php';
    $config['settings'] = array_replace_recursive($config['settings'], $customConfig['settings']);
}

$app = new \Slim\App($config);

require_once SRC_DIR . '/dependencies.php';
require_once SRC_DIR . '/global_defines.php';
Utils::setSettings($app->getContainer()->settings);
// Utils::checkLastQuery($app->getContainer());
Logger::getInstance($app->getContainer());
CrocusApi::getInstance($app->getContainer());

// Контроллер, вызываемый для генерации json-схем из csv-файлов
$app->map(['get', 'post'], '/generate_hall_scheme', function (Request $request, Response $response) {
    $generator = new SchemeGenerator($request);
    $result = $generator->run();

    if (SchemeGenerator::RESULT_OK !== $result['status']) {
        return $response->write($generator->formatError($result['error']));
    }

    if ($request->isPost()) {
        return $response
            ->withHeader('Content-Type', 'application/octet-stream')
            ->withHeader('Content-Description', 'File Transfer')
            ->withHeader('Content-Disposition', 'attachment; filename=' . basename($result['filename']))
            ->withHeader('Content-Transfer-Encoding', 'binary')
            ->withHeader('Content-Length', $result['length'])
            ->withHeader('Expires', '0')
            ->withHeader('Cache-Control', 'must-revalidate')
            ->withHeader('Pragma', 'public')
            ->write($result['content']);
    }

    return $response->write($result);
});

$app->get('/testCrocusConnection', function (Request $request, Response $response, $args) {
    if ($this->settings['debugMode']) {
        $api = CrocusApi::getInstance($this);
        $result = $api->testConnection();
        return $response->write($result);
    }

    return $response->withStatus(404);
});

$app->get('/test/{method}[/]', function (Request $request, Response $response, $args) {
    if ($this->settings['debugMode'] == true) {
        $method = $args['method'];
        $params = $request->getQueryParams();
        $test = new Test($this);

        if (is_callable([$test, $method])) {
            $result = $test->__call($method, $params);
        } else {
            $result = ['err_msg' => "ERROR: test method {$method} is not found"];
        }

        return $response->write($result);
    }

    return $response->withStatus(404);
});

// Вызов методов API
$app->map(['get', 'post'], '/api/{method}', function (Request $request, Response $response, $args) {
    $method = $args['method'];
    $params = $request->getQueryParams();

    if ($request->isPost() && is_array($params) && is_array($request->getParsedBody())) {
        $params = array_merge($params, $request->getParsedBody());
    }

    $lang = isset($params['lang']) ? $params['lang'] : $this->settings['languages']['default'];
    I18N::setLanguage($lang);
    $api = new Api($this);
    if (is_callable([$api, $method])) {
        $result = $api->__call($method, $params);
    } else {
        $result = ['err_msg' => "ERROR: api method {$method} is not found"];
    }

    if (isset($params['raw']) && $params['raw'] == 1) {
        return $response->write($result);
    } else if (isset($params['xml']) && $params['xml'] == 1) {
        return $response->write($result)->withHeader('Content-type', 'text/xml');
    } else {
        return $response->withJson($result, 200, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }
})->setName('api');

// @todo должно браться из базы
$app->get('/schema/svg/{id:[0-9]+}', function (Request $request, Response $response, $args) {
    $result = ["svg" => file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/schema/svg/' . $args['id'] . '.svg')];
    return $response->withJson($result);
});

$app->get('/event/{id:[0-9]+}', function (Request $request, Response $response, $args) {
    return $response->withStatus(301)->withHeader('Location', '/' . $this->settings['languages']['default'] . $request->getUri()->getPath());
});

$app->get('/{lang}/event/{id:[0-9]+}', function (Request $request, Response $response, $args) {
    $lang = $args['lang'];

    if (empty($lang)) {
        $lang = $this->settings['languages']['default'];
        $args['lang'] = $lang;
    }

    $args['i18n'] = I18N::getTranslation($lang);
    $args['ticket_settings'] = $this->settings['ticket'];

    $args['js'][] = Utils::addFileLastVersion('/js/lib/jquery.min.js');
    $args['js'][] = Utils::addFileLastVersion('/js/lib/bootstrap-3.3.6-dist/js/bootstrap.min.js');
    $args['js'][] = Utils::addFileLastVersion('/js/scene.js');

    $args['css'][] = Utils::addFileLastVersion('/css/style.css');
    $this->session->clear();
    $params = $request->getQueryParams();

    $api = new Api($this);
    $event = $api->getEvent(['event' => $args['id']]);

    if (isset($event['schema_id']) && 401 == $event['schema_id']) {
        $args['css'][] = Utils::addFileLastVersion('/css/arena.css');
    }
    if (isset($event['schema_id']) && 412 == $event['schema_id']) {
        $args['css'][] = Utils::addFileLastVersion('/css/412.css');
    }

    $args['insurance_on'] = $event['insurance_on'];
    if ($event['gift'] > 0) {
        $args['gift'] = $api->getCoupon($args['id']);
        $args['gifts_count'] = $event['gift'];
    } else {
        $args['gifts_count'] = 0;
    }
    $args['discount'] = $event['discount'];

    $args['testPlatron'] = Utils::settings('platron.debugMode');

    // Render index view
    if (isset($event['err_msg'])) {
        return $event['err_msg'];
    } else {
        return $this->view->render($response, 'event.twig', $args);
    }
});

$app->get('/ticket/code/{id:[0-9]+}', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    $angle = (isset($params['angle'])) ? (int)$params['angle'] : 0;
    $width = (isset($params['w'])) ? (int)$params['w'] : 200;
    $height = (isset($params['h'])) ? (int)$params['h'] : 200;
    if ((int)$args['id'] != 0) {
        $code = Ticket::drawCode($args['id'], $width, $height, $angle);
        echo $code;
    } else {
        return $response->withStatus(404);
    }

    return $response->withHeader('Content-Type', 'image/png;base64');
});

// Вывод билета для печати
$app->get('/ticket/{id:\w+}', function (Request $request, Response $response, $args) {
    $ticket = new Ticket($args['id'], $this);
    $data = $ticket->getPrintData();
    $template = 'ticket/' . Utils::settings('ticket.system', 'crocus') . '.twig';
    return $this->view->render($response, $template, ['data' => $data]);
})->setName('ticket');

// Вывод талона на подарок для печати
$app->get('/coupon/{id:\w+}', function (Request $request, Response $response, $args) {
    $coupon = new Coupon($args['id'], $this);
    $data = $coupon->getPrintData();
    $template = 'coupon.twig';
    return $this->view->render($response, $template, ['data' => $data]);
})->setName('coupon');

$app->get('/rules/{action:[\w\/]+}[/]', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    $lang = isset($params['lang']) ? $params['lang'] : $this->settings['languages']['default'];
    $args['ticket_settings'] = $this->settings['ticket'];
    // @todo использовать $lang, когда будут правила на других языках
    return $this->view->render($response, 'rules/' . $args['action'] . '.twig', $args);
});

$app->get('/adminarea/platronTransactions', function (Request $request, Response $response, $args) {
    $adminarea = new Adminarea($this);
    if (!$adminarea->isAuthorized()) {
        return $response->withHeader('Location', '/adminarea/login?back=platronTransactions');
    }

    Logger::getInstance($this);
    $platron = new Platron($this);
    $result = $platron->getTransactionsLog();
    $args['transactions'] = $result;
    $args['js'][] = Utils::addFileLastVersion('/js/lib/jquery.min.js');

    return $this->view->render($response, 'adminarea/platron_transactions.twig', $args);
});
$app->get('/adminarea/platronLogs', function (Request $request, Response $response, $args) {
    $adminarea = new Adminarea($this);
    if (!$adminarea->isAuthorized()) {
        return $response->withHeader('Location', '/adminarea/login?back=platronLogs');
    }

    Logger::getInstance($this);
    $platron = new Platron($this);
    $result = $platron->getLog();
    $args['logs'] = $result;
    $args['js'][] = Utils::addFileLastVersion('/js/lib/jquery.min.js');

    return $this->view->render($response, 'adminarea/platron_logs.twig', $args);
});
$app->get('/adminarea/tickets', function (Request $request, Response $response, $args) {
    $adminarea = new Adminarea($this);
    if (!$adminarea->isAuthorized()) {
        return $response->withHeader('Location', '/adminarea/login?back=tickets');
    }

    $tickets = $adminarea->getTickets($request->getQueryParams());
    return $this->view->render($response, 'adminarea/tickets.twig', [
        'tickets' => $tickets,
        'params' => $request->getQueryParams(),
        'js' => [Utils::addFileLastVersion('/js/lib/jquery.min.js')],
    ]);
});
$app->get('/adminarea/login', function (Request $request, Response $response, $args) {
    $adminarea = new Adminarea($this);
    if ($adminarea->isAuthorized()) {
        return $response->withHeader('Location', '/adminarea');
    }

    return $this->view->render($response, 'adminarea/login.twig', [
        'params' => $request->getQueryParams(),
        'js' => [Utils::addFileLastVersion('/js/lib/jquery.min.js')],
    ]);
});
$app->post('/adminarea/login', function (Request $request, Response $response, $args) {
    $adminarea = new Adminarea($this);
    if ($adminarea->isAuthorized()) {
        return $response->withHeader('Location', '/adminarea');
    }

    $params = $request->getParsedBody();
    if ($params && $adminarea->auth($params)) {
        return $response->withHeader('Location', '/adminarea' . (isset($params['back']) ? '/' . $params['back'] : ''));
    }

    return $this->view->render($response, 'adminarea/login.twig', [
        'params' => $params,
        'js' => [Utils::addFileLastVersion('/js/lib/jquery.min.js')],
    ]);
});

$app->run();
