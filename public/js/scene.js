var URL = {};
var ID = {};
var CLASSES = {};
var STEPS = {};
//свойства для кругляшей-маркеров мест
var OPTIONS = {
    'base': {
        'circle': {
            radius: 2,
            fillColor: '#bbb',
            strokeWidth: 0
        }
    },
    'active': {
        'circle': {
            radius: 4,
            fillColor: 'green',
            strokeWidth: 0
        }
    },
    'hover': {
        'circle': {
            radius: 7,
            strokeWidth: 1,
            fillColor: 'green',
            strokeColor: 'green'
        },
        'text': {
            fillColor: '#1d222e',
            fontFamily: 'arial',
            scale: 0.8
        }
    },
    'selected': {
        'circle': {
            radius: 7,
            strokeWidth: 1,
            fillColor: '#1d222e',
            strokeColor: 'green'
        },
        'text': {
            fillColor: 'black',
            fontFamily: 'arial',
            scale: 0.8
        }
    },
    'small_text': {
        fontFamily: 'Roboto-Regular',
        fontWeight: '',
        fontSize: 12,
        justification: 'center',
        fillColor: '#A7AAB3',
        opacity: 1,
        // leading: 5
    },
    'legend': ['#4EBCBD', '#84C14A', '#E45B8F', '#F2A83B', '#915BE4', '#DAE45B', '#E45B5B']
};
var DEFAULT_LANG = 'ru';

URL.get_event_data = '/api/getEvent';
URL.get_place_status = '/api/getPlaceStatus';
URL.get_schema = '/api/getSchema';
URL.get_schema_svg = '/schema/svg/';
URL.get_free_places = '/api/getFreePlaces';
URL.check_places = '/api/checkPlaces';
URL.block_places = '/api/blockPlaces';
URL.unblock_places = '/api/unblockPlaces';
URL.reserve_places = '/api/reservePlaces';
URL.buy_places = '/api/buyPlaces';
URL.get_phone_code = '/api/getPhoneCode';
URL.check_code = '/api/checkPhoneCode';
URL.rules = '/rules/';

ID.steps_wrapper = 'ticket-steps__wrapper';
ID.schema_plan = 'ticket-schema_plan';
ID.svg_div = 'ticket-schema_plan_svg';
ID.cnvs_overlay="ticket-schema_plan_canvas__overlay";
ID.cnvs_base="ticket-schema_plan_canvas__base";
ID.cnvs_seats="ticket-schema_plan_canvas__seats";
ID.cnvs_seats_hover="ticket-schema_plan_canvas__seats_hover";
ID.cnvs_seats_selected="ticket-schema_plan_canvas__seats_selected";
ID.cnvs_seats_overlay = "ticket-schema_plan_canvas__seats_overlay";
ID.bubble_seat = 'bubble_seat';
ID.bubble_sector = 'bubble_sector';
ID.block_place = 'ticket_card';
ID.block_contaner = 'ticket_card__container';
ID.schema_mini = 'ticket-schema_mini';
ID.timer ='timer';
ID.ticket_wrapper = 'ticket__wrapper';
ID.main_container = 'ticket-main-container';
ID.ticket_header = 'ticket-event_header';
ID.legend = 'ticket_legend';
ID.loading_message = 'ticket-loading_message';
ID.fan_wrapper = 'no_seats_message__wrapper';
ID.fan_count_input = 'no_seats_message__input';

CLASSES.disabled = 'disabled';
CLASSES.step = 'ticket-step';

STEPS.choose_event = 'choose_event';
STEPS.confirm = 'confirm';
STEPS.choose_action = 'choose-action';
STEPS.confirm_phone = 'confirm-phone';
STEPS.user_contacts_reserve = 'user_contacts_reserve';
STEPS.user_contacts_buy = 'user_contacts_buy';
STEPS.reserved_success = 'reserved_success';
STEPS.pay_confirm = 'pay_confirm';

var Widget = function(id, lang) {
    this.init(id, lang);
};

Widget.prototype = {
    _session_key: '',
    _lang: DEFAULT_LANG,
    _svg: null,

    _canvases: {
        base: null,
        overlay: null,
        seats: null,
        seats_hover: null,
        seats_selected: null
    },
    _cnvs_seats_overlay: null,

    _zoom: {
        current: 1,
        min: 1,
        max: 2.5
    },

    _event_id: null,
    _event_data: null,

    _schema: {
        id: null,
        svg: null,
        element: null,
        data: null,
        width: 0,
        height: 0
    },

    _schema_mini: {
        element: null,
        svg: null,
        width: 0,
        height: 0,
        zoom: 1
    },

    _free_places: {},
    _selected_places: {},
    _are_places_blocked: false,
    _legend: {
        element: $('#' + ID.legend),
        colors: [],
        legend: {}
    },
//максимальное число билетов, которые можно заказать за раз
    _max_order: 6,

    init: function (id, lang) {
        this._lang = lang || DEFAULT_LANG;
        this._schema.element = $('#' + ID.svg_div);
        this._schema.sector_svg = Snap('#' + ID.svg_div);
        this._schema_mini.element = $('#' + ID.schema_mini);
        this._schema_mini.svg = Snap('#' + ID.schema_mini);

        this._canvases.overlay = new Project(document.getElementById(ID.cnvs_overlay));
        this._canvases.seats_selected = new Project(document.getElementById(ID.cnvs_seats_selected));
        this._canvases.seats_hover = new Project(document.getElementById(ID.cnvs_seats_hover));
        this._canvases.seats = new Project(document.getElementById(ID.cnvs_seats));
        this._canvases.base = new Project(document.getElementById(ID.cnvs_base));

        this._cnvs_seats_overlay = $('#' + ID.cnvs_seats_overlay);
        this._event_id = id;
        this._bubble_seat = new Bubble(ID.bubble_seat);
        this._bubble_sector = new Bubble(ID.bubble_sector);
        this._block_place = new Block(ID.block_place, ID.block_contaner);

        this._fan_wrapper = $('#' + ID.fan_wrapper);
        this._fan_count_input = $('#' + ID.fan_count_input);

        /* выставляем размеры для контейнеров */
        var ticket_wrapper = $('#' + ID.ticket_wrapper);

        var steps_wrapper = $('#' + ID.steps_wrapper);
        var w2 = steps_wrapper.outerWidth() - steps_wrapper.width();
        var h2 = steps_wrapper.outerHeight() - steps_wrapper.height();

        var header = $('#' + ID.ticket_header);
        var w = ticket_wrapper.width() - w2;
        var h = ticket_wrapper.height() - header.outerHeight() - h2;


        var steps = $('.' + CLASSES.step);
        steps.outerWidth(w);
        steps.outerHeight(h);

        this.setDimensions(w, h);

        this._timer = new Timer(ID.timer);
        this.getEventData();
    },

    getEventData: function() {
        var _this = this;
        getAjax({
            url: URL.get_event_data,
            data: {event: _this._event_id},
            datatype: 'json'
        })
            .done(function(data) {
                _this._event_data = data;
                _this._schema.id = data.schema_id;

                _this.fillEventInfo();
                _this.getSchema();
            })

            .fail(function() {
                console.log('error');
            });
    },

    getSchema: function() {
        var _this = this;
        getAjax({
            url: URL.get_schema,
            data: {id: _this._schema.id},
            datatype: 'json'
        })
            .done(function(data) {
                _this._schema.data = data;

                if (_this._schema.data.hasOwnProperty('width') && _this._schema.data.hasOwnProperty('height')) {
                    _this._schema.data.width += 100;
                    _this._zoom.min = _this._schema.width  / _this._schema.data.width;
                    _this._schema_mini.zoom = _this._schema_mini.width / _this._schema.data.width;
                }
                _this.drawSchema();
                //if (!Modernizr.touchevents) {
                    _this.addHoverSectors();
                    _this.bindFanClickEvent();
                //}
                _this.zoom('out');

                _this.getFreePlaces();

            })
            .fail(function() {
                console.log('error');
            });
    },

    drawSchema: function() {
        var _this = this;
        var data = _this._schema.data;
        _this._canvases.base.activate();

        //расставляем все места
        for (var d in data.seats) {
            var seat = data.seats[d];
            var place = new Shape.Circle({
                center: [seat.x, seat.y]
            });
            place.set(OPTIONS.base.circle);
        }


        for (var _seg in _this._schema.data.segments) {
            var _segment = _this._schema.data.segments[_seg];
            var _name = _segment.name;
                if (
                    _segment.hasOwnProperty('x') &&
                    _segment.hasOwnProperty('y') &&
                    _segment.hasOwnProperty('width') &&
                    _segment.hasOwnProperty('height')
                ) {
                    if (_segment.hasOwnProperty('name')) {
                        _name = _segment.name;
                    }
                    var _segment_text = new PointText({
                        point: new Point(_segment.x + _segment.width/2, _segment.y + _segment.height*1.1),
                        content:  _name
                    }).sendToBack();
                    _segment_text.set(OPTIONS.small_text);
                }
        }
        _this._canvases.base.view.draw();
    },

    getSchemaImage: function() {
        var _this = this;
        if (_this._schema.data.hasOwnProperty('background_image')) {
            _this._canvases.base.activate();
            var img = new Raster({
                source: this._schema.data.background_image,
                position: 'center'
            });
            _this._schema.width = img.width;
            _this._schema.height = img.height;
            // _this._canvases.base.view.draw();
        }
    },

    getSchemaSvg: function() {
        var _this = this;
        getAjax({
            url: URL.get_schema_svg + _this._schema.id
        })
            .done(function(data) {
                _this._canvases.base.activate();
                var svg = new Item.importSVG({
                    svg: data['svg']
                });
                // _this._canvases.base.view.draw();
            })
            .fail(function() {
                console.log('error');
            });
    },

    setDimensions: function (w, h) {
        var _this = this;
        _this._schema.width = w;
        _this._schema.height = h;

        _this._cnvs_seats_overlay.width(w).height(h);
        _this._schema_mini.width = _this._schema_mini.element.width();
        _this._schema_mini.height = Math.ceil(_this._schema_mini.width/w*h);
        _this._schema_mini.element.height(_this._schema_mini.height);


        var container = $('#' + ID.schema_plan);

        container.width(w);
        container.height(h);

        for (var c in _this._canvases) {
            _this._canvases[c].view.viewSize = new Size(w, h);
        }
    },

    fillEventInfo: function() {
        var _this = this;
        var event_data = _this._event_data;
        var eventDateTime = new Date(event_data.date_time * 1000);
        var event = {
            title: event_data.name[_this._lang],
            date: eventDateTime.format('d mmmmm', false, _this._lang),
            time: eventDateTime.format('HH:MM', false, _this._lang),
            org_name: event_data.organisation.name,
            org_inn: event_data.organisation.inn,
            org_address: event_data.organisation.address
        };

        bindText(ID.ticket_header, event);
    },

    getFreePlaces: function() {
        var _this = this;

        getAjax({
            url: URL.get_free_places + '?event=' + _this._event_id,
            datatype: 'json'
        }, null, 'Получаем данные о свободных местах', false)
            .done(function(data) {
                toggleLoading(true, 'Расставляем места...');
                _this._free_places = data;
                _this.drawLegend();
                _this.locateFreePlaces();
                _this.bindHoverSectorEvent();
                toggleLoading(false);
                $('.legend_table-prices').css('max-width', '150px');
            })
            .fail(function() {
                console.log('error');
            });
    },

    //расставляем все доступные места
    locateFreePlaces: function() {
        var _this = this;
        _this._canvases.seats.activate();

        for (var _p in _this._schema.data.seats) {
            var _place_base = _this._schema.data.seats[_p];
            var price = null;
            var color = null;

            var segment_id = _place_base.segment_id;
            var sector_id = _place_base.sector_id;

            if (hasNestedProperties(_this._free_places, ['segments',segment_id,'sectors',sector_id])) {
                var segment = findObjects(_this._schema.data.segments, {'id': segment_id}, true);
                var segment_name = segment.name;

                var place_id = segment_id + '_' + sector_id;
                var place_data = {
                    'segment_id': segment_id,
                    'sector_id': sector_id,
                    'segment_name': segment_name
                };

                //если это сегмент помечен как фан-зона
                if (segment.hasOwnProperty('no_seats')
                    &&
                    segment.no_seats == 1) {
                    place_id += '_fan';
                    //todo сделать вывод цены для фан-зоны
                    price = this._free_places.segments[segment_id].sectors[sector_id].rows[row_num][seat_num];

                } else {

                    var row_num = _place_base.row;
                    var seat_num = _place_base.seat;

                    if (hasNestedProperties(_this._free_places, ['segments',segment_id,'sectors',sector_id,'rows',row_num,seat_num].join('.'))) {

                        price = this._free_places.segments[segment_id].sectors[sector_id].rows[row_num][seat_num];
                        place_id += '_' + row_num + '_' + seat_num;

                        place_data.row = row_num;
                        place_data.row_label = _place_base.row_label;
                        place_data.seat = seat_num;
                        place_data.seat_label = _place_base.seat_label;
                    } else {
                        continue;
                    }

                }

                if (price) {
                    color = _this._legend.legend[price];
                    place_data.price = price;
                    place_data.color = color;
                    place_data.place_id = place_id;

                    //маркер на схеме
                    var place = new Shape.Circle({
                        center: [_place_base.x,_place_base.y]
                    });

                    place.set(OPTIONS.active.circle);
                    place.set({fillColor: color});

                    place.data = place_data;
                }
            }


        }
        checkDisabledButton('submit_places', Object.keys(_this._selected_places).length > 0);

        // _this._canvases.seats.view.draw();
    },
    //рисуем легенду
    drawLegend: function() {
        //генерация псевдослучайных цветов,
        // seed - число, чтобы генерировать одинаковые цвета,
        // seedStep - шаг между цветами
        this._legend.colors = randomColor({
            count: hasNestedProperties(this._free_places, 'prices') ? this._free_places.prices.length : 0,
            // luminosity: 'light',
            format: 'rgb',
            hue: 'random',
            seed: 150,
            seedStep: 20
        });

        for (var p in this._free_places.prices) {
            var price = this._free_places.prices[p];
            if (!this._legend.colors[p]) {
                this._legend.colors[p] = OPTIONS.hover.circle.fillColor;
            }
            var color = this._legend.colors[p];
            this._legend.legend[price] = color;

            var legend_element = $('<span></span>', {id: 'ticket_legend_element_'+price})
                .addClass('ticket_legend_element')
                .append($('<span></span>').addClass('legend_i').css('background-color', color))
                .append($('<span></span>').html(price + '&nbsp;' + i18n.INF_PRICE_RUB));

            this._legend.element.append(legend_element);
        }
        this._legend.element.append($('<span></span>', {id: 'ticket_legend_element_0'})
            .addClass('ticket_legend_element')
            .append($('<span></span>').addClass('legend_i').css('background-color', OPTIONS.base.circle.fillColor))
            .append($('<span></span>').text(i18n.INF_UNAVAILABLE))
        );

        var steps_wrapper = $('.' + CLASSES.step);
        var w = steps_wrapper.width();
        var h = steps_wrapper.height() - this._legend.element.outerHeight();
        this.setDimensions(w, h);
    },

    //добавляем возможность перетаскивания холста мышью
    bindDragOverlayEvent: function () {
        var _this = this;
        var viewbox_mini = _this._schema_mini.svg.select('#viewbox_mini');
        var bbox = viewbox_mini.getBBox();
        var x_min = _this._schema.width/2 / _this._zoom.max;
        var x_max = _this._schema.data.width - _this._schema.width/2 / _this._zoom.max;

        var y_min = _this._schema.height/2 / _this._zoom.max;
        var y_max = _this._schema.data.height - _this._schema.height/2 / _this._zoom.max;

        _this._schema_mini.element.mouseenter(function(e) {
            e.stopImmediatePropagation();
            _this.unbindHoverPlaceEvent();
            _this.unbindClickPlaceEvent();

            _this._schema_mini.element.mousedown(function(e){
                e.stopImmediatePropagation();

                var _this_overlay = this;
                var _initX = e.offsetX;
                var _initY = e.offsetY;

                viewbox_mini.transform(Snap.matrix().translate(_initX - bbox.width/2, _initY - bbox.height/2));

                var x = (_initX + bbox.width/2) / _this._schema_mini.zoom;
                var y = (_initY + bbox.height/2) /_this._schema_mini.zoom;

                for (var c in _this._canvases) {
                    var canvas = _this._canvases[c];
                    canvas.view.center = new Point(x, y);
                }

                $('body').addClass('dragger');

                $(_this_overlay).on('mousemove.drag', function (e) {
                    var x_mini = e.offsetX - bbox.width/2;
                    var y_mini = e.offsetY - bbox.height/2;

                    if (x_mini < 0) {
                        x_mini = 0;
                    } else if (x_mini > _this._schema_mini.width - bbox.width) {
                        x_mini =  _this._schema_mini.width - bbox.width;
                    }

                    if (y_mini < 0) {
                        y_mini = 0;
                    } else if (y_mini > _this._schema_mini.height - bbox.height) {
                        y_mini = _this._schema_mini.height - bbox.height;
                    }
                    viewbox_mini.transform(Snap.matrix().translate(x_mini, y_mini));

                    var x = (x_mini + bbox.width/2) / _this._schema_mini.zoom;
                    var y = (y_mini + bbox.height/2) /_this._schema_mini.zoom;

                    for (var c in _this._canvases) {
                        var canvas = _this._canvases[c];
                        canvas.view.center = new Point(x, y);
                    }

                });

                $('body, body *').mouseup(function (e) {
                    $(_this_overlay).off('mousemove.drag');
                    $('body').removeClass('dragger');
                });
            });

        });

        _this._schema_mini.element.mouseleave(function (e) {
            _this.bindHoverPlaceEvent();
            _this.bindClickPlaceEvent();
        });

        _this._cnvs_seats_overlay.mousedown(function(e) {
            e.stopImmediatePropagation();

            var _this_overlay = this;
            var _initX = e.offsetX;
            var _initY = e.offsetY;

            $('body').addClass('dragger');
            $('body').on('select.drag', function (e) {
                e.preventDefault();
                return false;
            });

            $(this).on('mousemove.drag', function (e) {
                var _curX = e.offsetX;
                var _curY = e.offsetY;

                var _dX = _curX - _initX;
                var _dY = _curY - _initY;

                _initX = _curX;
                _initY = _curY;

                var center = _this._canvases.overlay.view.center;
                var x = center.x - _dX;
                var y = center.y - _dY;

                if (x < x_min) {
                    x = x_min;
                } else if (x > x_max) {
                    x = x_max;
                }

                if (y < y_min) {
                    y = y_min;
                } else if (y > y_max){
                    y = y_max;
                }

                for (var c in _this._canvases) {
                    var canvas = _this._canvases[c];
                    canvas.view.center = new Point(x, y);
                }

                var x_mini = center.x * _this._schema_mini.zoom - bbox.width/2;
                var y_mini = center.y * _this._schema_mini.zoom - bbox.height/2;
                viewbox_mini.transform(Snap.matrix().translate(x_mini, y_mini));
            });

            $('body, body *').mouseup(function (e) {
                $(_this_overlay).off('mousemove.drag');
                $('body').removeClass('dragger');
                $('body').off('select.drag');
            });

        });

    },

    unbindDragOverlayEvent: function() {
        var _this = this;
        _this._cnvs_seats_overlay.off('mousedown');
    },

    //добавляем событие при наведении на место

    bindHoverPlaceEvent: function() {
        var is_hovered = false;
        var _this = this;
        _this._cnvs_seats_overlay.on('mousemove click.place tap', function(e) {
            e.stopPropagation();



            var eventPoint = _this._canvases.overlay.view.viewToProject(new Point(e.offsetX, e.offsetY));
            var hitResult =  _this._canvases.seats.hitTest(eventPoint);
            if (hitResult != null) {
                var target = hitResult.item;
                if (!is_hovered || is_hovered !== target) {
                    if (is_hovered) {
                        _this._canvases.seats_hover.clear();
                    }
                    $('body').addClass('pointer');
                    is_hovered = target;
                    var color = target.data.color;

                    _this._canvases.seats_hover.activate();

                    var hoveredGroup = new Group();
                    var center = target.position;
                    var hoveredPlace = new Shape.Circle({
                        center: [center.x, center.y]
                    });
                    hoveredPlace.set(OPTIONS.hover.circle);
                    hoveredPlace.set({fillColor: color, strokeColor: color});

                    var pos = hoveredPlace.bounds.center;

                    var text = new PointText({
                        point: new Point(pos.x, pos.y),
                        content: target.data.seat
                    });
                    text.set(OPTIONS.hover.text);
                    text.fitBounds(hoveredPlace.bounds.scale(OPTIONS.hover.text.scale));

                    hoveredGroup.addChildren([hoveredPlace, text]);
                    hoveredGroup.data = target.data;

                    center = _this._canvases.seats_hover.view.projectToView(pos);
                    _this._bubble_seat.showThis(center, {
                        'row': hoveredGroup.data.row_label + ' ' + hoveredGroup.data.row,
                        'place': hoveredGroup.data.seat_label + ' ' + hoveredGroup.data.seat,
                        'price': hoveredGroup.data.price + ' ' + i18n.INF_PRICE_RUB
                    });
                }
            } else {
                $('body').removeClass('pointer');
                if (is_hovered) {
                    _this._canvases.seats_hover.clear();
                    _this._bubble_seat.hideThis();
                    is_hovered = false;
                }
            }
        });
    },

    unbindHoverPlaceEvent: function () {
       //this._canvases.overlay.view.off('mousemove click tap');
        this._cnvs_seats_overlay.off('mousemove click tap');
    },

    // событие при нажатии на место
    bindClickPlaceEvent: function () {
        var _this = this;

        _this._cnvs_seats_overlay.on('tap click.place', function (e) {
            e.stopPropagation();
            var eventPoint = _this._canvases.overlay.view.viewToProject(new Point(e.offsetX, e.offsetY));
            var hitResult =  _this._canvases.seats_selected.hitTestAll(eventPoint);
            //если попали в выделенное место, удаляем его
            if (hitResult.length != 0) {
                for (var i in hitResult) {
                    if (hitResult[i].item.parent instanceof Group) {
                        var target = hitResult[i].item.parent;
                        var place_id = target.data.segment_id + '_' + target.data.sector_id + '_' + target.data.row + '_' + target.data.seat;
                        _this.removePlace(place_id);
                        _this._canvases.seats_hover.clear();
                        _this._bubble_seat.hideThis();
                        _this._block_place.removeBlock('[data-marker-id=' + target.id + ']');
                        target.remove();
                        $(this).trigger('tap');
                        break;
                    }
                }
            } else {
                //если попали на наведенное место, выделяем его
                var hitResult =  _this._canvases.seats_hover.hitTestAll(eventPoint);
                if (hitResult.length != 0) {
                    for (var i in hitResult) {
                        if (hitResult[i].item.parent instanceof Group) {
                            var selected_count = _this.countSelectedPlaces();
                            //если выбрали больше чем можно
                            if (!_this.checkMaxOrderedPlaces()) {
                                break;
                            }
                            var target = hitResult[i].item.parent;
                            var color = target.data.color;

                            _this._canvases.seats_selected.activate();
                            var center = target.position;
                            var selectedPlace = new Shape.Circle({
                                center: [center.x, center.y]
                            });
                            selectedPlace.set(OPTIONS.selected.circle)
                                         .set({strokeColor: color});

                            var text = new PointText({
                                point: new Point(center.x, center.y),
                                content: target.data.seat
                            });
                            text.set(OPTIONS.selected.text)
                                .set({fillColor: color});
                            text.fitBounds(selectedPlace.bounds.scale(OPTIONS.selected.text.scale));

                            var selectedGroup = new Group();
                            selectedGroup.addChildren([selectedPlace, text]);
                            selectedGroup.copyAttributes(target);
                            target.remove();

                            var sg_data = selectedGroup.data;

                            _this.addPlace(sg_data.place_id, {
                                'id': selectedGroup.id,
                                'segment': sg_data.segment_id,
                                'sector': sg_data.sector_id,
                                'row': sg_data.row,
                                'place': sg_data.seat,
                                'segment_name': sg_data.segment_name,
                                'price': sg_data.price
                            });


                            _this._block_place.addBlock({
                                sector: sg_data.segment_name,
                                place: sg_data.row + ' ' +
                                       sg_data.row_label + ', ' +
                                       sg_data.seat + ' ' +
                                       sg_data.seat_label,
                                price: sg_data.price,
                                marker_id: selectedGroup.id,
                                place_id: sg_data.place_id,
                                color: sg_data.color
                            });
                            break;
                        }
                    }
                }
            }
        });
    },

    unbindClickPlaceEvent: function () {
        this._cnvs_seats_overlay.off('click.place');
        this._cnvs_seats_overlay.off('tap');
    },

    bindFanClickEvent: function() {
        var _this = this;
        $('[data-tickets_count=plus]').click(function() {
            var input = _this._fan_count_input;
            var place_id = input.data('place_id');
            var _place = {
                segment: input.data('segment_id'),
                sector: input.data('sector_id'),
                row: input.data('row'),
                place: input.data('place'),
                fan: 1,
                segment_name: input.data('segment_name')
            };
            var sector = _this._free_places.segments[_place.segment].sectors[_place.sector];
            var price = sector.rows[_place.row][_place.place];
            _place['price'] = price;

            var _fan_total = sector.total;
            if ((_this.isFanPlace(place_id)
                &&
                _fan_total > _this._selected_places[place_id].fan
                ||
                !_this._selected_places.hasOwnProperty(place_id))
                && _this.checkMaxOrderedPlaces()
            ) {
                _this.addPlace(place_id, _place);
                input.val(parseInt(_this._selected_places[place_id].fan));
            }
        });
        
        $('[data-tickets_count=minus]').click(function() {
            var input = _this._fan_count_input;
            var place_id = input.data('place_id');
            _this.removePlace(place_id);
            var _count =
                    _this.isFanPlace(place_id) ?
                    parseInt(_this._selected_places[place_id].fan) :
                    0;
            input.val(_count);
        });

        _this._fan_wrapper.on('hidden.bs.modal', function() {
            var input = _this._fan_count_input;
            var place_id = input.data('place_id');
            var _count =
                    _this.isFanPlace(place_id) ?
                    parseInt(_this._selected_places[place_id].fan) :
                    0;

            if (_count > 0) {
                _this._block_place.addBlock({
                    sector: input.data('segment_name'),
                    place: i18n_count_places(_count),
                    price: _count * input.data('price'),
                    place_id: place_id
                });
            }
        });
    },

    addHoverSectors: function () {
        var _this = this;
        var data = _this._schema.data;

        //размещаем все контуры секторов
        var s = Snap(_this._schema.width, _this._schema.height);
        var matrix = Snap.matrix();
        matrix.scale(_this._zoom.min);

        //размещаем контуры для мини-карты
        var s_mini = Snap(_this._schema_mini.width, _this._schema_mini.height);
        var matrix_mini = Snap.matrix();
        matrix_mini.scale(_this._schema_mini.zoom);

        for (var seg in data.segments) {
            var segment = data.segments[seg];
            for (var sect in segment.sectors) {
                var sector = segment.sectors[sect];
                if (sector.hasOwnProperty('outline')) {
                    var outline = sector.outline;
                    var path =  s.path(Snap.path.map(outline, matrix));
                    var box = path.getBBox();

                    path.attr({
                        cursor: "pointer",
                        stroke: "#2B2A29",
                        fill:"#ffffff",
                        'fill-opacity': 0,
                        'data-segment-id': segment.id,
                        'data-sector-id': sector.id,
                        'data-name': segment.name,
                        'data-center-x': box.cx,
                        'data-center-y': box.cy,
                        'data-no_seats': segment.hasOwnProperty('no_seats') ? segment.no_seats : 0
                    });
                    var path_mini =  s_mini.path(Snap.path.map(outline, matrix_mini));
                    path_mini.attr({
                        stroke: "none",
                        fill: "#bbb"
                    });
                }
            }
        }
        var viewbox_mini = s_mini.rect(
            0,
            0,
            _this._schema_mini.width * _this._zoom.min / _this._zoom.max,
            _this._schema_mini.height * _this._zoom.min / _this._zoom.max
        );
        viewbox_mini.attr({
            id: 'viewbox_mini',
            cursor: "pointer",
            stroke: "orange",
            'stroke-width': 2,
            fill:"#ffffff",
            'fill-opacity': 0
        });
        _this._schema.sector_svg.append(s);
        _this._schema_mini.svg.append(s_mini);
    },

    /*
    добавляем событие при наведении на сектор
    */
    bindHoverSectorEvent: function () {
        var _this = this;
        var data = _this._schema.data;

        _this._schema.element.show();
        _this._schema.sector_svg.selectAll("path").forEach(function(p) {
            var name = p.attr('data-name');
            var segment_id = p.attr('data-segment-id');
            var sector_id = p.attr('data-sector-id');
            var available_string = i18n.INF_TICKETS_NOPLACES;
            var available_sector_places = 0;
            var prices = '';
            var no_seats = parseInt(p.attr('data-no_seats'));

            if (hasNestedProperties(_this._free_places, ['segments',segment_id,'sectors',sector_id,'total'].join('.'))) {
                var sector = _this._free_places.segments[segment_id].sectors[sector_id];
                available_string  = sector.total > 0
                    ? i18n.INF_AVAILABLE_TICKETS + ' ' + sector.total
                    : i18n.INF_TICKETS_NOPLACES;
                prices = sector.total > 0
                    ?  sector.price.min == sector.price.max
                        ? i18n_trans('%:price_min% %INF_PRICE_RUB%', {price_min: sector.price.min})
                        : i18n_trans('%INF_PRICE_START% %:price_min% %INF_PRICE_RUB% %WORD_UNTIL% %:price_max% %INF_PRICE_RUB%', {price_min: sector.price.min, price_max: sector.price.max})
                    : '';
                available_sector_places = sector.total;
            }

            var box = p.getBBox();
            var center = {x: box.cx, y: box.y};

            //if (!Modernizr.touchevents) {
                p.mouseover(
                    function (e) {
                        this.attr({stroke: "orange"});
                        _this._bubble_sector.showThis(center, {
                            'name': name,
                            'available': available_string,
                            'price': prices
                        });
                    });
                p.mouseout(
                    function () {
                        this.attr({stroke: "#2B2A29"});
                        _this._bubble_sector.hideThis();
                    }
                );
            //}
            p.click(function() {
                if (no_seats) {
                    if (available_sector_places > 0) {
                        _this._fan_wrapper.find('.modal-title').text(name);
                        _this._fan_wrapper.modal('show');
                        var _input = _this._fan_count_input;
                        var _id = segment_id + '_' + sector_id;
                        var _count =
                            _this.isFanPlace(_id) ?
                            parseInt(_this._selected_places[_id].fan) :
                            0;
                        _input.val(_count);
                        _input.attr('data-place_id', _id);
                        _input.attr('data-segment_id', segment_id);
                        _input.attr('data-segment_name', name);
                        _input.attr('data-sector_id', sector_id);

                        var price = 0;
                        if (hasNestedProperties(_this._free_places, ['segments',segment_id,'sectors',sector_id,'rows'])) {
                            var row = Object.keys(_this._free_places.segments[segment_id].sectors[sector_id].rows)[0];
                            var place = Object.keys(_this._free_places.segments[segment_id].sectors[sector_id].rows[row])[0];
                            _input.attr('data-row', row);
                            _input.attr('data-place', place);
                            price = _this._free_places.segments[segment_id].sectors[sector_id].rows[row][place];
                            _input.attr('data-price', price);
                        }

                        bindText(_this._fan_wrapper.attr('id'), {
                            available_total: available_sector_places,
                            price: price
                        });
                    }
                } else {
                    var centerZoom = {x: box.cx / _this._zoom.min, y: box.cy / _this._zoom.min};
                    _this.zoom('in', centerZoom);
                }
            });

        });
    },

    /**
     * убираем обводку секоторов
     */
    hideHoverSectors: function () {
        var _this = this;
        this._bubble_sector.hideThis();
        // this._block_place.showBlocks();
        this._schema.element.hide();

        var center = _this._canvases.overlay.view.center;
        var viewbox_mini = _this._schema_mini.svg.select('#viewbox_mini');
        var bbox = viewbox_mini.getBBox();
        var x_mini = center.x * _this._schema_mini.zoom - bbox.width/2;
        var y_mini = center.y * _this._schema_mini.zoom - bbox.height/2;
        viewbox_mini.transform(Snap.matrix().translate(x_mini, y_mini));

        this._schema_mini.element.show();

    },

    /**
     *  добавляем обводку секторов
     */
    showHoverSectors: function () {
        this._schema.element.show();
        // this._block_place.hideBlocks();
        this._schema_mini.element.hide();

    },

    addPlace: function (id, data) {
        var _this = this;
        if (data.fan) {
            if (typeof _this._selected_places[id] !== 'undefined') {
                _this._selected_places[id].fan++;
            } else {
                _this._selected_places[id] = data;
            }
        } else {
            if (typeof _this._selected_places[id] === 'undefined') {
                _this._selected_places[id] = data;
            }
        }
        _this._selected_places[id].id = data.id;
        checkDisabledButton('submit_places', Object.keys(_this._selected_places).length > 0);
    },

    removePlace: function (id) {
        var _this = this;
        if (_this._selected_places.hasOwnProperty(id)) {
            if (_this.isFanPlace(id, 1)) {
                _this._selected_places[id].fan--;
            } else {
                delete _this._selected_places[id];
            }
        }
        checkDisabledButton('submit_places', Object.keys(_this._selected_places).length > 0);
    },

    /**
     * Удалить место из таблицы
     * @param id
     */
    unselectPlace: function (id) {
        var target = this._canvases.seats_selected.getItem({id: id});
        target.remove();
    },

    clearPlaces: function () {
        var _this = this;
        _this._selected_places = {};
        _this._canvases.seats_selected.clear();
        _this._block_place.container.children().remove();
        checkDisabledButton('submit_places', false);
    },

    isPlaceSelected: function (id) {
        var _this = this;
        return _this._selected_places.hasOwnProperty(id);
    },

    isFanPlace: function (id, moreThanValue) {
        var _this = this;
        if (typeof moreThanValue === 'undefined') {
            moreThanValue = 0;
        } else {
            moreThanValue = parseInt(moreThanValue);
        }

        if (_this.isPlaceSelected(id)) {
            var _place = _this._selected_places[id];
            return _place.hasOwnProperty('fan') && _place.fan > moreThanValue;
        } else {
            return false;
        }
    },

    countSelectedPlaces: function () {
        var count = 0;
        var _this = this;
        if (Object.keys(_this._selected_places).length > 0) {
            for (var i in _this._selected_places) {
                if (_this.isFanPlace(i)) {
                    count += parseInt(this._selected_places[i].fan);
                } else {
                    count++;
                }
            }
        }
        return count;
    },

    checkMaxOrderedPlaces: function () {
        var selected_count = this.countSelectedPlaces();
        //если выбрали больше чем можно
        if (selected_count >=  this._max_order) {
            bindText('error_message__container', {
                'err_msg': i18n_trans('%MAIN_SITE_MSG_MAX_ORDER%', {count: this._max_order})
            });
            $('#error_message__wrapper').modal('show');
            return false;
        }
        return true;
    },

    /*
    * строит таблицу выбранных мест для подтверждения и для показа купленных билетов
    * */
    generatePlacesTable: function (container_id, with_delete) {
        with_delete = (typeof with_delete !== 'undefined') ? with_delete : false;
        var _this = this;
        var _table = $('<table></table>').addClass('table table-bordered table-tickets');
        var gift = 0;
        var discount = $('body').data('discount');
        var _row ;
        var price;

        for (var p in _this._selected_places) {
            var place = _this._selected_places[p];
            price = place.price;
            _row = $('<tr></tr>')
                .append($('<td></td>').addClass("media-middle").text(i18n.INF_SEG+' '+place.segment_name));
            if (place.hasOwnProperty('fan')) {
                //правильное окончание для слова "место"
                var fanCount = parseInt(place.fan);
                price = price * fanCount;
                _row.append($('<td></td>'))
                    .append($('<td></td>').addClass("media-middle place_count").text(i18n_count_places(fanCount)));
            } else {
                _row.append($('<td></td>').addClass("media-middle ").text(place.row+' '+i18n.INF_ROW))
                    .append($('<td></td>').addClass("media-middle").text(place.place+' '+i18n.INF_PLACE));
            }

            if (container_id === 'pay_confirm__table' && $('body').data('action') === 'buy') {
                price = Math.floor(price - (price * discount / 100));
            }
            _row.append($('<td></td>').addClass("media-middle price-info").text(
               price  + ' ' + i18n.INF_PRICE_RUB
            ));
            if (with_delete) {
                _row.append(
                    $('<td></td>')
                        .addClass("text-center")
                        .append(
                            $('<a></a>', {
                                'data-remove-place': p,
                                'data-remove-selected-index': place.id,
                                role: 'button'
                            }).addClass('remove_button')
                              .append($('<i></i>').addClass("icon-remove"))
                        )
                );
            }
            _table.append(_row);
        }

        // Добавление строки с подарком
        if (container_id === 'pay_confirm__table' && $('#gift-block').data('on') !== 0) {
            gift = getGiftPrice();
            var count = $('#gift-count')[0].value;
            if (count !== 0) {
                _row = $('<tr></tr>').addClass("pay_confirm__row-gift") .append(
                    $('<td></td>').addClass("media-middle").text(i18n.GIFTS)
                ).append($('<td></td>')).append($('<td></td>').text(count + ' шт.')).append(
                    $('<td></td>').text(gift + ' ' + i18n.INF_PRICE_RUB)
                );
                _table.append(_row);
            }
        }

        // Добавление строки со страховкой
        if (container_id === 'pay_confirm__table' && $('#insurance-acceptance')[0].checked) {
            var insurance = $('#insurance-holder').text();
            _row = $('<tr></tr>') .append(
                $('<td></td>').addClass("media-middle").text(i18n.INSURANCE)
            ).append($('<td></td>')).append($('<td></td>')).append($('<td></td>').text(insurance));
            _table.append(_row);
        }

        // Добавление строки с общей суммой
        if (container_id === 'pay_confirm__table') {
            if ($('#insurance-acceptance')[0].checked) {
                insurance = parseInt(insurance.replace(rub, ""))
            } else {
                insurance = 0;
            }

            var rub = " " + i18n.INF_PRICE_RUB;
            var priceHolders = $('.price-info');
            var amount = 0;
            var amountText;

            if (discount > 0) {
                amountText = i18n.AMOUNT + " со скидкой " + discount + "%";
            } else {
                amountText = i18n.AMOUNT;
            }

            for (var i = 0; i < priceHolders.length; i++) {
                price = parseInt(priceHolders[i].textContent.replace(rub, ""));
                amount = amount + price;
            }

            amount = amount + insurance + gift + i18n.INF_PRICE_RUB;

            _row = $('<tr></tr>') .append(
                $('<td></td>').addClass("media-middle").text(amountText)
            ).append($('<td></td>')).append($('<td></td>')).append($('<td></td>').text(amount));
            _table.append(_row);
        }

        $('#' + container_id).empty().append(_table);
    },

    zoom: function (action, value) {
        $('[data-zoom=' + action+ ']').addClass(CLASSES.disabled);
        $('[data-zoom!=' + action+ ']').removeClass(CLASSES.disabled);
        switch (action) {
            case 'in':
                this._zoom.current = this._zoom.max;
                this.bindDragOverlayEvent();
                //if (!Modernizr.touchevents) {
                    this.bindHoverPlaceEvent();
                //}
                this.bindClickPlaceEvent();
                break;
            case 'out':
                this._zoom.current = this._zoom.min;
                this.unbindDragOverlayEvent();
                //if (!Modernizr.touchevents) {
                    this.unbindHoverPlaceEvent();
                //}
                this.unbindClickPlaceEvent();
                break;
            case 'value':
                var level = (typeof value != 'undefined') ? value : 1;
                this._zoom.current = level;
                break;
            default:
                break;
        }

        for (var c in this._canvases) {
            var canvas = this._canvases[c];
            canvas.view.zoom = this._zoom.current;
            var size = new Point(canvas.view.size);
            var center = new Point(size.x/2, size.y/2);
            if (typeof value == 'object') {
                canvas.view.center = new Point(value);
            } else {
                canvas.view.center = center;
            }
        }
        switch (action) {
            case 'in':
                this.hideHoverSectors();
                break;
            case 'out':
                this.showHoverSectors();
                break;
            default:
                break;
        }
    }
};


var Bubble = function (id) {
    this.id = id;
    this.bubble = $('#' + id);
};

Bubble.prototype = {
    showThis: function(coords, data) {
        var _this = this;
        if (!coords.hasOwnProperty('x') || !coords.hasOwnProperty('y') ) {
            console.log('Wrong input coordinates');
            return false;
        }
        //добавляем данные, если есть
        //подставляем поля в элементы с соответствующим data-bind-text
        if (typeof data !== 'undefined') {
            bindText(this.id, data);
        }

        //устанавливаем координаты после того, как вписали текст, чтобы узнать высоту-ширину
        var top = coords.y - _this.bubble.outerHeight();
        var left = coords.x - _this.bubble.outerWidth()/2;
        _this.bubble.css({top: top, left: left, display: 'inline-block'});
    },
    hideThis: function () {
        this.bubble.hide();
    }
};

var Block = function (id, container) {
    this.id = id;
    this.origin = $('#' + id);
    this.container = $('#' + container);
    this.counter = 0;
};

Block.prototype = {
    addBlock: function(data) {
        var _id = this.id + '-' + this.counter;
        this.counter++;
        var _clone = this.origin.clone().attr({id: _id});
        this.container.append(_clone);

        //добавляем данные, если есть
        //подставляем поля в элементы с соответствующим data-bind-text
        if (typeof data !== 'undefined') {
            bindText(_id, data);
        }
        if (data.hasOwnProperty('place_id')) {
            _clone.attr('data-place-id', data.place_id);
        }
        if (data.hasOwnProperty('marker_id')) {
            _clone.attr('data-marker-id', data.marker_id);
        }
        _clone.css({display: 'inline-block'});
        _clone.find('.ticket_card-price').css('color', data.color);
    },
    removeBlock: function (selector) {
        $(selector).remove();
    },
    hideBlocks: function () {
        this.container.children().hide();
    },
    showBlocks: function() {
        this.container.children().css({display: 'inline-block'});
    }
};

var Timer = function(container) {
    this.timer = $('#' + container);
    this.startMin = parseInt(this.timer.data('minutes'));
    this.reminderMin = parseInt(this.timer.data('reminder-minutes'));
};
Timer.prototype = {
    //переменная для таймера
    _timerInterval: null,
    _isStopped: true,
    showTimer: function () {
        this.timer.show();
        return this;
    },
    hideTimer: function () {
        this.timer.hide();
        return this;
    },

    _formatTime: function (v) {
        if (v < 10) {
            return '0' + v;
        } else {
            return v;
        }
    },
    startTimer: function () {
        var _this = this;
        _this._isStopped = false;

        var min = this.startMin; //начальное число минут
        var sec = 0; //начальное число секунд
        _this.clearTimer();
        var txtMin;
        var txtSec;
        var timeSpan = _this.timer.find('span');

        _this._timerInterval = setInterval(
            function() {
                sec--;
                if (sec < 0) {
                    min--;
                    txtMin = _this._formatTime(min);
                    sec = 59;
                }
                txtSec = _this._formatTime(sec);
                timeSpan.text(['00', txtMin, txtSec].join(':'));

                if (min == _this.reminderMin && sec == 0) {
                    //     if (min == 14 && sec == 58) {
                    timeSpan.animate({
                        'font-size': '+=2',
                        margin: '-=1'
                    }, 300, '', function() {
                        $(this).addClass('alert')
                            .animate({
                                'font-size': '-=2',
                                margin: '+=1'
                            }, 300);

                    });
                }
                if (min == 0 && sec == 0) {
                    _this.stopTimer();
                    cancelProcess({msg: i18n.MSG_PLACES_CANCEL}, _this._isStopped);
                }
            },
            1000);
        return this;
    },
    stopTimer: function () {
        this._isStopped = true;
        if (this._timerInterval != null) {
            clearInterval(this._timerInterval);
        }
        $('form *').val('');
        return this;
    },
    clearTimer: function () {
        this.timer.find('span')
            .text(['00', this._formatTime(this.startMin), '00'].join(':'))
            .removeClass('alert');
        return this;
    }
};

/*
* находит объект/объекты по значению в поле
* */
function findObjects(array, conditionPairs, returnFirst) {
    var result = $.grep(array, function (el) {
        var match = true;
        for (var field in conditionPairs) {
            match = match & (el[field] == conditionPairs[field]);
        }
        return match;
    });

    if (result.length === 0) {
        return false;
    } else {
        if (returnFirst) {
            return result[0];
        } else {
            return result;
        }
    }

}

/**
 * hasNestedProperties check if an object has the nested
 * properties passed in params
 *
 * @param  {Object}  obj         object we want to test
 * @param  {String|Array}  properties  nested property we want
 * @return {Boolean}             either the object has these
 *                               nested properties or not
 */

function hasNestedProperties(obj, properties) {
    if (typeof properties === 'string') {
        properties = properties.split('.');
    }

    var property;

    // For each properties, check if it's an object property
    for(var i = 0, l = properties.length; i < l; i++) {
        property = properties[i];
        if(!obj.hasOwnProperty(property)) {
            return false;
        }
        obj = obj[property];
    }

    return true;
}

/**
* отправляет аякс-запрос в апи и выключает анимацию загрузки
* */
function getAjax(params, submit_btn_id, msg, hide_loading) {
    toggleLoading(true, msg);
    hide_loading = (hide_loading === false) ? hide_loading : true;

    var submit_btn = $('#' + submit_btn_id);
    submit_btn.prop('disabled', true).addClass(CLASSES.disabled);

    var res = $.ajax(params);

    res.done(function(data) {
        checkErrors(data);
    });

    $.when(res).always(function () {
        submit_btn.prop('disabled', false).removeClass(CLASSES.disabled);
        if (hide_loading) {
            toggleLoading(false);
        }
    });
    return res;
}
/**
 * проверяет наличие поля err_msg и выводит его во всплывающем окне
 * */
function hasErrors(data) {
    if (data != null && data.hasOwnProperty('err_msg')) {
        return true;
    }
    return false;

}
function checkErrors(data) {
    if (hasErrors(data)) {
        bindText('error_message__container', data);
        $('#error_message__wrapper').modal('show');
    }
}
/**
* открывает блок с шагом next-step
* */
function goToStep(nextStep) {
    var nextBlock = $('[data-step=' + nextStep + ']');
    $('.' + CLASSES.step).css({'visibility': 'hidden', display: 'none'});
    nextBlock.show().css({visibility: 'visible', display: 'block'});
}

/**
* подставляет текст в соответствующие поля внутри контейнера $('#id')
* значение из ключа key подставляется в элемент с атрибутом data-bind-text=key
* */
function bindText(id, textObj) {
    var container = $('#' + id);
    for (var key in textObj) {
        container
            .find('[data-bind-text=' + key + ']')
            .add(container.filter('[data-bind-text=' + key + ']'))
            .html(textObj[key]);
        container
            .find('[data-bind-href=' + key + ']')
            .add(container.filter('[data-bind-href=' + key + ']'))
            .attr('href', textObj[key]);
    }
    return container;
}

/**
* включает/выключает индикатор загрузки
* */
function toggleLoading(to_show, msg) {
    to_show = to_show || false;
    var loading = $('.loading_overlay');
    var loading_msg = $('.loading_message');

    if (to_show && msg) {
        loading_msg.text(msg);
        loading_msg.show();

        // console.log(msg);
    } else {
        loading_msg.empty().hide();
    }

    if (loading.is(':visible') && to_show === false) {
        loading.hide();
    } else if (loading.is(':hidden') && to_show === true) {
        loading.show();
    }

}
/**
 * проверяет условие condition, если true, то делает кнопку активной, false - деактивирует
 * */
function checkDisabledButton(btn, condition, warning) {
    if (condition === true) {
        $('#' + btn).removeClass(CLASSES.disabled);
        return true;
    } else {
        $('#' + btn).addClass(CLASSES.disabled);
        return false;
    }
}
/**
* проверяет заполненность полей, у которых установлен атрибут data-required=1
* */
function checkRequiredFields(form_id) {
    var form = $('#' + form_id);
    var messages = $('<div></div>', {'data-bind-span': 1});
    var emptyFields = 0;
    form.find('[data-required=1]').each(function() {
        if ($(this).attr('type') == 'checkbox' && !$(this).is(':checked')
            ||
            $(this).val() == '') {
            // console.log($(this));
            emptyFields++;
            messages.append(
                $('<div></div>')
                    .text(i18n_trans(i18n.WORD_PLEASE_FILL_FIELD, {'field': $(this).attr('data-title')}))
                    .addClass("error_message")
            );
        }
    });
    if (emptyFields > 0) {
        $('#error_message__wrapper .modal-body').append(messages);
        $('#error_message__wrapper').modal('show');
        return false;
    }
    return true;
}
/**
 * %KEY% = i18n.KEY
 * :key = params[key]
* */
function i18n_trans(phrase, params) {
    try {
        phrase = phrase.replace(/%([a-zA-Z]\w*)%/g, function(match, p1) {
            if (i18n.hasOwnProperty(p1)) {
                return i18n[p1];
            } else {
                return match;
            }
        });
        phrase = phrase.replace(/%:([a-zA-Z]\w*?)%/g, function(match, p1) {
            if (params.hasOwnProperty(p1)) {
                return params[p1];
            } else {
                return match;
            }
        });
    } catch (e) {
        console.log(e.message);
    }
    return phrase;
}

/**
 * функция для правильного вывода количества
 * @param value int
 * @param strings = {one: "место", some: "места", many: "мест"}
 */
function i18n_count(value, strings) {
    var value = parseInt(value);
    var _tmp = value%10;
    var _textCount = (_tmp === 1) ?
        strings.one :
        (_tmp >= 2 && _tmp <= 4) ?
            strings.some :
            strings.many;

    _textCount = value + ' ' + _textCount;
    return _textCount;
}
/**
 * функция для правильной подстановки количества мест
 * @param value
 */
function i18n_count_places(value) {
    return i18n_count(value, {one: i18n.INF_PLACE_F2, some: i18n.INF_PLACE_F3, many: i18n.INF_PLACE_F1});
}

function cancelProcess(data, timer_stopped) {
    if (data.hasOwnProperty('msg')) {
        bindText('error_message__container', {err_msg: data.msg});
    }

    if ($('#error_message__container .modal-footer').length === 0) {
        var btns = $('<div></div>').addClass("modal-footer")
            .append(
                $('<button></button>', {onclick: "yaCounter41926539.reachGoal('Callbackvegas_CANCEL'); return true;", type: "button", "data-dismiss": "modal", id: "unblock_places_btn"}).addClass("btn btn-success").text(timer_stopped ? "OK" : i18n.MSG_YES)
            );

        if (!timer_stopped) {
            btns.append(
                $('<button></button>', {type: "button", "data-dismiss": "modal"}).addClass("btn btn-error").text(i18n.MSG_NO)
            )
        }
        $('#error_message__container').append(btns);
    }

}

function insuranceOn() {
    var insuranceBlock = $("#insurance-block");
    var insurance_on = insuranceBlock.data("on");

    if (insurance_on === "") {
        insurance_on = 0;
    } else {
        insurance_on = parseInt(insurance_on);
    }

    return insurance_on;
}

/**
 * Подсчитаем и запишем в дата-аттрибут
 * общую сумму билетов
 */
function setAmount() {
    var $body = $('body');
    var amount = 0;
    var discount = $body.data('discount');
    var rub = i18n.INF_PRICE_RUB;

    $('.price-info').each(function() {
        var price = parseInt($(this).text());
        if ($body.data('action') === 'buy') {
            price = Math.floor(price - (price * discount / 100));
            $(this).text(price + " " + rub);
        }

        amount = amount + price;
    });

    $body.data("amount", amount);
}

/**
 * Получить общую сумму билетов
 * @returns {number}
 */
function getAmount() {
    var amount = parseInt($('body').data("amount"));
    return amount;
}

/**
 * Подсчитаем и запишем в дата-аттрибут
 * общую сумму подарков
 */
function setGiftPrice() {
    var giftsPrice = $('#gift-count')[0].value * parseInt($('#gift-holder').text());
    $('body').data("gifts-price", giftsPrice);
}

/**
 * Получить сумму за подарки
 * @returns {Number}
 */
function getGiftPrice() {
    var giftPrice = parseInt($('body').data("gifts-price"));
    if (!giftPrice) {
        giftPrice = 0;
    }
    return giftPrice;
}

function setInsurance(insurance) {
    $('body').data("insurance", insurance);
}

/**
 * Получить сумму страховки
 * @returns {Number}
 */
function getInsurance() {
    var insurance = parseInt($('body').data("insurance"));
    if (!insurance) {
        insurance = 0;
    }
    return insurance;
}

/**
 * Посчитаем и запишем общую сумму
 * с учетом всех изменений
 * типа страховки или подарка
 */
function setTotal() {
    var rub = i18n.INF_PRICE_RUB;
    var amountTd = $('#b-table-td-amount');
    var amount = getAmount() + getGiftPrice() + getInsurance();

    amountTd.text(amount + " " + rub);
}

function setTotalGifts() {
    var rub = i18n.INF_PRICE_RUB;
    var countTd = $('#b-table-tr-gifts-count');
    var amountTd = $('#b-table-tr-gifts-amount');
    var count = $('#gift-count')[0].value;
    var amount = getGiftPrice();

    countTd.text(count + " шт.");
    amountTd.text(amount + " " + rub);
}

/**
 * Сумма страховки
 * @param percent
 * @param init
 */
function insuranceSum(percent, init) {
    init = (typeof init !== 'undefined') ?  init : true;
    var rub = " " + i18n.INF_PRICE_RUB;
    var insurance = 0;

    $('.price-info').after(function() {
        var price = parseInt($(this).text());
        var localInsurance;

        if (price < 60000) {
            localInsurance = Math.floor(price * (percent / 100));
        } else {
            localInsurance = 3600;
        }

        insurance = insurance + localInsurance;

        if (init !== false) {
            return '<td>' +
                '<span class="local-insurance media-middle">' + localInsurance + rub + '</span>' +
                '</td>';
        } else {
            return '';
        }
    });

    $('#insurance-holder').text(insurance + rub);
    return insurance;
}

function insuranceChange() {
    var rub = " " + i18n.INF_PRICE_RUB;
    var acceptance = $('#insurance-acceptance');
    var insuranceHolder = $('#insurance-holder');
    var insurance = parseInt(insuranceHolder.text().replace(rub, ""));

    if (acceptance[0].checked) {
        $('.local-insurance').css('visibility', 'visible');
        setInsurance(insurance);
        setTotal();
    } else {
        $('.local-insurance').css('visibility', 'hidden');
        setInsurance(0);
        setTotal();
    }
}

/**
 * Активирует возможность покупки подарков
 * если это необходимо
 */
function initGifts() {
    var giftBlock = $('#gift-block');
    var on = parseInt(giftBlock.data('on'));
    if (!on) {
        on = 0;
    }

    if (on !== 0) {
        giftBlock.css("display", "block");
        firstTableGifts();
        setGiftPrice();
        setTotalGifts();
    } else {
        giftBlock.css("display", "none");
    }
}

function firstTableGifts() {
    var tbody = "#confirm_places__table > table > tbody";

    var newTr = "<tr class='b-table-tr-gifts'>" +
        "<td>" + i18n.GIFTS + ":</td><td></td><td id='b-table-tr-gifts-count'></td>" +
        "<td id='b-table-tr-gifts-amount'></td>" +
        "<td></td><td></td>" +
        "</tr>";
    $(tbody).append(newTr);
}

/**
 * Вставка суммы в таблицу с выбранными местами
 */
function firstTableAmount() {
    var amount = '';
    var tbody = "#confirm_places__table > table > tbody";
    var discount = $('body').data('discount');
    if (discount > 0) {
        amount = i18n.AMOUNT + " со скидкой " + discount + "%";
    } else {
        amount = i18n.AMOUNT;
    }

    var newTr = "<tr>" +
        "<td>" + amount + ":</td><td></td><td></td><td id='b-table-td-amount'>0</td>" +
        "<td></td><td></td>" +
        "</tr>";
    $(tbody).append(newTr);
}

/**
 * Инициализация вкладки с таблицей
 * выбранных мест для покупки
 */
function initActionBuy() {
    var insuranceBlock = $("#insurance-block");
    var insuranceAcceptance = $("#insurance-acceptance");
    var insurance_on = insuranceOn();
    $('body').data('action', 'buy');

    // Сумма билетов
    setAmount();

    // Инициализация талонов на подаки
    initGifts();

    /**
     * Вкл/выкл страховки при переходе
     * к таблице выбранных мест
     */
    if (insurance_on === 0) {
        insuranceAcceptance.prop("checked", false);
        insuranceBlock.css("display", "none");
    } else {
        insuranceBlock.css("display", "block");
        setInsurance(0);
        insuranceSum(parseInt(insurance_on));
    }

    // Итого
    firstTableAmount();
    setTotal();
}

$(function() {
    var $body = $('body');
    paper.install(window);
    var eventId = $body.data('event-id');
    var lang = $body.data('lang');
    var places_data = {};
    var choose_action = '';
    var choose_action_step = '';
    var user_phone = '';
    var W = new Widget(eventId, lang);
    var rub = i18n.INF_PRICE_RUB;

    // Страховка
    var insuranceBlock = $("#insurance-block");
    var insurance_on = insuranceOn();

    // Событие если выбрали покупку билетов
    $(document).on('click', '#choose-action_buy', initActionBuy);

    // Событие если выбрали бронирование
    $(document).on('click', '#choose-action_reserve', function () {
        insuranceBlock.css("display", "none");
        $('#gift-block').css("display", "none");
        $body.data('action', 'reserve');
    });

    // Событие если изменили кол-во подарков
    $(document).on('change', '#gift-count', function () {
        setGiftPrice();
        setTotal();
        setTotalGifts();
    });

    // Событие на галочку Страховка
    $(document).on('change', '#insurance-acceptance', function () {
        insuranceChange();
    });

    // всплывашки с текстом для правил
    $body.on('click', '.modal-link', function(e) {
        e.stopPropagation();
        var title = $(this).data('title');
        var document = $(this).data('document');
        var source = $(this).data('source');
        var id = 'document_' + document.replace('/', '_');
        var content;

        if ($('#' + id).length) {
            content = $('#' + id);
            content.modal('show');
        } else {
            content = $('.modal[data-link=' + $(this).data('content') + ']').first().clone().attr('id', id).appendTo('body');
            getAjax({
                url: URL.rules + document
            }).done(function(data) {
                if (title) {
                    content.find('.modal-title').text(title.toUpperCase());
                }
                content.find('.modal-body').html(data);
                content.modal('show');
            });
        }
        if (source) {
            content.on('hidden.bs.modal', function(e) {
                $('body').addClass('modal-open');
            });
        }
    });

    //раскрывающиеся поля с адресом
    $('.roll-link').click(function() {
        var title = $(this).data('title');
        var content = $('.roll-content[data-link=' + $(this).data('content') + ']');
        content.slideToggle();
    });
    $('.close-roll-link').click(function() {
        var title = $(this).data('title');
        var content = $('.roll-content[data-link=' + $(this).data('content') + ']');
        content.slideToggle();
        $('#form__user_contacts_reserve__delivery').prop('checked',false);
    });

    $body.on('hidden.bs.modal', function() {
        $('#error_message__container [data-bind-text]').empty();
        $('#error_message__container [data-bind-span]').remove();
    });

    // Удалить места из таблицы подтверждения
    $body.on('click', '[data-remove-place]', function (e) {
        var place_id = $(this).data('remove-place');
        var index = $(this).data('remove-selected-index');
        var rows_count = $(this).parents('table').find('tr').length - 1;
        var place = W._selected_places[place_id];

        W.removePlace(place_id);
        if (index) {
            W.unselectPlace(index);
        }
        if (W.isFanPlace(place_id)) {
            $(this).parents('tr').find('.place_count').text(i18n_count_places(place.fan));
            $('')
        } else {
            W._block_place.removeBlock('[data-place-id=' + place_id + ']');
            $(this).parents('tr').first().remove();
            rows_count--;
        }

        if (rows_count < 0) {
            goToStep(STEPS.choose_event);
            $('#ticket_legend, #ticket_card__container').show();
            $('#submit_places').show();
        }

        // Пересчитаем сумму итого
        setAmount();
        setInsurance(insuranceSum(parseInt(insurance_on), false));
        setTotal();
    });

    //удалить блок с выбранным местом
    $('#ticket_card__container').on('click', '.ticket_card .remove_button', function() {
        var _this = $(this).parents('.ticket_card');
        var id = _this.attr('id');
        var place_id = _this.data('place-id');
        var marker_id = _this.data('marker-id');

        if (W.isFanPlace(place_id, 1)) {
            var count = W._selected_places[place_id].fan;
            var price = W._selected_places[place_id].price;
            count--;
            var card = $('.ticket_card[data-place-id='+place_id+']');
            card.find('[data-bind-text=price]').text(count * price);
            card.find('[data-bind-text=place]').text(i18n_count_places(count));
        } else {
            W._block_place.removeBlock('#' + id);
            if (marker_id) {
                W.unselectPlace(marker_id);
            }
        }
        W.removePlace(place_id);
    });

    //zoom
    $('[data-zoom]').click(function (e) {
        e.preventDefault();
        var action = $(this).data('zoom');
        W.zoom(action);
    });

    $('#submit_places').click(function (e) {
        e.preventDefault();
        var $this = $(this);
        $this.hide();
        $('#ticket_legend, #ticket_card__container').hide();
        $('#ticket-controls a+a').trigger('click');
        if (Object.keys(W._selected_places).length == 0) {
            return false;
        }
        var nextStep = $(this).data('next-step');

        places_data = {
            'event': W._event_id,
            'places': W._selected_places
        };

        getAjax({
            url: URL.check_places,
            method: 'post',
            datatype: 'json',
            data: places_data
        }, 'submit_places', i18n.WAIT_PROCESSING).done(function(data) {
            if (hasErrors(data)) {
                checkErrors(data);
                 $this.show();
				 $('#ticket_legend, #ticket_card__container').show();
                return false;
            }
            W.generatePlacesTable('confirm_places__table', true);
            W.unbindClickPlaceEvent();
            W.unbindDragOverlayEvent();
            W.unbindHoverPlaceEvent();
            goToStep(nextStep);
        });

    });

    $('#confirm_places').click(function(e) {
        e.preventDefault();
        var nextStep = $(this).data('next-step');
        $('.steps').show();

        getAjax({
            url: URL.block_places,
            method: 'post',
            datatype: 'json',
            data: places_data
        }, 'confirm_places', i18n.WAIT_PROCESSING).done(function(data) {
            // console.log(data);

            if (data.status == 1) {
                goToStep(nextStep);
                W._are_places_blocked = true;
                W._timer.showTimer().startTimer();

                if (data.test == 1) {
                    if ($('#choose-action_buy__test').length === 0) {
                        var testBtn = $('#choose-action_buy').clone();
                        $('.choose-action__buttons_buy').append(testBtn);
                        testBtn.attr({
                            id: 'choose-action_buy__test',
                            'data-choose-action': 'buy_test'
                        }).text(i18n_trans('%MAIN_SITE_BUY% в тестовом режиме'));
                    }
                }

            }
        });
    });

    $('#step__choose-action').on('click',
        '#choose-action_buy, #choose-action_reserve, #choose-action_buy__test',
        function(e) {
            e.preventDefault();
            $('.steps .step_item:nth-child(3)').addClass('active');
            var parentStep = $(this).parents('.' + CLASSES.step);
            var nextStep = $(this).data('next-step');
            choose_action = $(this).data('choose-action');
            choose_action_step = $(this).data('choose-step');

            goToStep(nextStep);
            checkDisabledButton('confirm-phone', $('#form__confirm-phone__phone').val() !== '');
        }
    );

    $('#form__confirm-phone__phone').on('change keyup', function () {
        checkDisabledButton('confirm-phone', $(this).val() !== '');
    });

    $('#confirm-phone').click(function(e) {
        e.preventDefault();
        user_phone = $('#form__confirm-phone__phone').val();
        var user_data = $('form#form__confirm-phone').serialize();

        getAjax({
            url: URL.get_phone_code,
            method: 'post',
            datatype: 'json',
            data: user_data
        }, 'confirm-phone', i18n.WAIT_PROCESSING).done(function(data) {
            var checkCodeInput =$('#form__check-code__code');
            if (data.hasOwnProperty('code')) {
                checkCodeInput.val(data.code);
            }

            bindText("confirm-phone__interval", data);
            $("#confirm-phone__interval").show();
            if (data.hasOwnProperty('interval')) {
                var repeat_interval = data.interval;
                var int = setInterval(function() {
                    repeat_interval--;
                    bindText("confirm-phone__interval", {interval: repeat_interval});
                    checkDisabledButton('confirm-phone', repeat_interval <= 0);
                    if (repeat_interval ==0) {
                        clearInterval(int);
                        $("#confirm-phone__interval").hide();
                    }
                }, 1000);
            }

            checkDisabledButton('check-code', checkCodeInput.val() != '');
            checkCodeInput
                .prop('disabled', false)
                .on('keyup change', function(e) {
                    checkDisabledButton('check-code', $(this).val() != '');
                });
            checkCodeInput.focus();
        });
    });

    $('#check-code').click(function(e) {
        e.preventDefault();
        var nextStep = choose_action_step;
        var user_data = $('form#form__check-code').serialize();

        getAjax({
            url: URL.check_code,
            method: 'post',
            datatype: 'json',
            data: user_data
        }).done(function(data) {
            if (data.status == 1) {
                goToStep(nextStep);
                $('#form__' + nextStep + '__phone').val(user_phone);
            }
        });
    });

    $('#user_contacts_reserve, #user_contacts_buy').click(function(e) {
        e.preventDefault();
        var url;
        var id = $(this).attr('id');
        var nextStep = $(this).data('next-step');
        var form = $('form#form__' + id);

        switch (choose_action) {
            case 'reserve':
                url = URL.reserve_places;
                break;
            case 'buy':
                url = URL.buy_places;
                break;
            case 'buy_test':
                url = URL.buy_places + '?test=1';
                break;
            default:
                console.log('Error! Action wasn\'t chosen');
                return;
                break;
        }

        form.find('[data-check-required]').each(function () {
            var required_id = $(this).data('check-required');
            var required_form_element = $('#' + required_id);
            if (required_form_element.is(':checked')) {
                $(this).attr('data-required', 1);
            } else {
                $(this).removeAttr('data-required');
            }
        });

        if (!checkRequiredFields('form__' + id)) {
            return false;
        }

        //связываем информацию из разных полей в одно для ФИО и адреса
        var combines = {};
        form.find('[data-combine]').each(function() {
            var combineField = $(this).data('combine');
            if (!combines.hasOwnProperty(combineField)) {
                combines[combineField] = [];
            }

            var fieldValue = $(this).val();
            combines[combineField].push(fieldValue);

            $(this).prop('disabled', true);
        });

        for (var combineField in combines) {
            var combine = form.find('[name=' + combineField + ']');
            var delimiter = combine.is('[data-combine-delimiter]') ?
                            combine.data('combine-delimiter') :
                            ' ';
            combine.val(combines[combineField].join(delimiter));
        }

        var user_data = form.serialize();
        user_data = user_data + '&insurance=' + $('#insurance-acceptance')[0].checked;
        var countCoupon = parseInt($('#gift-count')[0].value);
        if (countCoupon > 0) {
            var coupon = {
                coupon_id: $('#gift-block').data('coupon'),
                coupon_count: countCoupon,
                coupon_price: parseInt($('#gift-holder').text())
            };
            user_data = user_data + '&coupons=' + JSON.stringify([coupon]);
        }

        getAjax({
            url: url,
            method: 'post',
            datatype: 'json',
            data: user_data
        }, null, i18n.WAIT_PROCESSING).done(function(data) {
            if (data.status == 1) {
                goToStep(nextStep);
                $('.steps .step_item:nth-child(4)').addClass('active');

                switch (choose_action) {
                    case 'reserve':
                        if (data.code != 0) {
                            data.datetime = new Date(data.datetime_until * 1000).format('d.mm.yyyy, HH:MM');
                            bindText('reserved_success__order', data);
                            W.generatePlacesTable('reserved_success__table', false);
                        } else {
                            var err = {'err_msg': 'В процессе бронирования произошла ошибка! Попробуйте повторить ещё раз'};
                            bindText('error_message__container', err);
                            $('#error_message__wrapper').modal('show');
                        }
                        break;
                    case 'buy':
                    case 'buy_test':
                        if ($(this).data('test')) {
                            data.redirect_url = data.redirect_url + '&pg_testing_mode=1';
                        }

                        if (data.status === 1) {
                            bindText('pay_confirm__order', data);
                            bindText('pay_confirm__redirect_url', data);
                            W.generatePlacesTable('pay_confirm__table', false);
                        }
                        break;
                    default:
                        console.log('Error! Action wasn\'t chosen');
                        return;
                        break;
                }
                W._timer.stopTimer().hideTimer();
            }
        });
    });
/*
    $('#pay_confirm__redirect_url').click(function(e) {
        e.preventDefault();
        if ($(this).attr('href').length > 0) {
            var link = $(this).attr('href');
            console.log(link);
        }
    });*/

    $('.submit_button').click(function(e) {
        e.preventDefault();
        if ($(this).hasClass(CLASSES.disabled)) {
            return false;
        }
    });

    $('.cancel_button').click(function(e) {
        e.preventDefault();
        var nextStep = $(this).data('next-step');
        cancelProcess({msg:
            W._timer._isStopped
            ? i18n.MSG_CONFIRM_CANCEL
            : i18n_trans('%MSG_PLACES_CANCEL%<span id="count_tickets"></span><br/>%MSG_CONFIRM_CANCEL%')}, false);
            var count_tickets = W.countSelectedPlaces();
            $('body').find('#count_tickets').html(count_tickets);

        $('#error_message__wrapper').modal('show');
        $('#error_message__wrapper').on('hidden.bs.modal', function(e) {
            $(this).find(".modal-footer").remove();
        });
    });

    $body.on('click', '#bubble_seat', function(event) {
        event.preventDefault();
        /* Act on the event */
        $(this).hide();
    });

    // Событие если выбрали возврат к началу
    $('#error_message__wrapper').on('click', '#unblock_places_btn', function(e) {
        e.preventDefault();
        $('#submit_places').show();
        $('#ticket_legend, #ticket_card__container').show();
        $('.steps').hide();

        $('.steps .step_item').removeClass('active');
        $('.steps .step_item:nth-child(2)').addClass('active');
        goToStep(STEPS.choose_event);

        if (insurance_on !== "0") {
            insuranceBlock.css("display", "block");
            $('#insurance-acceptance').prop("checked", false);
        }

        var stopProcess = function () {
            W._timer.stopTimer().hideTimer();
            W.zoom('out');
            W.clearPlaces();
            $('form [data-combine]').prop('disabled', false);
        };
        stopProcess();

        if (W._are_places_blocked) {
            getAjax({
                url: URL.unblock_places,
                method: 'post',
                datatype: 'json'
            }).done(function(data) {
                W._are_places_blocked = false;
                stopProcess();
            });
        } else {
            stopProcess();
        }
    });

    $('#form__confirm-phone').submit(function (e) {
        e.preventDefault();
        $('#confirm-phone').click();
    });

    $('#form__check-code').submit(function(e) {
        e.preventDefault();
        $('#check-code').click();
    });

    $('#form__user_contacts_reserve').submit(function () {
        e.preventDefault();
        $('#user_contacts_reserve').click();
    });

    $('#form__user_contacts_buy').submit(function () {
        e.preventDefault();
        $('#user_contacts_buy').click();
    });



    /*
    * получить данные по мероприятию
    * получить данные по залу
     * скачать схему зала
     * сделать сектора кликабельными
     * получить данные по свободным местам
     * расставить кресла из данных по залу
     * расставить свободные места
     * сделать их кликабельными со всплывашкой о цене
     * собрать данные по выбранным местам
     * спросить подтверждение выбранных мест
     * спросить бронировать или купить
     * отправить места на блокировку
     * получить ид блокировки
     * показать форму для заполнения контактов
     * принять форму, выслать смс с кодом
     * проверить код, отправить места в резерв
     * получить ид транзакции для отправки в платрон
     * получить статус платежа, выслать пдф
     * */


    $('.info').hover(
        function() {
            $(this).addClass("active");
            $(this).find('.spoiler-body').stop(true, true); // останавливаем всю текущую анимацию
            $(this).find('.spoiler-body').slideDown();
        },
        function() {
            $(this).removeClass("active");
            $(this).find('.spoiler-body').slideUp('fast');
        }
    );

    $('.gift-minus').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 0 ? 0 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.gift-plus').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });

});