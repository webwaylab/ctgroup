<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
date_default_timezone_set('Europe/Moscow');

define('APP_ROOT', realpath(__DIR__ . '/..'));
define('SRC_DIR', APP_ROOT . '/src');
require_once APP_ROOT . '/vendor/autoload.php';
require SRC_DIR . '/autoload.php';

function getApp()
{
    static $app = null;
    if ($app) {
        return $app;
    }
    /** @var array $config */
    require_once SRC_DIR . '/settings.php';
    if (file_exists(SRC_DIR . '/custom_settings.php')) {
        /** @var array $customConfig */
        require_once SRC_DIR . '/custom_settings.php';
        $config['settings'] = array_replace_recursive($config['settings'], $customConfig['settings']);
    }
    $app = new \Slim\App($config);
    require_once SRC_DIR . '/dependencies.php';
    require_once SRC_DIR . '/global_defines.php';
    Utils::setSettings($app->getContainer()->settings);
    return $app;
}
