<?php

require_once __DIR__ . '/../src/autoload.php';

class MessageTest extends PHPUnit_Framework_TestCase
{
    public function testSms()
    {
        $message = new Message();
        $this->prepareTranslation();

        $smsReserveOutput =
            "Бронь: 666, " .
            "действует до 01.01.2016.\n" .
            "test, 13.01.16,\n" .
            "2 билета\n" .
            "Кассы TEST 7(499)5500055";

        $this->assertEquals($message->smsReservePlaces(666, '01.01.2016', 'test', '13.01.16', 2, 'TEST'), $smsReserveOutput);

        $smsBuyOutput =
            "Заказ 666 оплачен успешно, распечатайте билеты из почты, указанной при оформлении покупки. " .
            "7(499)5500055";

        $this->assertEquals($message->smsBuyPlaces(666), $smsBuyOutput);

        $mailReserveOutput =
            "Бронирование билетов успешно совершено.\n" .
            "test, 13.01.16.\n" .
            "Бронь: 666, " .
            "Вам необходимо выкупить билеты до: 01.01.2016.\n" .
            "2 билета\n" .
            "Кассы TEST 7(499)5500055";

        $this->assertEquals($message->mailReservePlaces(666, '01.01.2016', 'test', '13.01.16', 2, 'TEST'), $mailReserveOutput);

        $mailBuyOutput =
            "Покупка билетов была успешно совершена.\n" .
            "test, 13.01.16.\n" .
            "Заказ: 666.\n" .
            "Печать билетов:\nhttp://www.example.com/1\n" .
            "Call-центр: 7(499)5500055";

        $this->assertEquals($message->mailBuyPlaces(666, 'test', '13.01.16', 'http://www.example.com/1'), $mailBuyOutput);
    }

    private function prepareTranslation()
    {
        $mock_trans = [
            "INF_ORDER_NUM_SHORT" => "Бронь:",
            "WORD_VALID_UNDER" => "действует до",
            "TICKET_SHOP_SHORT" => "Кассы",
            "CROCUS_CITY_HALL_PHONE_SHORT" => "7(499)5500055",
            "CALL_CENTER" => "Call-центр",
            "MSG_ORDER_GOOD" => "Бронирование билетов успешно совершено",
            "MSG_ORDER_UNDER" => "Вам необходимо выкупить билеты до:",
            "MSG_BUY_GOOD" => "Покупка билетов была успешно совершена",
            "MSG_ORDER_NUMBER_SHORT" => "Заказ",
            "MSG_YOU_CAN_PRINT_SHORT" => "Печать билетов",
            "MSG_YOU_CAN_PRINT_COUPON" => "Печать талонов на подарки",
            "MSG_BUY_GOOD_COMPLETED" => "Заказ %:order_id% оплачен успешно, распечатайте билеты из почты, указанной при оформлении покупки"
        ];
        I18N::setLanguage(LANG_RUS);
        I18N::setTranslation($mock_trans, LANG_RUS);
    }
}
